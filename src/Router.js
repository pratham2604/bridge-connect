import React, { Suspense, lazy } from "react"
import { Router, Switch, Route } from "react-router-dom"
import { history } from "./history"
import { connect } from "react-redux"
import Spinner from "./components/@OCG/spinner/Loading-spinner"
import { ContextLayout } from "./utility/context/Layout"
import AuthContainer from './containers/auth';

// Route-based code splitting
const comingSoon = lazy(() => import("./views/pages/misc/ComingSoon"))
const error404 = lazy(() => import("./views/pages/misc/error/404"))
const error500 = lazy(() => import("./views/pages/misc/error/500"))
const authorized = lazy(() => import("./views/pages/misc/NotAuthorized"))
const maintenance = lazy(() => import("./views/pages/misc/Maintenance"))
const Login = lazy(() => import("./views/pages/authentication/login/Login"))
const meetings = lazy(() => import("./views/pages/meetings/index"))
const broadcasts = lazy(() => import("./views/pages/broadcasts/index"))
const tasks = lazy(() => import("./views/pages/tasks/index"))
const chats = lazy(() => import("./views/pages/chats/index"))
const groups = lazy(() => import("./views/pages/groups/index"))
const settings = lazy(() => import("./views/pages/settings/index"))
const home = lazy(() => import("./views/pages/home/index"))
const meetingRoom = lazy(() => import("./views/pages/meetingRoom/index"))
const accountSetup = lazy(() => import("./views/pages/accountSetup/index"))
const broadcastRoom = lazy(() => import("./views/pages/broadcasts/liveBroadcast"))
const recordedBroadcast = lazy(() => import("./views/pages/broadcasts/play"))
const logout = lazy(() => import("./views/pages/logout/index"))
const privacy = lazy(() => import("./views/pages/home/privacy"));
const terms = lazy(() => import('./views/pages/home/terms'));

// Set Layout and Component Using App Route
const RouteConfig = ({ component: Component, fullLayout, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      return (
        <AuthContainer>
          <ContextLayout.Consumer>
            {context => {
              let LayoutTag =
                fullLayout === true
                  ? context.fullLayout
                  : context.state.activeLayout === "horizontal"
                  ? context.horizontalLayout
                  : context.VerticalLayout
              return (
                <LayoutTag {...props} permission={props.user}>
                  <Suspense fallback={<Spinner />}>
                    <Component {...props} />
                  </Suspense>
                </LayoutTag>
              )
            }}
          </ContextLayout.Consumer>
        </AuthContainer>
      )
    }}
  />
)
const mapStateToProps = state => {
  return {
    user: state.auth.user
  }
}

const AppRoute = connect(mapStateToProps)(RouteConfig)

class AppRouter extends React.Component {
  render() {
    return (
      // Set the directory path if you are deploying in sub-folder
      <Router history={history}>
        <Switch>
          <AppRoute exact path="/" component={home} fullLayout />
          <AppRoute path="/meetings" component={meetings} />
          <AppRoute path="/meeting-room/:id" component={meetingRoom} />
          <AppRoute path="/broadcasts" component={broadcasts} />
          <AppRoute path="/play-broadcast/:id" component={recordedBroadcast}/>
          <AppRoute path="/live-broadcast/:id" component={broadcastRoom} />
          <AppRoute path="/tasks" component={tasks} />
          <AppRoute path="/chats" component={chats} />
          <AppRoute path="/groups" component={groups} />
          <AppRoute path="/settings" component={settings} />
          <AppRoute path="/account-setup" component={accountSetup} fullLayout/>
          <AppRoute path="/privacy-policy" component={privacy} fullLayout/>
          <AppRoute path="/terms" component={terms} fullLayout/>
          <AppRoute path="/logout" component={logout} fullLayout/>
          <AppRoute
            path="/misc/coming-soon"
            component={comingSoon}
            fullLayout
          />
          <AppRoute path="/misc/error/404" component={error404} fullLayout />
          <AppRoute path="/login" component={Login} fullLayout />
          <AppRoute path="/misc/error/500" component={error500} fullLayout />
          <AppRoute
            path="/misc/not-authorized"
            component={authorized}
            fullLayout
          />
          <AppRoute
            path="/misc/maintenance"
            component={maintenance}
            fullLayout
          />
          <AppRoute component={error404} fullLayout />
        </Switch>
      </Router>
    )
  }
}

export default AppRouter
