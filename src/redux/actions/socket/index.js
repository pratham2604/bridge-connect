import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
const Centrifuge = require("centrifuge");

export const setupClient = (userID, token, dispatch, meeting_id) => {
  const { hostname } = window.location;
  const baseURL = hostname === 'localhost' ? 'ws://34.87.111.208' : 'wss://bridge.ocg.technology';
  const wsUrl = `${baseURL}/connection/websocket`;
  const centrifuge = new Centrifuge(wsUrl, {
    debug: true,
    subscribeEndpoint: '/api/v1/centrifuge/subscribe',
    subscribeHeaders: {
      Authorization: `Token ${token}`
    }
  });

  const personalChannelName = `personal:#${userID}`;
  centrifuge.subscribe(personalChannelName, (event) => {
    _onCentifugeEvent(event, dispatch);
  });

  if (meeting_id) {
    const meetingChannelName = `$meeting:${meeting_id}`
    centrifuge.subscribe(meetingChannelName, (event) => {
      console.log(event)
      _onCentifugeEvent(event, dispatch);
    });
  }

  // use this way to authenticate
  centrifuge.setConnectData(token);
  centrifuge.connect();
}

export const setupBroadcastClient = (token, dispatch, broadcast_id) => {
  const { hostname } = window.location;
  const name = `$broadcast:${broadcast_id}`
  const baseURL = hostname === 'localhost' ? 'ws://34.87.111.208' : 'wss://bridge.ocg.technology';
  const wsUrl = `${baseURL}/connection/websocket`;
  const centrifuge = new Centrifuge(wsUrl, {
    debug: true,
    subscribeEndpoint: '/api/v1/centrifuge/subscribe',
    subscribeHeaders: {
      Authorization: `Token ${token}`
    }
  });

  centrifuge.subscribe(name, (event) => {
    _onCentifugeEvent(event, dispatch);
  }).on("join", function(message) {
    console.log("Client joined channel", message);
  }).on("leave", function(message) {
      console.log("Client left channel", message);
  });

  // use this way to authenticate
  centrifuge.setConnectData(token);
  centrifuge.connect();
}

export const _onCentifugeEvent = (message, dispatch) => {
  const payload = message.data;
  console.log(payload)
  if (dispatch) {
    dispatch(payload);
  }
}