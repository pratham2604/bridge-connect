import axios from "axios"

export const startMeeting = (user, data) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/meetings`
  return axios
  .post(url, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const getMeetingInfo = (user, meeting_id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/meetings/${meeting_id}/`
  return axios
  .get(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data.meeting;
  })
  .catch(err => console.log(err))
}

export const getMeetingInfoFromShortId = (user, short_id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/meetings/short/${short_id}/`
  return axios
  .get(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data.meeting;
  })
  .catch(err => console.log(err))
}

export const updateMeetingInfo = (user, meeting_id, data) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/meetings/${meeting_id}/`
  return axios
  .put(url, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data.meeting;
  })
  .catch(err => console.log(err))
}

export const requestJoinMeeting = (user, meeting_id) => {
  const { token, user_id } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/meetings/${meeting_id}/join`
  return axios
  .post(url, {}, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const addToMeeting = (user, meeting_id) => {
  const { token, user_id } = user;
  const agora_id = user_id.replace(/\D/g,'').slice(0, 8);
  const data = {
    agora_id,
  }
  const { origin } = window.location;
  const url = `${origin}/api/v1/meetings/${meeting_id}/users`
  return axios
  .post(url, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const allowUsers = (user, meeting_id, user_id) => {
  const { token } = user;
  const data = {
    user_id,
  }
  const { origin } = window.location;
  const url = `${origin}/api/v1/meetings/${meeting_id}/allowed_users`
  return axios
  .post(url, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const muteRemoteStream = (user, meeting_id, data) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/meetings/${meeting_id}/media_status`
  return axios
  .post(url, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const rejectUser = (user, meeting_id, user_id) => {
  const { token } = user;
  const data = {
    user_id,
  }
  const { origin } = window.location;
  const url = `${origin}/api/v1/meetings/${meeting_id}/rejected_users`
  return axios
  .post(url, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const sendMessage = (user, data, meeting_id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/meetings/${meeting_id}/messages`
  return axios
  .post(url, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const getMeetingChats = (user, meeting_id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/meetings/${meeting_id}/messages`
  return axios
  .get(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const getFutureMeetings = (user) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/meetings/future`
  return axios
  .get(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data.meetings;
  })
  .catch(err => console.log(err))
}

export const updateMeetingSettings = (data) => {
  return dispatch => {
    dispatch({ type: "UPDATE_MEETING_SETTINGS", data});
  }
}