import axios from "axios"
import * as firebase from "firebase/app";
import "firebase/storage"
import { config } from "../../../authServices/firebase/firebaseConfig"
import { v4 as uuidv4 } from 'uuid';
if (!firebase.apps.length) {
  firebase.initializeApp(config)
}
const FirebaseStorage = firebase.storage();

export const clearChat = (user, chatId) => {
  const { token } = user;
  return axios
  .post(`api/v1/chats/${chatId}/clear`, {}, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const getChats = (user) => {
  const { token } = user; 
  return axios
  .get("api/v1/chats/", {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data.chats;
  })
  .catch(err => console.log(err))
}

export const getUserChat = (user, chatId) => {
  const { token } = user; 
  return axios
  .get(`api/v1/chats/${chatId}/`, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data = {} } = response;
    return data.messages;
  })
  .catch(err => console.log(err))
}

export const sendMessage = (user, data, chatId) => {
  const { token } = user;
  return axios
  .post(`api/v1/chats/${chatId}/`, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const onUpload = (file, onSuccess, onFailure) => {
  const { name } = file;
  const extension = name.split('.').pop();
  const fileName = `${uuidv4()}.${extension}`;
  FirebaseStorage.ref(fileName).put(file).then(snapshot => {
    snapshot.ref.getDownloadURL().then(link => {
      const data = { fileName, link, name };
      onSuccess(data);
    })
  }).catch(err => {
    onFailure(err);
  });
}

export const getCommonChats = (user, user_id) => {
  const { token } = user;
  return axios
  .post(`/api/v1/common_chats`, {user_id}, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data.result;
  })
  .catch(err => console.log(err))
}

export const makeAdmin = (user, chat_id, user_id) => {
  const { token } = user;
  return axios
  .post(`api/v1/chats/${chat_id}/make_admin`, {user_id}, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}


export const getContacts = (user, onSuccess) => {
  return dispatch => {
    const { token } = user;
    axios
      .get("api/v1/contacts", {
        headers: {
          Authorization: `Token ${token}`
        }
      })
      .then(response => {
        const { contacts } = response.data;
        onSuccess(contacts)
        // dispatch({
        //   type: "GET_CHAT_CONTACTS",
        //   chats: response.data
        // })
      })
      .catch(err => console.log(err))
  }
}

export const getContactsList = (user) => {
  const { token } = user;
  return axios.get(`/api/v1/contacts`, {
    headers: {
      Authorization: `Token ${token}`
    }
  }).then(response => {
    return response.data.contacts;
  })
}

export const muteChat = (user, chat_id) => {
  const { token } = user;
  return axios
  .post(`api/v1/chats/${chat_id}/mute`, {}, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const renameGroup = (user, chat_id, name) => {
  const { token } = user;
  return axios
  .post(`api/v1/chats/${chat_id}/rename`, { name }, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const unmuteChat = (user, chat_id) => {
  const { token } = user;
  return axios
  .post(`api/v1/chats/${chat_id}/unmute`, {}, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const notifyRead = (user, chat_id, lastMessageId) => {
  const { token } = user;
  let data = {};
  if (lastMessageId) {
    data = {
      msg_id: lastMessageId,
    }
  }
  return axios
  .post(`api/v1/chats/${chat_id}/read`, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const deleteMessage = (user, chatId, messageId, forSelf) => {
  const { token } = user;
  const data = {};
  if (forSelf) {
    data.only_mine = true;
  }
  return axios
  .post(`api/v1/chats/${chatId}/messages/${messageId}/delete`, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const createGroup = (user, data) => {
  const { token } = user;
  return axios
  .post("api/v1/chats/", data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const callUser = (user, chat_id, data) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/chats/${chat_id}/calls`
  return axios
  .post(url, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data.call;
  })
  .catch(err => console.log(err))
}

export const getCallLog = (user) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/calls/history`
  return axios
  .get(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const data = response.data;
    return data.calls;
  })
  .catch(err => console.log(err))
}

export const getCallInfo = (user, call_id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/calls/${call_id}/`
  return axios
  .get(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const data = response.data;
    return data.call;
  })
  .catch(err => console.log(err))
}

export const acceptCall = (user, chat_id, call_id, agora_id) => {
  const { token } = user;
  const { origin } = window.location;
  const data = {
    agora_id,
  }
  const url = `${origin}/api/v1/chats/${chat_id}/calls/${call_id}/users`
  return axios
  .post(url, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const endCall = (user, chat_id, call_id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/chats/${chat_id}/calls/${call_id}/`
  return axios
  .delete(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const leaveGroup = (user, chat_id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/chats/${chat_id}/users/`
  return axios
  .delete(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const addToGroup = (user, chat_id, user_id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/chats/${chat_id}/users/${user_id}`
  return axios.post(url, {}, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err));
}

export const removeFromGroup = (user, chat_id, user_id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/chats/${chat_id}/users/${user_id}`
  return axios.delete(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err));
}

export const StartChat = (user, userId) => {
  const { token } = user;
  const data = {
    with: userId
  }
  return axios
  .post("api/v1/chats/", data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const getContactChats = () => {
  return dispatch => {
    axios
      .get("api/app/chat/chat-contacts")
      .then(response => {
        dispatch({
          type: "GET_CHAT_CONTACTS",
          chats: response.data
        })
      })
      .catch(err => console.log(err))
  }
}

export const togglePinned = (id, value) => {
  return dispatch => {
    axios
      .post("/api/apps/chat/set-pinned/", {
        contactId: id,
        value
      })
      .then(response => {
        dispatch({
          type: "SET_PINNED",
          id,
          value
        })
      })
      .catch(err => console.log(err))
  }
}

export const addUserToCall = (user, call_id, user_id) => {
  const { token } = user;
  const data = {
    user_id: user_id,
  }
  const { origin } = window.location;
  const url = `${origin}/api/v1/calls/${call_id}/allowed_users`
  return axios
  .post(url, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}


// export const sendMessage = (id, isPinned, text) => {
//   if (text.length > 0) {
//     return dispatch => {
//       let newMsg = {
//         textContent: text,
//         isSent: true,
//         isSeen: false,
//         time: new Date().toString()
//       }
//       axios
//         .post("/api/app/chat/send-message", {
//           contactId: id,
//           message: newMsg,
//           isPinned
//         })
//         .then(response => {
//           dispatch({
//             type: "SEND_MESSAGE",
//             msg: newMsg,
//             id,
//             isPinned,
//             text
//           })
//           dispatch(getChats())
//         })
//         .catch(err => console.log(err))
//     }
//   } else {
//     return
//   }
// }

export const changeStatus = status => {
  return dispatch => {
    dispatch({
      type: "CHANGE_STATUS",
      status
    })
  }
}

export const searchContacts = query => {
  return dispatch => {
    dispatch({
      type: "SEARCH_CONTACTS",
      query
    })
  }
}

export const markSeenAllMessages = id => {
  return dispatch => {
    axios
      .post("/api/apps/chat/mark-all-seen/", {
        contactId: id
      })
      .then(response => {
        dispatch({
          type: "MARK_AS_SEEN",
          id
        })
      })
      .catch(err => console.log(err))
  }
}

export const getMedia = (user, chat_id, type) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/chats/${chat_id}/${type}`
  return axios
  .get(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data.messages;
  })
  .catch(err => console.log(err))
}

