import axios from "axios"
import * as firebase from "firebase/app";
import "firebase/storage"
import { config } from "../../../authServices/firebase/firebaseConfig"
import { v4 as uuidv4 } from 'uuid';
if (!firebase.apps.length) {
  firebase.initializeApp(config)
}
const FirebaseStorage = firebase.storage();

export const getPublicBroadCasts = (user, onSuccess) => {
  return dispatch => {
    const { token } = user;
    axios
      .get("api/v1/broadcasts/public", {
        headers: {
          Authorization: `Token ${token}`
        }
      })
      .then(response => {
        const { data } = response;
        onSuccess(data);
        // dispatch({
        //   type: "GET_CHAT_HEADS",
        //   chatHeads: data,
        // })
      })
      .catch(err => console.log(err))
  }
}

export const onUpload = (file, onSuccess, onFailure) => {
  const { name } = file;
  const extension = name.split('.').pop();
  const fileName = `${uuidv4()}.${extension}`;
  FirebaseStorage.ref(fileName).put(file).then(snapshot => {
    snapshot.ref.getDownloadURL().then(link => {
      const data = { fileName, link, name };
      onSuccess(data);
    })
  }).catch(err => {
    onFailure(err);
  });
}

export const getMyBroadCasts = (user, onSuccess) => {
  return dispatch => {
    const { token } = user;
    axios
      .get("api/v1/broadcasts/mine", {
        headers: {
          Authorization: `Token ${token}`
        }
      })
      .then(response => {
        const { data } = response;
        onSuccess(data);
        // dispatch({
        //   type: "GET_CHAT_HEADS",
        //   chatHeads: data,
        // })
      })
      .catch(err => console.log(err))
  }
}

export const getOrders = (user) => {
  const { token } = user;
  return axios.get(`/api/v1/payments/orders/`, {
    headers: {
      Authorization: `Token ${token}`
    }
  }).then(response => {
    return response.data;
  })
}

export const getBroadCast = (user, id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/broadcasts/${id}/`
  return axios.get(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  }).then(response => {
    return (response.data || {}).result || {};
  })
}

export const createBroadCast = (data, user, onSuccess) => {
  return dispatch => {
    const { token } = user;
    axios.post("api/v1/broadcasts", data, {
      headers: {
        Authorization: `Token ${token}`,
        "Content-Type": 'application/json'
      }
    })
    .then(response => {
      const { data } = response;
      console.log(data, 'broadcast created');
      onSuccess(data);
      // dispatch({
      //   type: "GET_CHAT_CONTACTS",
      //   chats: response.data
      // })
    })
    .catch(err => console.log(err))
  }
}

export const getBroadCastChats = (user, id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/broadcasts/${id}/messages`
  return axios.get(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  }).then(response => {
    return (response.data || {});
  })
}

export const getBroadCastStats = (user, id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/broadcasts/${id}/stats`
  return axios.get(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  }).then(response => {
    return (response.data.stats || {});
  })
}

export const sendMessage = (user, data, id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/broadcasts/${id}/messages`
  return axios
  .post(url, data, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const geBroadcastChat = (user, id) => {
  const { token } = user;
  const { origin } = window.location;
  const url = `${origin}/api/v1/broadcasts/${id}/messages`
  return axios
  .get(url, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}