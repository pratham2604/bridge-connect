import * as firebase from "firebase/app"
import { history } from "../../../history"
import "firebase/auth"
import "firebase/database"
import axios from "axios"
import { config } from "../../../authServices/firebase/firebaseConfig"

// Init firebase if not already initialized
if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

let firebaseAuth = firebase.auth()

// const initAuth0 = new auth0.WebAuth(configAuth)

export const SendOTP = (phone, onSuccess) => {
  const data = { phone };
  axios.post("../api/v1/auth/code/send/", data, {
    headers: {
      "Content-Type": 'application/json'
    }
  }).then((response) => {
    const { data } = response;
    if (data.status === 'err') {
      window.alert(data.err);
    } else {
      onSuccess();
    }
  }).catch(e => {
    console.log(e)
  })
}

export const VerifyOTP = (phone, code, onSuccess) => {
  return dispatch => {
    const data = { phone, code };
    axios.post("../api/v1/auth/code/check/", data, {
      headers: {
        "Content-Type": 'application/json'
      }
    }).then((response) => {
      const { data } = response;
      if (data.status === 'err') {
        window.alert(data.err);
      } else {
        const user = {
          user_id: data.user_id,
          token: data.token,
        }
        dispatch({
          type: "LOGIN_SUCCESS",
          payload: {
            user,
          }
        })
        onSuccess(user);
      }
    })
  }
}

export const getSelfInfo = (user) => {
  const { token } = user;
  return dispatch => {
    const { origin } = window.location;
    const url = `${origin}/api/v1/self`
    axios.get(url, {
      headers: {
        Authorization: `Token ${token}`
      }
    }).then((response) => {
      const { data } = response;
      if (data.status === 'err') {
        window.alert(data.err);
      } else {
        const userData = Object.assign({}, data.profile, {
          token,
        })
        dispatch({
          type: "LOGIN_SUCCESS",
          payload: {
            user: userData,
          }
        })
      }
    }).catch((err) => {
      const { response } = err;
      const { status } = response || {};
      if (status === 403) {
        dispatch({
          type: "LOGOUT_SUCCESS",
        });
        window.location.reload();
      }
    })
  }
}

export const getSelfUserInfo = (user) => {
  const { token } = user;
  return axios.get(`/api/v1/self`, {
    headers: {
      Authorization: `Token ${token}`
    }
  }).then(response => {
    return response.data;
  }).catch((err) => {
    console.log(err);
  })
}

export const blockUser = (user, user_id) => {
  const { token } = user;
  return axios
  .post(`api/v1/users/${user_id}/block`, {}, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const unblockUser = (user, user_id) => {
  const { token } = user;
  return axios
  .post(`api/v1/users/${user_id}/unblock`, {}, {
    headers: {
      Authorization: `Token ${token}`
    }
  })
  .then(response => {
    const { data } = response;
    return data;
  })
  .catch(err => console.log(err))
}

export const updateProfile = (data, user) => {
  const inputData = data;
  return dispatch => {
    const { token, user_id } = user;
    axios.post(`api/v1/users/${user_id}/`, data, {
      headers: {
        "Content-Type": 'application/json',
        Authorization: `Token ${token}`
      }
    }).then((response) => {
      const { data } = response;
      if (data.status === 'err') {
        window.alert(data.err);
      } else {
        window.alert('Profile Updated')
        const userData = Object.assign({}, inputData, {
          user_id: user.user_id,
          token: user.token,
        })
        dispatch({
          type: "LOGIN_SUCCESS",
          payload: {
            user: userData,
          }
        })
        history.push("/meetings")
      }
    })
  }
}

export const getUser = (user, userId) => {
  const { token } = user;
  return axios.get(`/api/v1/users/${userId}/`, {
    headers: {
      Authorization: `Token ${token}`
    }
  }).then(response => {
    return response.data.profile;
  })
}

export const logout = () => {
  return dispatch => {
    dispatch({ type: "LOGOUT_SUCCESS"})
    history.push('/');
  }
}

export const submitLoginWithFireBase = (email, password, remember) => {
  return dispatch => {
    let userEmail = null,
      loggedIn = false
    firebaseAuth
      .signInWithEmailAndPassword(email, password)
      .then(result => {
        firebaseAuth.onAuthStateChanged(user => {
          result.user.updateProfile({
            displayName: "Admin"
          })
          let name = result.user.displayName
          if (user) {
            userEmail = user.email
            loggedIn = true
            dispatch({
              type: "LOGIN_WITH_EMAIL",
              payload: {
                email: userEmail,
                name,
                isSignedIn: loggedIn,
                loggedInWith: "firebase"
              }
            })
          }
          if (user && remember) {
            firebase
              .auth()
              .setPersistence(firebase.auth.Auth.Persistence.SESSION)
              .then(() => {
                dispatch({
                  type: "LOGIN_WITH_EMAIL",
                  payload: {
                    email: userEmail,
                    name,
                    isSignedIn: loggedIn,
                    remember: true,
                    loggedInWith: "firebase"
                  }
                })
              })
          }
          history.push("/")
        })
      })
      .catch(error => {
        console.log(error)
      })
  }
}

export const loginWithFB = () => {
  return dispatch => {
    let provider = new firebase.auth.FacebookAuthProvider()
    provider.setCustomParameters({
      display: "popup"
    })
    firebaseAuth
      .signInWithPopup(provider)
      .then(result => {
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        let token = result.credential.accessToken,
          // The signed-in user info.
          user = result.user.email
        dispatch({
          type: "LOGIN_WITH_FB",
          payload: {
            user,
            token,
            loggedInWith: "firebase"
          }
        })
        if (user) history.push("/")
      })
      .catch(error => {
        console.log(error)
      })
  }
}

export const loginWithTwitter = () => {
  return dispatch => {
    let provider = new firebase.auth.TwitterAuthProvider()
    firebaseAuth
      .signInWithPopup(provider)
      .then(function(result) {
        let token = result.credential.accessToken,
          user = result.user.email,
          name = result.user.displayName,
          photoUrl = result.user.photoURL
        dispatch({
          type: "LOGIN_WITH_TWITTER",
          payload: {
            user,
            name,
            photoUrl,
            token,
            loggedInWith: "firebase"
          }
        })
        history.push("/")
      })
      .catch(function(error) {
        console.log(error)
      })
  }
}

export const loginWithGoogle = () => {
  return dispatch => {
    let provider = new firebase.auth.GoogleAuthProvider()
    firebaseAuth
      .signInWithPopup(provider)
      .then(function(result) {
        let token = result.credential.accessToken,
          user = result.user.email,
          name = result.user.displayName,
          photoUrl = result.user.photoURL
        dispatch({
          type: "LOGIN_WITH_GOOGLE",
          payload: {
            email: user,
            name: name,
            photoUrl,
            token,
            loggedInWith: "firebase"
          }
        })
        history.push("/")
      })
      .catch(function(error) {
        console.log(error)
      })
  }
}

export const loginWithGithub = () => {
  return dispatch => {
    let provider = new firebase.auth.GithubAuthProvider()
    firebaseAuth
      .signInWithPopup(provider)
      .then(function(result) {
        let token = result.credential.accessToken,
          user = result.user.email,
          name = result.additionalUserInfo.username,
          photoUrl = result.user.photoURL

        dispatch({
          type: "LOGIN_WITH_GITHUB",
          payload: {
            user,
            name,
            photoUrl,
            token,
            loggedInWith: "firebase"
          }
        })
        history.push("/")
      })
      .catch(function(error) {
        console.log(error)
      })
  }
}

export const loginWithJWT = user => {
  return dispatch => {
    axios
      .post("/api/authenticate/login/user", {
        email: user.email,
        password: user.password
      })
      .then(response => {
        var loggedInUser

        if (response.data) {
          loggedInUser = response.data.user

          dispatch({
            type: "LOGIN_WITH_JWT",
            payload: { loggedInUser, loggedInWith: "jwt" }
          })

          history.push("/")
        }
      })
      .catch(err => console.log(err))
  }
}

export const logoutWithJWT = () => {
  return dispatch => {
    dispatch({ type: "LOGOUT_WITH_JWT", payload: {} })
    history.push("/pages/login")
  }
}

export const logoutWithFirebase = user => {
  return dispatch => {
    dispatch({ type: "LOGOUT_WITH_FIREBASE", payload: {} })
    history.push("/pages/login")
  }
}

export const changeRole = role => {
  return dispatch => dispatch({ type: "CHANGE_ROLE", userRole: role })
}
