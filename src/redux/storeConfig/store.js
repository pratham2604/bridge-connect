import { createStore, applyMiddleware, compose } from "redux"
import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage';
import createDebounce from "redux-debounced"
import thunk from "redux-thunk";
import reducers from "../reducers/rootReducer"

const config = {
  key: 'root',
  storage,
  blacklist: [],
};

const reducer = persistCombineReducers(config, reducers);
const middlewares = [thunk, createDebounce()]

const configureStore = () => {
  const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(reducer, composeEnhancer(applyMiddleware(...middlewares)));

  const persistor = persistStore(store, null, () => {
    store.getState();
  });

  return {
    persistor,
    store
  };
};

export default configureStore;
