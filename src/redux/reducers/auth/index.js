const initialState = {
  fetching: false,
  error: null,
  user: {},
  isAuthenticated: false,
  token: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "LOGIN_SUCCESS": {
      return Object.assign({}, state, {
        user: action.payload.user,
        fetching: false,
        error: null,
        isAuthenticated: true,
      });
    }
    case "LOGOUT_SUCCESS": {
      return Object.assign({}, state, {
        user: {},
        fetching: false,
        error: null,
        isAuthenticated: false,
      });
    }
    case "FETCH_AUTH_SUCCESS": {
      return Object.assign({}, state, {
        user: action.data,
        fetching: false,
        error: null,
        isAuthenticated: true,
      });
    }
    case "FETCH_AUTH_REQUEST": {
      return Object.assign({}, state, {
        fetching: true,
        error: null,
      });
    }
    case "FETCH_AUTH_FAILURE": {
      return Object.assign({}, state, {
        user: {},
        fetching: false,
        error: null,
        isAuthenticated: false,
      });
    }
    case "CHANGE_ROLE": {
      return { ...state, userRole: action.userRole }
    }
    default: {
      return state
    }
  }
}
