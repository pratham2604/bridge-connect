const initialState = {
  settings: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "UPDATE_MEETING_SETTINGS": {
      return Object.assign({}, state, {
        settings: action.data,
      });
    }
    default: {
      return state
    }
  }
}
