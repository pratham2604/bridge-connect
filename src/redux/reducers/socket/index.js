
export default (state = {}, action) => {
  switch (action.type) {
    case "NEW_MSG": {
      return Object.assign({}, state, {
        type: 'NEW_MSG',
        newMessage: true,
        // user: action.payload.user,
        // fetching: false,
        // error: null,
        // isAuthenticated: true,
      });
    }
    case "MSG_READ": {
      return Object.assign({}, state, {
        type: 'MSG_READ',
        // newMessage: true,
        // user: {},
        // fetching: false,
        // error: null,
        // isAuthenticated: false,
      });
    }
    case "MSG_DELETE": {
      return Object.assign({}, state, {
        type: 'MSG_DELETE',
        newMessage: true,
        // user: action.data,
        // fetching: false,
        // error: null,
        // isAuthenticated: true,
      });
    }
    case "CLEAR_CHAT": {
      return Object.assign({}, state, {
        type: 'CLEAR_CHAT',
        newMessage: true,
        // fetching: true,
        // error: null,
      });
    }
    case "NEW_CALL": {
      return Object.assign({}, state, {
        type: 'NEW_CALL',
        newMessage: true,
        data: action,
        // fetching: true,
        // error: null,
      });
    }
    case "JOIN_MEETING": {
      return Object.assign({}, state, {
        type: 'JOIN_MEETING',
        newMessage: true,
        data: action,
        // fetching: true,
        // error: null,
      });
    }
    case "NEW_MEETING_MSG": {
      return Object.assign({}, state, {
        newMessage: true,
        type: 'NEW_MEETING_MSG',
      })
    }
    case "NEW_BROADCAST_MSG": {
      return Object.assign({}, state, {
        newMessage: true,
        type: 'NEW_BROADCAST_MSG',
      })
    }
    case "RESET_SOCKET": {
      return Object.assign({}, state, {
        newMessage: false,
        type: '',
      })
    }
    case "END_CALL": {
      return Object.assign({}, state, {
        type: 'END_CALL',
        newMessage: action,
        data: action,
        // fetching: true,
        // error: null,
      });
    }
    case "GROUP_INFO_CHANGED": {
      return Object.assign({}, state, {
        type: 'GROUP_INFO_CHANGED',
        newMessage: action,
        data: action,
        // fetching: true,
        // error: null,
      });
    }
    default: {
      return state
    }
  }
}
