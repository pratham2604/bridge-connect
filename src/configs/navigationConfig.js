import React from "react"
import * as Icon from "react-feather"
const navigationConfig = [
  {
    id: "meetings",
    title: "Meetings",
    type: "item",
    icon: <Icon.Inbox size={20} />,
    navLink: "/meetings",
  },
  {
    id: "broadcast",
    title: "Broadcast",
    type: "item",
    icon: <Icon.Cast size={20} />,
    navLink: "/broadcasts"
  },
  // {
  //   id: "task",
  //   title: "Tasks",
  //   type: "item",
  //   icon: <Icon.CheckSquare size={20} />,
  //   navLink: "/tasks",
  // },
  {
    id: "chat",
    title: "Chats",
    type: "item",
    icon: <Icon.MessageSquare size={20} />,
    navLink: "/chats"
  },
  {
    id: "groups",
    title: "Groups",
    icon: <Icon.Users size={20} />,
    type: "item",
    navLink: "/groups",
  },
  {
    id: "settings",
    title: "Settings",
    icon: <Icon.Settings size={20} />,
    type: "item",
    navLink: "/settings",
  },
  {
    id: "logout",
    title: "Logout",
    icon: <Icon.LogOut size={20} />,
    type: "item",
    navLink: "/logout",
  }
]

export default navigationConfig
