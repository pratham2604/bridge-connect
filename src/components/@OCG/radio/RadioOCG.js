import React from "react"
import classnames from "classnames"
class RadioOCG extends React.Component {
  render() {
    return (
      <div
        className={classnames(
          `ocg-radio-con ${this.props.className} ocg-radio-${this.props.color}`
        )}
      >
        <input
          type="radio"
          defaultChecked={this.props.defaultChecked}
          value={this.props.value}
          disabled={this.props.disabled}
          name={this.props.name}
          onClick={this.props.onClick}
          onChange={this.props.onChange}
          ref={this.props.ref}
          checked={this.props.checked}
        />
        <span
          className={classnames("ocg-radio", {
            "ocg-radio-sm": this.props.size === "sm",
            "ocg-radio-lg": this.props.size === "lg"
          })}
        >
          <span className="ocg-radio--border" />
          <span className="ocg-radio--circle" />
        </span>
        <span>{this.props.label}</span>
      </div>
    )
  }
}
export default RadioOCG
