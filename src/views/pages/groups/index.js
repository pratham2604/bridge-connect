import React from "react"
import Sidebar from "react-sidebar"
import { ContextLayout } from "../../../utility/context/Layout"
import "../../../assets/scss/pages/app-email.scss"
import FixedSidebar from './sidebar';
import ChatBox from './chatBox';
import ReceiverSidebar from './UserInfo/index';
import { connect } from "react-redux";
import { getChats, getContactsList } from "../../../redux/actions/chat/index"
import { getUser } from "../../../redux/actions/auth/loginActions";
const mql = window.matchMedia(`(min-width: 992px)`)

class Index extends React.Component {
  state = {
    composeMailStatus: false,
    sidebarDocked: mql.matches,
    sidebarOpen: false,
    contact: {},
    showProfile: false,
    chats: [],
    contacts: [],
  }

  componentDidMount () {
    this.fetchGroups();
  }

  componentDidUpdate() {
    const { updateFromSocket } = this.props;
    const { type } = updateFromSocket;
    console.log(type)
    if (type && type === 'GROUP_INFO_CHANGED') {
      this.fetchGroups();
    }
  }

  fetchGroups = async () => {
    // await getChats()
    const { user } = this.props;
    const chats = await getChats(user);
    const groupChats = await Promise.all((chats || []).filter(chat => chat.is_group).map(async(chat) => {
      const { users } = chat;
      const usersData = await Promise.all(users.map(userId =>  getUser(user, userId)))
      return Object.assign({}, chat, {
        users: usersData,
      });
    }));
    const contact = groupChats.find(group => group.chat_id === this.state.contact.chat_id) || {};

    const contacts = await getContactsList(user);
    const filteredChats = (chats || []).filter(chat => !chat.is_group).filter((chat) => {
      return !!(chat.last_msg && chat.last_msg.msg)
    });
    const userIds = contacts.map(contact => contact);
    filteredChats.forEach(chat => {
      const { users } = chat;
      users.forEach(userId => {
        if (userId !== user.user_id && !userIds.some(id => id === userId)) {
          userIds.push(userId);
        }
      })
    });
    const users = await Promise.all(userIds.map(userId => getUser(user, userId)));
    const updatedUsers = users.map(user => {
      const chat = chats.find(chat => !chat.is_group && chat.users.some(userId => userId === user.user_id)) || {};
      return Object.assign({}, user, chat);
    }).filter(currentUser => currentUser.user_id !== user.user_id)

    this.setState({
      chats: groupChats,
      contact,
      contacts: updatedUsers,
    })
  }


  handleComposeSidebar = status => {
    if (status === "open") {
      this.setState({
        composeMailStatus: true
      })
    } else {
      this.setState({
        composeMailStatus: false
      })
    }
  }

  UNSAFE_componentWillMount() {
    mql.addListener(this.mediaQueryChanged)
  }

  componentWillUnmount() {
    mql.removeListener(this.mediaQueryChanged)
  }

  onSetSidebarOpen = open => {
    this.setState({ sidebarOpen: open })
  }

  mediaQueryChanged = () => {
    this.setState({ sidebarDocked: mql.matches, sidebarOpen: false })
  }

  handleMainAndComposeSidebar = () => {
    this.handleComposeSidebar("close")
    this.onSetSidebarOpen(false)
  }

  showChat = (contact) => {
    this.setState({
      contact,
    })
  }

  showProfile = () => {
    this.setState({
      showProfile: !this.state.showProfile
    })
  }

  render() {
    const { chats, contacts } = this.state;
    return (
      <React.Fragment>
        <div className="custom-fixed-sidebar-container position-relative" style={{height: '100%'}}>
          <div
            className={`app-content-overlay ${
              this.state.composeMailStatus || this.state.sidebarOpen ? "show" : ""
            }`}
            onClick={this.handleMainAndComposeSidebar}
          />
          <ContextLayout.Consumer>
            {context => (
              <Sidebar
                sidebar={
                  <FixedSidebar showChat={this.showChat} chats={chats} fetchGroups={this.fetchGroups}/>
                }
                docked={this.state.sidebarDocked}
                open={this.state.sidebarOpen}
                sidebarClassName="sidebar-content custom-app-sidebar d-flex"
                touch={false}
                contentClassName="sidebar-children"
                pullRight={context.state.direction === "rtl"}>
                <div className="chat-application" style={{height: '100%'}}>
                  <ChatBox activeUser={this.state.contact} showProfile={this.showProfile} fetchGroups={this.fetchGroups}/>
                  <ReceiverSidebar
                    group={this.state.contact}
                    receiverProfile={this.state.showProfile}
                    handleReceiverSidebar={this.showProfile}
                    fetchData={this.fetchGroups}
                    contacts={contacts}
                  />
                </div>
              </Sidebar>
            )}
          </ContextLayout.Consumer>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    updateFromSocket: state.socket.newMessage
  }
}

export default connect(mapStateToProps, {})(Index);

