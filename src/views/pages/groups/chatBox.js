import React, { Component } from "react"
import ReactDOM from "react-dom"
import { Input, Button } from "reactstrap"
import { MessageSquare, Menu, Send } from "react-feather"
import PerfectScrollbar from "react-perfect-scrollbar"
import { connect } from "react-redux"
import { sendMessage, getUserChat, deleteMessage, clearChat, muteChat, unmuteChat, notifyRead, onUpload, StartChat } from "../../../redux/actions/chat/index"
import * as Icon from "react-feather"
import { Dropdown, Modal, Image } from 'semantic-ui-react';
import { Button as SemanticButton, Icon as SemanticIcon } from 'semantic-ui-react' ;
import ReactPlayer from 'react-player'

class ChatLog extends React.Component {
  static getDerivedStateFromProps(props, state) {
    if (
      props.activeUser !== state.activeChat ||
      props.activeChat !== state.activeChat
    ) {
      return {
        activeUser: props.activeUser,
        activeChat: props.activeChat
      }
    }
    // Return null if the state hasn't changed
    return null
  }
  state = {
    msg: "",
    activeUser: null,
    activeChat: null,
    chat: [],
    replyMessage: {}
  }

  handleSendMessage = (id, isPinned, text) => {
    // if (text.length > 0) {
    //   this.props.sendMessage(id, isPinned, text)
    //   this.setState({
    //     msg: ""
    //   })
    // }
  }

  componentDidMount() {
    this.scrollToBottom()
  }

  componentDidUpdate(prevPros) {
    this.scrollToBottom();
    const { activeUser, updateFromSocket } = this.props;
    if (prevPros.activeUser.chat_id !== activeUser.chat_id) {
      this.fetchData();
    }

    if (updateFromSocket) {
      this.fetchData();
      this.props.fetchGroups();
    }
  }

  fetchData = async () => {
    const { activeUser, user, dispatch } = this.props;
    const group = activeUser;
    const { chat_id, last_read_msg_id, users } = group;
    console.log(group)
    let read = false;
    const chat = await getUserChat(user, chat_id);
    const chatData = (chat || []).map(item => {
      if (item.msg_id === last_read_msg_id) {
        read = true;
      }
      return Object.assign({}, item, {
        isSent: item.sender_id !== user.user_id,
        user: users.find(groupUser => groupUser.user_id === item.sender_id) || {},
        read,
      });
    })
    const lastMessageId = ((chatData || [])[0] || {}).msg_id;
    await notifyRead(user, chat_id, lastMessageId);
    this.props.fetchGroups();
    this.setState({
      chat: chatData,
    })
    dispatch({
      type: 'RESET_SOCKET'
    });
  }

  deleteMessage = async (messageId, forSelf = false) => {
    const { user, activeUser } = this.props;
    const { chat_id } = activeUser;
    await deleteMessage(user, chat_id, messageId, forSelf);
    this.fetchData();
    this.props.fetchGroups();
  } 
  
  handleTime = (time_to, time_from) => {
    const date_time_to = new Date(Date.parse(time_to))
    const date_time_from = new Date(Date.parse(time_from))
    return (
      date_time_to.getFullYear() === date_time_from.getFullYear() &&
      date_time_to.getMonth() === date_time_from.getMonth() &&
      date_time_to.getDate() === date_time_from.getDate()
    )
  }

  scrollToBottom = () => {
    const chatContainer = ReactDOM.findDOMNode(this.chatArea)
    chatContainer.scrollTop = chatContainer.scrollHeight
  }

  unmuteUser = async () => {
    const { user, activeUser } = this.props;
    const { chat_id } = activeUser;
    await unmuteChat(user, chat_id);
    this.fetchData();
  }

  muteUser = async () => {
    const { user, activeUser } = this.props;
    const { chat_id } = activeUser;
    await muteChat(user, chat_id);
    this.fetchData();
  }

  clearChat = async () => {
    const { user, activeUser } = this.props;
    const { chat_id } = activeUser;
    await clearChat(user, chat_id);
    this.fetchData();
    this.props.fetchGroups();
  }

  getPrivateChatId = async (receiverId) => {
    const { user } = this.props;
    const data = await StartChat(user, receiverId);
    const { chat_id } = data;
    return chat_id;
  }

  sendMessage = async () => {
    const { activeUser, user } = this.props;
    let { chat_id } = activeUser;
    const { newMessage } = this.state;
    if (!newMessage) {
      return;
    }
    const msg = JSON.stringify({
      content: newMessage
    })
    const data = {
      msg,
      msg_type: 'text',
    }

    const { replyMessage, isPrivateReply } = this.state;
    if (replyMessage.msg_id) {
      const { msg_id } = replyMessage;
      data.reply_to = msg_id;
    }
    if (isPrivateReply) {
      const { sender_id } = replyMessage;
      chat_id = await this.getPrivateChatId(sender_id);
    }

    await sendMessage(user, data, chat_id);
    this.fetchData();
    this.props.fetchGroups();
    this.setState({
      newMessage: '',
      replyMessage: {},
      isPrivateReply: false,
      replyUser: {},
    })
  }

  sendFileMessage = async (type, attachment) => {
    const { activeUser } = this.props;
    const { user } = this.props;
    const { chat_id } = activeUser;
    const content = {};
    if (type === 'image') {
      content.imageUrl = attachment.link;
    } else if (type === 'video') {
      content.videoUrl = attachment.link;
    } else if (type === 'file') {
      content.fileUrl = attachment.link;
      content.fileName = attachment.name;
    } else {
      content.content = "Invalid data";
    }
    const msg = JSON.stringify(content)
    const data = {
      msg,
      msg_type: type,
    }
    await sendMessage(user, data, chat_id);
    this.fetchData();
    this.props.fetchGroups();
    this.setState({
      newMessage: '',
      replyMessage: {},
      showAttachmentModal: false,
      isPrivateReply: false,
    })
  }

  setReply = (replyMessage) => {
    this.setState({
      replyMessage,
      isPrivateReply: false,
    })
  }

  setPrivateReply = (replyMessage) => {
    this.setState({
      replyMessage,
      isPrivateReply: true,
    })
  }

  toggleAttachmentModal = () => {
    this.setState({
      showAttachmentModal: !this.state.showAttachmentModal,
    });
  }

  render() {
    const { activeUser = {} } = this.props;
    const chats = this.state.chat.map(chat => Object.assign({}, chat));

    let renderChats = chats.reverse().map((chat, i) => {
      console.log(chat)
      let renderAvatar = () => {
        return (
          <div className="chat-avatar">
            <div className="avatar m-0">
            {chat.user.avatar ? <img src={chat.user.avatar} height="40" width="40" alt="profile"/> :
              <Avatar name={chat.user.fullname} />}
            </div>
          </div>
        )
      }
      const { msg } = chat;
      let message = {};
      try {
        message = JSON.parse(msg)
      } catch(e) {
        message = {
          content: msg,
        }
      }
      const { content, videoUrl, imageUrl, fileUrl, fileName } = message;
      let chatMessage = '';
      if (chat.msg_type === 'text') {
        chatMessage = <span>{content}</span>;
      } else if (chat.msg_type === 'image') {
        chatMessage = <Image src={imageUrl} size="medium" />;
      } else if (chat.msg_type === 'video') {
        chatMessage =  <ReactPlayer className='react-player' width='300px' height='200px' url={videoUrl} controls={true} />;
      } else if (chat.msg_type === 'file') {
        chatMessage = <a href={fileUrl} target="_blank" rel="noopener noreferrer" download>
          <span className="mr-1">{fileName}</span>
          <SemanticIcon name="file" size="large" className="mr-1"/>
        </a>;
      }
      const { reply_to } = chat;
      let showReply = false;
      let replyMessageContent = '';
      let replyUser = {};
      if (reply_to) {
        showReply = true;
        const targetChat = chats.find(chat => chat.msg_id === reply_to.msg_id);
        const { msg } = targetChat || {};
        let message = {};
        replyUser = (targetChat || {}).user || {};
        if (msg) {
          try {
            message = JSON.parse(msg)
          } catch(e) {
            message = {
              content: '',
            }
          }
          message = JSON.parse(msg) || {};
          replyMessageContent = message.content;
        } else {
          replyMessageContent = 'Deleted Message'
        }
      }
      return (
        <React.Fragment key={i}>
          <div
            className={`chat ${
              chat.isSent ? "chat-left" : "chat-right"
            }`}>
            {renderAvatar()}
            <div className="chat-body">
              <div className="chat-content">
                {showReply &&
                  <div className="text-left p-1" style={{borderRadius: '5px', backgroundColor: '#d3d3d3'}}>
                    <div className="text-bolo-600">{replyUser.fullname}</div>
                    <div>{replyMessageContent}</div>
                  </div>
                }
                <div>
                  {!chat.isSent &&
                    <span className="mr-1">
                      {!chat.read ?
                        <Icon.Check size={15} /> :
                        <Icon.Check size={15} style={{color: '#00ffff'}}/>
                      }
                    </span>
                  }
                  {chatMessage}
                  <span className="chat-actions">
                    <Dropdown icon="chevron down" direction='left'>
                      <Dropdown.Menu>
                        {chat.msg_type === 'image' &&
                          <Dropdown.Item text="Download" onClick={(e) => {this.download(imageUrl)}}></Dropdown.Item>
                        }
                        <Dropdown.Item text="Reply" onClick={(e) => {this.setReply(chat)}}></Dropdown.Item>
                        {chat.isSent && <Dropdown.Item text="Private Reply" onClick={(e) => {this.setPrivateReply(chat)}}></Dropdown.Item>}
                        <Dropdown.Item text="Delete For All" onClick={(e) => {this.deleteMessage(chat.msg_id)}}></Dropdown.Item>
                        <Dropdown.Item text="Delete For Me" onClick={(e) => {this.deleteMessage(chat.msg_id, true)}}></Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      )
    })
       
    const { replyMessage } = this.state;
    const { msg } = replyMessage || {};
    let message = {};
    let replyMessageContent = '';
    const replyUser = replyMessage.user;
    if (msg) {
      try {
        message = JSON.parse(msg)
      } catch(e) {
        message = {
          content: '',
        }
      }
      message = JSON.parse(msg) || {};
      replyMessageContent = message.content;
    }

    return (
      <div className="chat-app-window" style={{height: '100%'}}>
          {!activeUser.chat_id &&
            <div className={`start-chat-area ${ activeUser.uid === null ? "d-none" : "d-flex"}`} style={{height: '100%'}}>
              <span className="mb-1 start-chat-icon">
                <MessageSquare size={50} />
              </span>
              <h4
                className="py-50 px-1 sidebar-toggle start-chat-text"
                onClick={() => {
                  if (this.props.mql.matches === false) {
                    this.props.mainSidebar(true)
                  } else {
                    return null
                  }
                }}>
                Start Conversation
              </h4>
            </div>
          }
          <div
            style={{height: '100%', flexDirection: 'column'}}
            className={`active-chat ${
              !activeUser.chat_id ? "d-none" : "d-flex"
            }`}>
            <div className="chat_navbar">
              <header className="chat_header d-flex justify-content-between align-items-center p-1">
                <div className="d-flex align-items-center">
                  <div
                    className="sidebar-toggle d-block d-lg-none mr-1"
                    onClick={() => this.props.mainSidebar(true)}>
                    <Menu size={24} />
                  </div>
                  <div
                    className="avatar user-profile-toggle m-0 m-0 mr-1"
                    onClick={() => this.props.showProfile()}>
                    {activeUser.avatar ?
                        <img
                          src={activeUser !== null ? activeUser.avatar : ""}
                          height="40"
                          width="40"
                          alt="profile"
                        /> :
                        <Avatar name={activeUser.group_name} />
                      }
                  </div>
                  <h6 className="mb-0">
                    {activeUser !== null ? activeUser.group_name : ""}
                  </h6>
                </div>
                <span
                  className="favorite"
                  onClick={() => {
                    
                  }}>
                    {/* <Button.Ripple className="mr-1 mb-1 rounded-circle pl-1 pr-1">
                      <Icon.Video size={17} />
                    </Button.Ripple>
                    <Button.Ripple className="mr-1 mb-1 rounded-circle pl-1 pr-1">
                      <Icon.Phone size={17} />
                    </Button.Ripple> */}
                    <Dropdown icon="ellipsis vertical" direction='left'>
                      <Dropdown.Menu>
                        <Dropdown.Item text="Clear Chat" onClick={this.clearChat}></Dropdown.Item>
                        {activeUser.muted ?
                          <Dropdown.Item text="Unmute" onClick={this.unmuteUser}></Dropdown.Item> :
                          <Dropdown.Item text="Mute" onClick={this.muteUser}></Dropdown.Item>
                        }
                      </Dropdown.Menu>
                    </Dropdown>
                </span>
              </header>
            </div>
            <PerfectScrollbar style={{flex: '1'}} className="user-chats" options={{ wheelPropagation: false}} ref={el => {this.chatArea = el }}>
              <div className="chats">{renderChats}</div>
            </PerfectScrollbar>
            <div className="chat-app-form">
              <div>
                {replyMessageContent &&
                  <div className="mb-1 p-1 d-flex" style={{backgroundColor: '#c2c6dc', borderRadius: '5px'}}>
                    <div style={{flex: '1'}}>
                      <div className="text-bolo-600">{replyUser.fullname}</div>
                      <div>{replyMessageContent}</div>
                    </div>
                    <div className="p-1 cursor-pointer" onClick={(e) => {this.setState({replyMessage: {}, isPrivateReply: false})}}>
                      <Icon.X size={17} />
                    </div>
                  </div>
                }
              </div>
              <form className="chat-app-input d-flex align-items-center" onSubmit={e => {e.preventDefault(); this.sendMessage();}}>
                <Input type="text" className="message mr-1 ml-50" placeholder="Type your message" value={this.state.newMessage}
                  onChange={e => {
                    e.preventDefault()
                    this.setState({
                      newMessage: e.target.value
                    })
                  }}
                />
                <Button color="primary" type="button" className="mx-1" onClick={this.toggleAttachmentModal}>
                  <SemanticIcon name="attach" size={15} />
                </Button>
                <Button color="primary" type="submit">
                  <Send className="d-lg-none" size={15} />
                  <span className="d-lg-block d-none">Send</span>
                </Button>
              </form>
            </div>
          </div>
          {this.state.showAttachmentModal && <AttchmentModal onClose={this.toggleAttachmentModal} sendFileMessage={this.sendFileMessage} />}
        </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    user: state.auth.user,
    updateFromSocket: state.socket.newMessage
  }
}

const typeMap = {
  fileImageInput: 'image',
  fileVideoInput: 'video',
  fileInput: 'file',
}

class AttchmentModal extends Component {
  state = {}

  onUploadClick = (ref) => {
    this.refs[ref].click();
    this.setState({
      uploadType: typeMap[ref],
    })
  }

  uploadFile() {
    const { file, uploadType } = this.state;
    this.setState({
      uploading: true,
    });
    onUpload(file, (data) => {
      this.setState({
        uploading: false,
      });
      this.props.sendFileMessage(uploadType, data);
    }, (err) => {
      console.log(err);
    });
  }

  _handleImageChange = (event) => {
    event.preventDefault();

    const reader = new FileReader();
    const file = event.target.files[0];
    const self = this;

    reader.onloadend = () => {
      self.setState({
        file,
      });
      self.uploadFile();
    };

    // To clear currently loaded file from input
    event.target.value = null;
    reader.readAsDataURL(file);
  }

  render() {
    const { onClose } = this.props;
    const { uploading } = this.state;
    return (
      <Modal open={true} onClose={onClose} className="p-1 attachment-upload-modal">
        <Modal.Content className="m-0">
          Select Attchment type
          <div className="m-auto text-center pt-1">
            {uploading ?
              <div>Uploading and sending message ... </div> :
              <SemanticButton.Group>
                <SemanticButton className="position-relative" onClick={this.onUploadClick.bind(this, 'fileImageInput')}>
                  <span>Image</span>
                  <input type="file" accept="image/*" ref="fileImageInput" className="file-input" onChange={this._handleImageChange}></input>
                </SemanticButton>
                <SemanticButton.Or />
                <SemanticButton className="position-relative" onClick={this.onUploadClick.bind(this, 'fileVideoInput')}>
                  <span>Video</span>
                  <input type="file" accept="video/*" ref="fileVideoInput" className="file-input" onChange={this._handleImageChange}></input>
                </SemanticButton>
                <SemanticButton.Or />
                <SemanticButton className="position-relative" onClick={this.onUploadClick.bind(this, 'fileInput')}>
                  <span>File</span>
                  <input type="file" ref="fileInput" className="file-input" onChange={this._handleImageChange}></input>
                </SemanticButton>
              </SemanticButton.Group>
            }
          </div>
        </Modal.Content>
      </Modal>
    )
  }
}

class Avatar extends React.Component {
  render() {
    const { name = '' } = this.props;
    const initial = name[0];

    return (
      <span className="avatar-section">
        {initial ?
          <span className="avatar">{initial}</span> :
          <span className="user-icon icon-user-circle"></span>
        }
      </span>
    )
  }
}

const mapDispatchtoProps = (dispatch) => {
  return {
      dispatch
  };
};

export default connect(mapStateToProps, mapDispatchtoProps)(ChatLog)
