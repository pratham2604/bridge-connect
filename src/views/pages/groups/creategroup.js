import React, { Component } from 'react';
import {
  Input,
} from "reactstrap"
import { getContactsList, createGroup } from "../../../redux/actions/chat/index"
import { getUser } from '../../../redux/actions/auth/loginActions'
import { Grid, Modal, Button } from 'semantic-ui-react'
import { connect } from "react-redux";
import Select from "react-select"

class Index extends Component {
  state = {
    isLoading: false, results: [], value: '',
    contacts: [],
    name: '',
    modalOpen: false,
  }

  async componentDidMount () {
    const { user } = this.props;
    const contacts = await getContactsList(user);
    const users = await Promise.all(contacts.map(userId => getUser(user, userId)));
    this.setState({
      contacts: users,
    });
  }

  createGroup = async () => {
    const { user } = this.props;
    const data = {
      name: this.state.name,
      with: this.state.selectedOptions.map(option => option.value),
    }
    await createGroup(user, data);
    this.props.fetchGroups();
    this.handleClose();
  }

  handleOpen = () => this.setState({ modalOpen: true })

  handleClose = () => this.setState({ modalOpen: false })

  render() {
    const { name, contacts } = this.state;
    const contactOptions = contacts.map(contact => {
      return Object.assign({}, {
        label: contact.fullname,
        value: contact.user_id,
      })
    })
    return (
      <Modal
        trigger={<Button type="button" className="mb-1" onClick={this.handleOpen}>Add Group</Button>}
        style={{left: '40%', top: '10%', height: '400px', width: '400px'}}
        open={this.state.modalOpen}
        onClose={this.handleClose}
      >
        <Modal.Header>Create Group</Modal.Header>
        <Modal.Content className="m-0">
          <Grid className="w-100">
            <Grid.Row>
            <Grid.Column width={16}>
              <Input
                type="text"
                className="message ml-2"
                placeholder="Group Name"
                value={name}
                onChange={e => {
                  e.preventDefault()
                  this.setState({
                    name: e.target.value
                  })
                }}
              />
              <Select
                isMulti
                name="with"
                options={contactOptions}
                className="React mt-1 ml-2"
                classNamePrefix="select"
                onChange={(options) => {
                  this.setState({
                    selectedOptions: options
                  })
                }}
              />
              <Button color="primary" onClick={this.createGroup} className="mt-1 ml-2">
                <span className="d-lg-block d-none" style={{color: 'white'}}>Create Group</span>
              </Button>
            </Grid.Column>
            </Grid.Row>
          </Grid>
        </Modal.Content>
      </Modal>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {
})(Index);