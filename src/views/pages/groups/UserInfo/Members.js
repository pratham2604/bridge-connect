import React from 'react';
import Avatar from './avatar';

export default (props) => {
  const { group } = props;
  const { users = [], admins = [] } = group
  return (
    <div className="p-1" style={{borderBottom: '1px solid lightgray'}}>
      <h3 className="text-bold-600 mb-1">Group Members</h3>
      <div className="mb-1">
        {users.map((user, index) => {
          return (
            <div className="d-flex" key={index}>
              <div className="avatar">
                {user.avatar ?
                  <img src={user.avatar} height="40" width="40" alt="profile"/> :
                  <Avatar name={user.fullname} />
                }
              </div>
              <div className="d-flex flex-column justify-content-center text-bold-600" style={{flex: '1', paddingLeft: '0.5rem'}}>
                <div>{user.fullname}</div>
                <div style={{color: 'gray'}}>{user.status}</div>
              </div>
              <div style={{color: 'red'}}>
                {admins.some(id => id === user.user_id) && 'admin'}
              </div>
            </div>
          )
        })}
      </div>
    </div>
  )
}