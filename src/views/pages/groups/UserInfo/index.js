import React, { Component } from "react"
import { X } from "react-feather"
import { getCommonChats, getChats, getMedia } from "../../../../redux/actions/chat/index"
import { connect } from "react-redux"
import Avatar from './avatar';
import Members from './Members';
import SharedMedia from './sharedMedia';
import Settings from './settings';

class AboutUser extends Component {
  state = {}

  async componentDidUpdate(prevPros) {
    const { group, user } = this.props;
    if (group.chat_id && prevPros.group.chat_id !== group.chat_id) {
      const audio = await getMedia(user, group.chat_id, 'audios');
      const files = await getMedia(user, group.chat_id, 'files');
      const images = await getMedia(user, group.chat_id, 'images');
      const videos = await getMedia(user, group.chat_id, 'videos');
      const media = {
        audio,
        files,
        images,
        videos,
      }
      this.setState({
        media
      });
    } 
  }

  render() {
    const {  media = {} } = this.state;
    const { group, fetchData, handleReceiverSidebar, contacts } = this.props;
    const containerClass = `user-profile-sidebar ${this.props.receiverProfile ? "show" : null}`;

    return (
      <div style={{height: '100%', top: '0', right: '30px'}} className={containerClass}>
        <header className="user-profile-header" style={{borderBottom: '1px solid lightgray'}}>
          <span className="close-icon" onClick={() => handleReceiverSidebar("close")}>
            <X size={24} />
          </span>
          <div className="header-profile-sidebar">
            <div className="avatar">
              <Avatar name={group.group_name} />
            </div>
            <div>
              <h4 className="chat-user-name mb-0" style={{fontSize: '1.3rem'}}>
                {group.group_name}
              </h4>
              <h6 className="text-center" style={{color: 'lightgray'}}>{(group.users || []).length} members</h6>
            </div>
            <div>
              <Settings user={this.props.user} group={group} fetchData={fetchData} close={handleReceiverSidebar} contacts={contacts}/>
            </div>
          </div>
        </header>
        <Members group={group} />
        <SharedMedia media={media}/>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {})(AboutUser);

