import React, { Component } from 'react';
import { Icon, Modal, Button, Input } from 'semantic-ui-react';
import { unmuteChat, muteChat, leaveGroup, addToGroup, renameGroup, removeFromGroup } from '../../../../redux/actions/chat/index';
import Avatar from './avatar';

export default class extends Component {
  unmuteGroup = async () => {
    const { user, group } = this.props;
    const { chat_id } = group;
    await unmuteChat(user, chat_id);
    this.props.fetchData();
  }

  muteGroup = async () => {
    const { user, group } = this.props;
    const { chat_id } = group;
    await muteChat(user, chat_id);
    this.props.fetchData();
  }

  leaveGroup = async () => {
    const { user, group, close, fetchData } = this.props;
    const { chat_id } = group;
    await leaveGroup(user, chat_id);
    fetchData();
    close("close");
  }

  addToGroup = async (user_id) => {
    const { user, group, fetchData } = this.props;
    const { chat_id } = group;
    await addToGroup(user, chat_id, user_id);
    fetchData();
  }

  updateGroupName = async (value) => {
    const { user, group, fetchData } = this.props;
    const { chat_id } = group;
    await renameGroup(user, chat_id, value);
    fetchData();
  }

  removeFromGroup = async (user_id) => {
    const { user, group, fetchData } = this.props;
    const { chat_id } = group;
    await removeFromGroup(user, chat_id, user_id);
    fetchData();
  }

  render() {
    const { group = {}, user = {}, contacts = [] } = this.props;
    const { muted = false } = group;
    const muteIcon = muted ? 'alarm mute' : 'alarm';
    const muteMethod = muted ? this.unmuteGroup : this.muteGroup;
    const isAdmin = (group.admins || []).some(id => id === user.user_id);
    return (
      <div className="p-1 d-flex">
        {isAdmin && <div style={{margin: '0 0.75rem'}}>
          <AddGroup group={group} contacts={contacts} addToGroup={this.addToGroup} />
          <div style={{fontSize: '12px', margin: '0.25rem'}}>Add</div>
        </div>}
        <div style={{margin: '0 0.75rem'}}>
          <Button circular icon={muteIcon} onClick={muteMethod}/>
          <div style={{fontSize: '12px', margin: '0.25rem'}}>{muted ? 'Unmute' : 'Mute'}</div>
        </div>
        {isAdmin && <div style={{margin: '0 0.75rem'}}>
          <EditGroup group={group} user={user} update={this.updateGroupName} removeFromGroup={this.removeFromGroup}/>
          <div style={{fontSize: '12px', margin: '0.25rem'}}>Edit</div>
        </div>}
        <div style={{margin: '0 0.75rem'}}>
          <LeaveGroup group={group} leaveGroup={this.leaveGroup}/>
          <div style={{fontSize: '12px', margin: '0.25rem'}}>Leave</div>
        </div>
      </div>
    )
  }
}

const AddGroup = (props) => {
  const [open, setOpen] = React.useState(false);
  const { contacts = [], group = {}, addToGroup } = props;
  const { users = [] } = group;
  const newContacts = contacts.filter(contact => {
    const { user_id } = contact;
    return !users.some(user => user.user_id === user_id);
  });
  return (
    <Modal size="mini" style={{left: 'auto', height: 'auto', top: '10rem'}} onClose={() => setOpen(false)} onOpen={() => setOpen(true)} open={open} trigger={<Button circular icon='user plus' />}>
      <Modal.Header>Add To {group.group_name}</Modal.Header>
      <Modal.Content style={{margin: 0}}>
        <Modal.Description>
          <div className="text-bold-600 mb-1">Select from following contacts</div>
        </Modal.Description>
        <div>
          {newContacts.map((user, index) => (
            <div className="d-flex" key={index}>
              <div className="avatar">
                {user.avatar ?
                  <img src={user.avatar} height="40" width="40" alt="profile"/> :
                  <Avatar name={user.fullname} />
                }
              </div>
              <div className="d-flex flex-column justify-content-center text-bold-600" style={{flex: '1', paddingLeft: '0.5rem'}}>
                <div>{user.fullname}</div>
                <div style={{color: 'gray'}}>{user.status}</div>
              </div>
              <div style={{color: 'red'}} className="d-flex flex-column justify-content-center">
                <Button onClick={() => {addToGroup(user.user_id)}}>Add</Button>
              </div>
            </div>
          ))}
        </div>
      </Modal.Content>
      <Modal.Actions>
        <Button
          content="Done"
          labelPosition='right'
          icon='checkmark'
          onClick={() => {setOpen(false);}}
          positive
        />
      </Modal.Actions>
    </Modal>
  )
}

const LeaveGroup = (props) => {
  const [open, setOpen] = React.useState(false);
  return (
    <Modal size="mini" style={{left: 'auto', height: 'auto', top: '10rem'}} onClose={() => setOpen(false)} onOpen={() => setOpen(true)} open={open} trigger={<Button circular icon="sign out alternate" />}>
      <Modal.Header>Leave {props.group.group_name}</Modal.Header>
      <Modal.Content style={{margin: 0}}>
        <Modal.Description>
          <p>Are you sure you want to leave {props.group.group_name} ?</p>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button color='white' onClick={() => setOpen(false)}>
          No
        </Button>
        <Button
          content="Yes"
          labelPosition='right'
          icon='checkmark'
          onClick={() => {setOpen(false); props.leaveGroup()}}
          positive
        />
      </Modal.Actions>
    </Modal>
  )
}

const EditGroup = (props) => {
  const [open, setOpen] = React.useState(false);
  const { group = {}, user = {}, update, removeFromGroup } = props;
  const [name, setName] = React.useState(group.group_name);
  const { users = [] } = group;
  const currentUserId = user.user_id;
  return (
    <Modal size="mini" style={{left: 'auto', height: 'auto', top: '10rem'}} onClose={() => setOpen(false)} onOpen={() => setOpen(true)} open={open} trigger={<Button circular icon="compose" />}>
      <Modal.Header>Edit Group</Modal.Header>
      <Modal.Content style={{margin: 0}}>
        <div className="d-flex">
          <Input style={{flex: 1}} type="text" className="m-1" placeholder="Group Name" value={name} onChange={(e) => {setName(e.target.value)}}/>
          <div className="ml-1 d-flex flex-column justify-content-center">
            <Button  disabled={!name} onClick={() => {update(name)}}>Update</Button>
          </div>
        </div>
        <div>
          {users.map((user, index) => (
            <div className="d-flex" key={index}>
              <div className="avatar">
                {user.avatar ?
                  <img src={user.avatar} height="40" width="40" alt="profile"/> :
                  <Avatar name={user.fullname} />
                }
              </div>
              <div className="d-flex flex-column justify-content-center text-bold-600" style={{flex: '1', paddingLeft: '0.5rem'}}>
                <div>{user.fullname}</div>
                <div style={{color: 'gray'}}>{user.status}</div>
              </div>
              <div style={{color: 'red'}} className="d-flex flex-column justify-content-center">
                {user.user_id !== currentUserId && <Button type="button" onClick={() => {removeFromGroup(user.user_id)}}>Remove</Button>}
              </div>
            </div>
          ))}
        </div>
      </Modal.Content>
      <Modal.Actions>
        <Button
          content="Done"
          labelPosition='right'
          icon='checkmark'
          onClick={() => {setOpen(false)}}
          positive
        />
      </Modal.Actions>
    </Modal>
  )
}

const containerStyle = {
  display: 'flex',
  fontSize: '1.3rem',
  marginBottom: '0.75rem'
}
