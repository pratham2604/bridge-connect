import React from 'react';

export default (props) => {
  const { name = '' } = props;
  const initial = name[0];
  return (
    <span className="avatar-section">
      {initial ?
        <span className="avatar">{initial}</span> :
        <span className="user-icon icon-user-circle"></span>
      }
    </span>
  )
}