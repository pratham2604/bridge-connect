import React from "react"
import { X } from "react-feather"
import PerfectScrollbar from "react-perfect-scrollbar"
import { Input, Button } from "reactstrap"
import { renameGroup, makeAdmin } from "../../../redux/actions/chat/index"
import { connect } from "react-redux"

class AboutUser extends React.Component {
  state = {
    activeUser: {},
    groupName: '',
  }

  componentDidUpdate(prevPros) {
    const { activeUser } = this.props;
    if (prevPros.activeUser.chat_id !== activeUser.chat_id) {
      this.setState({
        activeUser,
        groupName: activeUser.group_name
      });
    } 
  }

  updateGroupName = async () => {
    const { user, activeUser } = this.props;
    const { groupName } = this.state;
    await renameGroup(user, activeUser.chat_id, groupName);
    this.props.handleReceiverSidebar("close")
  }

  makeAdmin = async (user_id) => {
    const { activeUser, user } = this.props;
    const { chat_id } = activeUser;
    await makeAdmin(user, chat_id, user_id);
    this.props.handleReceiverSidebar("close")
  }

  render() {
    const { activeUser, groupName } = this.state;

    return (
      <div
        style={{height: '100%', top: '0', right: '30px'}}
        className={`user-profile-sidebar ${
          this.props.receiverProfile ? "show" : null
        }`}
      >
        <header className="user-profile-header">
          <span
            className="close-icon"
            onClick={() => this.props.handleReceiverSidebar("close")}
          >
            <X size={24} />
          </span>
          <div className="header-profile-sidebar">
            <div className="avatar">
              {activeUser.avatar ?
                <img
                  src={activeUser !== null ? activeUser.avatar : ""}
                  height="40"
                  width="40"
                  alt="profile"
                /> :
                <Avatar name={activeUser.group_name} />
              }
              <span
                className={`${
                  activeUser !== null && activeUser.status === "do not disturb"
                    ? "avatar-status-busy"
                    : activeUser !== null && activeUser.status === "away"
                    ? "avatar-status-away"
                    : activeUser !== null && activeUser.status === "offline"
                    ? "avatar-status-offline"
                    : "avatar-status-online"
                } avatar-status-lg`}
              />
            </div>
            <div>
              <h4 className="chat-user-name mb-0">
                {activeUser !== null ? activeUser.group_name : ""}
              </h4>
              <Input type="text" className="m-1" placeholder="Group Name" value={groupName}
                onChange={e => {
                  e.preventDefault()
                  this.setState({
                    groupName: e.target.value
                  })
                }}
              />
              <Button color="primary" onClick={this.updateGroupName}>
                Update
              </Button>
            </div>
          </div>
        </header>
        <PerfectScrollbar
          className="user-profile-sidebar-area p-2"
          options={{
            wheelPropagation: false
          }}
        >
          <div>
            {(activeUser.users || []).map(user => {
              const { user_id } = user;
              const { admins } = activeUser;
              const isAdmin = admins[user_id];
              return (
                <div className="d-flex mb-1">
                  <div className="mr-1">
                    {user.avatar ?
                      <img
                        src={user !== null ? user.avatar : ""}
                        height="40"
                        width="40"
                        alt="profile"
                      /> :
                      <Avatar name={user.fullname} />
                    }
                  </div>
                  <div style={{flex: '1'}} className="d-flex flex-column justify-content-center text-bold-600">{user.fullname}</div>
                  <div className="d-flex flex-column justify-content-center mr-1">{isAdmin ? 'Admin' : 'Member'}</div>
                  <div>{!isAdmin && <Button color="primary" onClick={this.makeAdmin.bind(this, user_id)}>Admin</Button>}</div>
                </div>
              )
            })}
          </div>
          <p>{activeUser !== null ? activeUser.about : null}</p>
        </PerfectScrollbar>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {})(AboutUser);

class Avatar extends React.Component {
  render() {
    const { name = '' } = this.props;
    const initial = name[0];

    return (
      <span className="avatar-section">
        {initial ?
          <span className="avatar">{initial}</span> :
          <span className="user-icon icon-user-circle"></span>
        }
      </span>
    )
  }
}
