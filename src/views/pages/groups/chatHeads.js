import React, { Component } from 'react';
import "../../../assets/scss/pages/app-chat.scss"
import { connect } from "react-redux";
import CreateGroup from './creategroup';
import { Icon, Label } from 'semantic-ui-react';

class Index extends Component {
  state = {
    active: 0,
  }

  toggle = tab => {
    if (this.state.active !== tab) {
      this.setState({ active: tab })
    }
  }

  render() {
    const { search, chats = [] } = this.props;
    let filteredChats = chats;
    
    if (search) {
      filteredChats = (chats || []).filter(chat => {
        return chat.group_name.toLowerCase().includes(search.toLowerCase());
      })
    }
    return (
      <div>
        <CreateGroup fetchGroups={this.props.fetchGroups} />
        <ChatHeads showChat={this.props.showChat} chats={filteredChats} />
      </div>
    )
  }
}

class ChatHeads extends Component {
  render() {
    const { chats = [] } = this.props;
    return (
      <div className="chat-application mb-1" style={{overflow: 'auto'}}>
        <div className="chat-sidebar">
          <div className="sidebar-content h-100" style={{width: '100%'}}>
            <div className="chat-user-list list-group mt-0" style={{width: '100%'}} options={{ wheelPropagation: false }}>
              <ul className="chat-users-list-wrapper media-list">
                {chats.map((chat, index) => {
                  const { last_msg = {}, unread_msg_count } = chat || {};
                  const { msg, msg_type } = last_msg || {};
                  let message = {};
                  try {
                    message = JSON.parse(msg)
                  } catch(e) {
                    message = {
                      content: msg,
                    }
                  }
                  let content = '';
                  if (msg_type === 'text') {
                    content = message.content;
                  } else if (msg_type === 'image') {
                    content = <span><Icon name="image" size="small" /><span className="ml-1">Image</span></span>;
                  } else if (msg_type === 'video') {
                    content = <span><Icon name="video" size="small" /><span className="ml-1">Video</span></span>;
                  } else if (msg_type === 'file') {
                    content = <span><Icon name="file" size="small" /><span className="ml-1">File</span></span>;;
                  }
                  // let lastMsg = chats[chat.user_id] && chats[chat.user_id].msg ? chats[chat.user_id].msg.slice(-1)[0] : null,
                  //   lastMsgDate = new Date(lastMsg && lastMsg.time ? lastMsg.time : null),
                  //   lastMsgMonth = lastMsgDate.toLocaleString("default", {month: "short"}),
                  //   lastMsgDay = lastMsgDate.getDate()
                  return (
                    <li key={index}  onClick={() => { this.props.showChat(chat) }} className={`${false ? "active" : ""}`}>
                      <div className="pr-1">
                        <span className="avatar avatar-md m-0">
                          {chat.avatar ?
                            <img src={chat.avatar} height="38" width="38" alt="profile"/> :
                            <Avatar name={chat.group_name} />
                          }
                        </span>
                      </div>
                      <div className="user-chat-info">
                        <div className="contact-info">
                          <h5 className="text-bold-600 mb-0">{chat.group_name}</h5>
                          <p className="truncate">
                            {/* {lastMsg ? lastMsg.textContent : chat.about} */}
                            {content}
                          </p>
                        </div>
                        <div className="contact-meta d-flex- flex-column">
                          <span className="float-right mb-25">
                            {unread_msg_count > 0 && <Label color='teal'>{unread_msg_count}</Label>}
                            {/* {lastMsgMonth + " " + lastMsgDay} */}
                          </span>
                        </div>
                      </div>
                    </li>
                  )
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

class Avatar extends React.Component {
  render() {
    const { name = '' } = this.props;
    const initial = name[0];

    return (
      <span className="avatar-section">
        {initial ?
          <span className="avatar">{initial}</span> :
          <span className="user-icon icon-user-circle"></span>
        }
      </span>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {
  // getChats,
  // getContacts,
  // StartChat,
})(Index);