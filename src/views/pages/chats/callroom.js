import React, { Component } from "react";
import { Segment, Grid, Modal, Header } from 'semantic-ui-react';
import * as Icon from "react-feather"
import VideoStream from './videoStream';
import { callUser, endCall } from "../../../redux/actions/chat/index"
const short = require('short-uuid');
const translator = short(); 

export default class Call extends Component {
  state = {
    initialized: false,
    videoStream: {},
    remoteStreams: {},
    isAudio: true,
    isVideo: true,
    isSharingScreen: false,
    activeStream: 'local_stream',
    swapIndex: 0,
    callTime: 0,
  };

  async componentDidMount() {
    const { user, chat, callType } = this.props;
    const videoStream = new VideoStream(user.user_id, this.updateRemoteStrams, false, callType !== 'video');
    const data = {
      agora_id: user.user_id.replace(/\D/g,'').slice(0, 8),
      call_type: callType
    }
    const response = await callUser(user, chat.chat_id, data)
    const { call_id } = response;
    this.setState({
      videoStream,
      roomId: call_id,
    });
  }

  updateRemoteStrams = (remoteStreams) => {
    const isRemoteStreamPresent = Object.keys(remoteStreams).length > 0;
    this.setState({
      remoteStreams
    });
    if (isRemoteStreamPresent) {
      this.timer = setInterval(() => {
        this.setState({
          callTime: this.state.callTime + 1,
        });
      }, 1000)
    }
  }

  componentWillMount() {
    clearInterval(this.timer);
  }

  async componentDidUpdate(prevProps, prevState) {
    const { roomId } = this.state;
    if (!this.state.initialized && roomId !== "") {
      this.startRoom(roomId);
    }

    const { remoteStreams, swapVideos } = this.state;
    const isRemoteStreamPresent = Object.keys(remoteStreams).length > 0;
    if (!swapVideos && isRemoteStreamPresent) {
      this.setState({
        swapVideos: true,
        swapIndex: 1,
      })
    }

    if (swapVideos && !isRemoteStreamPresent) {
      this.setState({
        swapIndex: 0,
        swapVideos: false,
      })
    }

    const { updateFromSocket } = this.props;
    if (updateFromSocket && updateFromSocket.type === 'END_CALL') {
      const { call_id } = updateFromSocket || {};
      if (call_id === roomId) {
        const { videoStream } = this.state;
        videoStream.leaveMeeting();
        this.props.onClose();
      }
    }
  }

  startRoom = (roomId) => {
    const { videoStream } = this.state;
    const { user } = this.props;
    this.setState({
      showRoom: true,
      initialized: true,
      roomId,
    }, () => {
      videoStream.initLocalStream('local_stream', roomId, user.user_id, () => {
        const { isAudio, isVideo } = this.state;
        if (!isAudio) {
          this.toggleMic();
        }
        if (!isVideo) {
          this.toggleVideo();
        }
      })
    })
  }

  componentWillUnmount() {
    this.leaveMeeting();
  }

  leaveMeeting = async () => {
    const { videoStream, roomId } = this.state;
    videoStream.leaveMeeting();
    const { user, chat } = this.props;
    const response = await endCall(user, chat.chat_id, roomId)
    this.props.onClose();
  }

  toggleMic = () => {
    const { videoStream } = this.state;
    const isAudio = videoStream.toggleAudio();
    this.setState({
      isAudio,
    })
  }

  toggleVideo = () => {
    const { videoStream } = this.state;
    const isVideo = videoStream.toggleVideo();
    this.setState({
      isVideo,
    });
  }

  toggleShareScreen = () => {
    const { videoStream, isSharingScreen, roomId } = this.state;
    const { user } = this.props;
    videoStream.close();
    videoStream.stop();
    videoStream.leaveMeeting();
    let newStream = null;
    if (!isSharingScreen) {
      newStream = new VideoStream(user.user_id, this.updateRemoteStrams, true);
    } else {
      newStream = new VideoStream(user.user_id, this.updateRemoteStrams);
    }
    newStream.initLocalStream('local_stream', roomId, user.user_id, () => {});
    this.setState({
      videoStream: newStream,
      isSharingScreen: !isSharingScreen,
    });
  }

  toggleShareScreen = () => {
    const { videoStream, isSharingScreen, roomId } = this.state;
    const { user } = this.props;
    videoStream.close();
    videoStream.stop();
    videoStream.leaveMeeting();
    let newStream = null;
    if (!isSharingScreen) {
      newStream = new VideoStream(user.user_id, this.updateRemoteStrams, true);
    } else {
      newStream = new VideoStream(user.user_id, this.updateRemoteStrams);
    }
    newStream.initLocalStream('local_stream', roomId, user.user_id, () => {});
    this.setState({
      videoStream: newStream,
      isSharingScreen: !isSharingScreen,
    });
  }

  swapIndex = (index) => {
    this.setState({
      swapIndex: index,
    })
  }

  render() {
    const { open, onClose, chat } = this.props;
    const { isAudio, isVideo, videoStream, swapIndex, isSharingScreen, callTime } = this.state;
    const { showRoom } = this.state;
    const { remoteStreams = {} } = videoStream;
    if (!showRoom) {
      return null;
    }

    const presentVideoStreams = [];
    presentVideoStreams.push(
      <div id="local_stream" className="h-100 w-100"></div>
    );
    Object.keys(remoteStreams).forEach(key => {
      const stream = remoteStreams[key];
      const streamId = stream.getId();
      presentVideoStreams.push (
        <div key={streamId} id={`agora_remote ${streamId}`} className="h-100 w-100"/>
      );
    });
    const showVideo = this.props.callType === 'video';
    const height = showVideo ? '600px' : '230px';
    const width = showVideo ? '700px' : '350px';
    const isRemoteStreamPresent = Object.keys(remoteStreams).length > 0;
    const duration = callTime / 2;

    return (
      <Modal open={open} onClose={onClose} style={{left: '50%', top: '10%', height, width, transform: 'translateX(-50%)'}}>
        <Modal.Content className="m-0 h-100">
          <div className="d-flex flex-column position-relative h-100">
            {!showVideo &&
              <div>
                <Header as="h2" className="text-center mb-2">{chat.fullname}</Header>
                {isRemoteStreamPresent ?
                  <div className="text-center mb-1">{Math.floor(duration / 60)} : {Math.floor(duration % 60)}</div> :
                  <div className="text-center mb-1">Ringing ... </div>
                }
                <div className="chat-avatar text-center">
                  <div className="avatar m-0">
                    {chat.avatar ? <img src={chat.avatar} height="50" width="50" alt="profile"/> :
                    <Avatar name={chat.fullname} />}
                  </div>
                </div>
              </div>
            }
            {presentVideoStreams.map((stream, index) => {
              if (index === swapIndex) {
                return (
                  <div className="h-100">
                    {stream}
                  </div>
                )
              }
              const leftDistance = (100 * index) + 5
              return (
                <div className="position-absolute" style={{display: 'flex', marginTop: '1em', bottom: '70px', left: `${leftDistance}px`, zIndex: '100', width: '100px', height: '100px'}} onClick={this.swapIndex.bind(this, index)}>{stream}</div>
              )
            })}
            <Segment>
              <Grid>
                <Grid.Row className="p-1">
                  <Grid.Column width={4}>
                    
                  </Grid.Column>
                  <Grid.Column width={8}>
                    <div className="text-center">
                      {!isAudio ?
                        <Icon.MicOff size={20} className="mr-1 cursor-pointer" onClick={this.toggleMic} style={{color: 'red'}}/> :
                        <Icon.Mic size={20} className="mr-1 cursor-pointer" onClick={this.toggleMic} style={{color: 'green'}}/>
                      }
                      <Icon.PhoneOff size={20} className="mr-1 cursor-pointer" onClick={this.leaveMeeting} style={{color: 'red'}}/>
                      {!isVideo ?
                        showVideo && <Icon.VideoOff size={20} className="mr-1 cursor-pointer" onClick={this.toggleVideo} style={{color: 'red'}}/> :
                        showVideo && <Icon.Video size={20} className="mr-1 cursor-pointer" onClick={this.toggleVideo} style={{color: 'green'}}/>
                      }                  
                      {showVideo && <Icon.Airplay size={20} className="mr-1 cursor-pointer" onClick={this.toggleShareScreen} style={{color: isSharingScreen ? 'green' : 'gray'}}/>}
                    </div>
                  </Grid.Column>
                  <Grid.Column width={4}>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          </div>
        </Modal.Content>
      </Modal>
    );
  }
}

class Avatar extends React.Component {
  render() {
    const { name = '' } = this.props;
    const initial = (name || 'Unknown')[0];

    return (
      <span className="avatar-section">
        {initial ?
          <span className="avatar">{initial}</span> :
          <span className="user-icon icon-user-circle"></span>
        }
      </span>
    )
  }
}