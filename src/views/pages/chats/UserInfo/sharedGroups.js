import React from 'react';
import Avatar from './avatar';

export default (props) => {
  const { groups } = props;
  return (
    <div className="p-1" style={{borderBottom: '1px solid lightgray'}}>
      <h3 className="text-bold-600 mb-1">Shared Groups</h3>
      <div className="mb-1">
        {(groups || []).filter(group => group.is_group).map((group, index) => {
          return (
            <div className="d-flex" key={index}>
              <div className="avatar">
                <Avatar name={group.group_name} />
              </div>
              <div className="d-flex flex-column justify-content-center text-bold-600">
                {group.group_name}
              </div>
            </div>
          )
        })}
      </div>
    </div>
  )
}