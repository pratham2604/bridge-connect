import React, { Component } from "react"
import { X } from "react-feather"
import { getCommonChats, getChats, getMedia } from "../../../../redux/actions/chat/index"
import { connect } from "react-redux"
import Avatar from './avatar';
import SharedGroup from './sharedGroups';
import SharedMedia from './sharedMedia';
import Settings from './settings';

class AboutUser extends Component {
  state = {}

  async componentDidUpdate(prevPros) {
    const { activeUser, user } = this.props;
    if (activeUser.chat_id && prevPros.activeUser.chat_id !== activeUser.chat_id) {
      const groups = await getCommonChats(user, activeUser.user_id);
      const chats = await getChats(user);
      const groupData = groups.map(groupId => chats.find(chat => chat.chat_id === groupId)).filter(group => group.is_group);
      const audio = await getMedia(user, activeUser.chat_id, 'audios');
      const files = await getMedia(user, activeUser.chat_id, 'files');
      const images = await getMedia(user, activeUser.chat_id, 'images');
      const videos = await getMedia(user, activeUser.chat_id, 'videos');
      const media = {
        audio,
        files,
        images,
        videos,
      }
      this.setState({
        groups: groupData,
        media
      });
    } 
  }

  render() {
    const { groups = [], media = {} } = this.state;
    const { activeUser } = this.props;
    const containerClass = `user-profile-sidebar ${this.props.receiverProfile ? "show" : null}`;
    console.log(activeUser, "activeUser")

    return (
      <div style={{height: '100%', top: '0', right: '30px'}} className={containerClass}>
        <header className="user-profile-header" style={{borderBottom: '1px solid lightgray'}}>
          <span className="close-icon" onClick={() => this.props.handleReceiverSidebar("close")}>
            <X size={24} />
          </span>
          <div className="header-profile-sidebar">
            <div className="avatar">
              {activeUser.avatar ? <img src={activeUser.avatar} height="40" width="40" alt="profile" /> : <Avatar name={activeUser.fullname} />}
            </div>
            <div>
              <h4 className="chat-user-name mb-0">
                {activeUser.fullname}
              </h4>
            </div>
          </div>
        </header>
        <Settings
          user={this.props.user}
          person={activeUser}
          fetchData={this.props.fetchData}
        />
        {groups.length > 0 && <SharedGroup groups={groups} />}
        <SharedMedia media={media}/>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {})(AboutUser);

