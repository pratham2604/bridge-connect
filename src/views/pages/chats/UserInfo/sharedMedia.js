import React, { Component } from 'react';
import {TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap";
import { Button, Modal, Icon } from 'semantic-ui-react';
import ReactPlayer from 'react-player';
import classnames from 'classnames';
const Tabs = ['Images', 'Videos', 'Files'];

export default class extends Component {
  state = {
    active: 0,
  }

  toggle = tab => {
    if (this.state.active !== tab) {
      this.setState({ active: tab })
    }
  }

  render() {
    const { active } = this.state;
    const { media = {} } = this.props;
    const { files = [], audio = [], images = [], videos = [] } = media; 
    return (
      <div className="p-1">
        <h3 className="text-bold-600 mb-1">Shared Media</h3>
        <div className="mb-1">
          <Nav tabs className="nav-fill">
            {Tabs.map((tab, index) => {
              return (
                <NavItem key={index}>
                  <NavLink className={classnames({ active: active === index })} onClick={() => {this.toggle(index)}}>
                    {tab}
                  </NavLink>
                </NavItem>
              )
            })}
          </Nav>
            <TabContent activeTab={active}>
              <TabPane tabId={0}>
                <div>
                  {images.length > 0 ? images.map(image => {
                    const { msg } = image;
                    const message = JSON.parse(msg);
                    const { imageUrl } = message;
                    return <ImageModal src={imageUrl}/>
                  }) : <i style={{color: 'lightgray'}}>Nothing to show</i>}
                </div>
              </TabPane>
              <TabPane tabId={1}>
                <div>
                  {videos.length > 0 ? videos.map(video => {
                    const { msg } = video;
                    const message = JSON.parse(msg);
                    const { videoUrl } = message;
                    return <VideoModal src={videoUrl}/>
                  }) : <i style={{color: 'lightgray'}}>Nothing to show</i>}
                </div>
              </TabPane>
              <TabPane tabId={2}>
                <div>
                  {files.length > 0 ? files.map(file => {
                    const { msg } = file;
                    const message = JSON.parse(msg);
                    const { fileUrl, fileName } = message;
                    return (
                      <div className="mb-1">
                        <Icon name="file" size="large" className="mr-1"/>
                        <a href={fileUrl} target="_blank">{fileName}</a>
                      </div>
                    )
                  }) : <i style={{color: 'lightgray'}}>Nothing to show</i>}
                </div>
              </TabPane>
            </TabContent>
          </div>
        
      </div>
    )
  }
}

function ImageModal(props) {
  const { src } = props;
  const [open, setOpen] = React.useState(false)
  return (
    <Modal basic closeIcon onClose={() => setOpen(false)} onOpen={() => setOpen(true)} open={open} size='fullscreen'
      trigger={<img src={src} style={{height: '5rem', width: '5rem', marginRight: '5px', cursor: 'pointer'}}/>}
      style={{width: '100% !important'}}
    >
      <Modal.Content style={{margin: 'auto', textAlign: 'center'}}>
        <img src={src} />
      </Modal.Content>
    </Modal>
  )
}

function VideoModal(props) {
  const { src } = props;
  const [open, setOpen] = React.useState(false)
  return (
    <Modal basic closeIcon onClose={() => setOpen(false)} onOpen={() => setOpen(true)} open={open} size='fullscreen'
      trigger={
        <div style={{height: '5rem', width: '5rem', marginRight: '5px', cursor: 'pointer', backgroundColor: 'gray', display: 'inline-block', textAlign: 'center'}}>
          <Icon name="play" size="small" style={{top: '30%', position: 'relative'}}/>
        </div>
      }
      style={{width: '100% !important'}}
    >
      <Modal.Content style={{margin: 'auto', textAlign: 'center'}}>
        <ReactPlayer className='react-player' width='100%' height='500px' url={src} controls={true} />
      </Modal.Content>
    </Modal>
  )
}