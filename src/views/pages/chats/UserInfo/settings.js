import React, { Component } from 'react';
import { Icon, Checkbox, Modal, Button } from 'semantic-ui-react';
import { clearChat, unmuteChat, muteChat } from '../../../../redux/actions/chat/index';
import { blockUser, unblockUser } from "../../../../redux/actions/auth/loginActions";

export default class extends Component {
  clearChat = async () => {
    const { user, person } = this.props;
    const { chat_id } = person;
    await clearChat(user, chat_id);
    // this.props.fetchData();
  }

  unblockUser = async () => {
    const { user, person } = this.props;
    const { user_id } = person;
    await unblockUser(user, user_id);
    this.props.fetchData();
  }

  blockUser = async () => {
    const { user, person } = this.props;
    const { user_id } = person;
    await blockUser(user, user_id);
    this.props.fetchData();
  }

  unmuteUser = async () => {
    const { user, person } = this.props;
    const { chat_id } = person;
    await unmuteChat(user, chat_id);
    this.props.fetchData();
  }

  muteUser = async () => {
    const { user, person } = this.props;
    const { chat_id } = person;
    await muteChat(user, chat_id);
    this.props.fetchData();
  }

  onChange = (name, value) => {
    const { checked } = value;
    const method = checked ? this.muteUser : this.unmuteUser;
    method();
  }

  render() {
    const { person } = this.props;
    const { blocked = false, muted = false } = person;
    const blockMethod = blocked ? this.unblockUser : this.blockUser;
    return (
      <div className="p-1" style={{borderBottom: '1px solid lightgray'}}>
        <h3 className="text-bold-600 mb-1">Settings</h3>
        <div>
          <div style={containerStyle}>
            <div style={{flex: 1}}>Mute Notifications</div>
            <div>
              <Checkbox checked={muted} toggle style={{cursor: 'pointer'}} onChange={this.onChange}/>
            </div>
          </div>
          <div style={containerStyle}>
            <div style={{flex: 1}}>Clear chat</div>
            <div>
              <ClearChat clearChat={this.clearChat} />
            </div>
          </div>
          <div style={containerStyle}>
            <div style={{flex: 1}}>{blocked ? 'Unblock' : 'Block'} {person.fullname}</div>
            <div>
              <ToggleBlock name={person.fullname} method={blockMethod} isBlocked={blocked} />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const ToggleBlock = (props) => {
  const [open, setOpen] = React.useState(false);
  const action = props.isBlocked ? 'Unblock' : 'Block';
  const color = props.isBlocked ? 'black' : 'red';
  return (
    <Modal size="mini" style={{left: 'auto', height: 'auto', top: '10rem'}} onClose={() => setOpen(false)} onOpen={() => setOpen(true)} open={open} trigger={<Icon name="ban" style={{cursor: 'pointer', color}}/>}>
      <Modal.Header>{action} {props.name}</Modal.Header>
      <Modal.Content style={{margin: 0}}>
        <Modal.Description>
          <p>Are you sure you want to {action} {props.name} ?</p>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button color='white' onClick={() => setOpen(false)}>
          No
        </Button>
        <Button
          content="Yes"
          labelPosition='right'
          icon='checkmark'
          onClick={() => {setOpen(false); props.method()}}
          positive
        />
      </Modal.Actions>
    </Modal>
  )
}

const ClearChat = (props) => {
  const [open, setOpen] = React.useState(false)
  return (
    <Modal size="mini" style={{left: 'auto', height: 'auto', top: '10rem'}} onClose={() => setOpen(false)} onOpen={() => setOpen(true)} open={open} trigger={<Icon name="trash" style={{cursor: 'pointer'}} />}>
      <Modal.Header>Clear chat</Modal.Header>
      <Modal.Content style={{margin: 0}}>
        <Modal.Description>
          <p>Are you sure you want to clear chat ?</p>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button color='white' onClick={() => setOpen(false)}>
          No
        </Button>
        <Button
          content="Yes"
          labelPosition='right'
          icon='checkmark'
          onClick={() => {setOpen(false); props.clearChat()}}
          positive
        />
      </Modal.Actions>
    </Modal>
  )
}

const containerStyle = {
  display: 'flex',
  fontSize: '1.3rem',
  marginBottom: '0.75rem'
}
