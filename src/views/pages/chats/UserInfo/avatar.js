import React from 'react';

export default (props) => {
  const { name = '', style } = props;
  const initial = name[0];
  return (
    <span className="avatar-section">
      {initial ?
        <span className="avatar" style={style}>{initial}</span> :
        <span className="user-icon icon-user-circle"></span>
      }
    </span>
  )
}