import React, { Component } from "react"
import ReactDOM from "react-dom"
import { Input, Button } from "reactstrap"
import { MessageSquare, Menu, Send } from "react-feather"
import PerfectScrollbar from "react-perfect-scrollbar"
import { connect } from "react-redux"
import { sendMessage, getUserChat, deleteMessage, clearChat, notifyRead, StartChat, muteChat, unmuteChat, onUpload } from "../../../redux/actions/chat/index"
import { blockUser, unblockUser, getUser } from "../../../redux/actions/auth/loginActions"
import * as Icon from "react-feather"
import { Dropdown, Modal, Image } from 'semantic-ui-react';
import { Button as SemanticButton, Icon as SemanticIcon } from 'semantic-ui-react' ;
import moment from 'moment';
import Callroom from './callroom';
import ReactPlayer from 'react-player'

class ChatLog extends React.Component {
  state = {
    msg: "",
    activeUser: {},
    activeChat: null,
    chat: [],
    replyMessage: {},
    showCallRoom: false,
    showClearChatModal: false,
  }

  componentDidMount() {
    // this.fetchData();
    this.scrollToBottom();
  }

  async componentDidUpdate(prevPros) {
    this.scrollToBottom();
    const { activeUser, user, search, updateFromSocket } = this.props;
    if (prevPros.activeUser.chat_id !== activeUser.chat_id) {
      this.setState({
        activeUser,
      }, () => {
        this.fetchData(search);
      })
    }

    if (!activeUser.chat_id && activeUser.user_id) {
      const data = await StartChat(user, activeUser.user_id);
      const { chat_id } = data || {};
      activeUser.chat_id = chat_id;
      this.setState({
        activeUser,
      })
    }

    if (updateFromSocket) {
      this.fetchData(search);
      this.fetchActiveUserData();
      this.props.fetchData();
    }
  }

  fetchActiveUserData = async () => {
    const { user } = this.props;
    const { activeUser } = this.state;
    const data = await getUser(user, activeUser.user_id);
    const updatedUser = Object.assign({}, activeUser, data);
    this.setState({
      activeUser: updatedUser,
    })
  } 

  clearChat = async () => {
    const { activeUser } = this.state;
    const { user } = this.props;
    const { chat_id } = activeUser;
    await clearChat(user, chat_id);
    this.fetchData();
    this.toggleClearChatModal();
    this.props.fetchData();
  }

  unblockUser = async () => {
    const { activeUser } = this.state;
    const { user } = this.props;
    const { user_id } = activeUser;
    await unblockUser(user, user_id);
    this.fetchData();
    this.fetchActiveUserData();
    this.props.fetchData();
  }

  blockUser = async () => {
    const { activeUser } = this.state;
    const { user } = this.props;
    const { user_id } = activeUser;
    await blockUser(user, user_id);
    this.fetchData();
    this.fetchActiveUserData();
    this.props.fetchData();
  }

  unmuteUser = async () => {
    const { activeUser } = this.state;
    const { user } = this.props;
    const { chat_id } = activeUser;
    await unmuteChat(user, chat_id);
    this.fetchData();
  }

  muteUser = async () => {
    const { activeUser } = this.state;
    const { user } = this.props;
    const { chat_id } = activeUser;
    await muteChat(user, chat_id);
    this.fetchData();
  }

  deleteMessage = async (messageId, forSelf = false) => {
    const { activeUser } = this.state;
    const { user } = this.props;
    const { chat_id } = activeUser;
    await deleteMessage(user, chat_id, messageId, forSelf);
    this.fetchData();
    this.props.fetchData();
  } 

  fetchData = async (search) => {
    const { activeUser } = this.state;
    const { user, dispatch } = this.props;
    const { chat_id, last_read_msg_id } = activeUser;
    let read = false;
    const chat = await getUserChat(user, chat_id);
    const chatData = (chat || []).map(item => {
      if (item.msg_id === last_read_msg_id) {
        read = true;
      }
      return Object.assign({}, item, {
        isSent: item.sender_id === activeUser.user_id,
        user: item.sender_id === activeUser.user_id ? activeUser: user,
        read,
      })
    })
    const lastMessageId = ((chatData || [])[0] || {}).msg_id;
    console.log("calling here", lastMessageId)
    if (lastMessageId) {
      await notifyRead(user, chat_id, lastMessageId);
    }
    this.setState({
      chat: chatData,
    });
    this.props.fetchData();
    dispatch({
      type: 'RESET_SOCKET'
    }, () => {
      this.scrollToBottom();
    });
  }

  handleTime = (time_to, time_from) => {
    const date_time_to = new Date(Date.parse(time_to))
    const date_time_from = new Date(Date.parse(time_from))
    return (
      date_time_to.getFullYear() === date_time_from.getFullYear() &&
      date_time_to.getMonth() === date_time_from.getMonth() &&
      date_time_to.getDate() === date_time_from.getDate()
    )
  }

  scrollToBottom = () => {
    const chatContainer = ReactDOM.findDOMNode(this.chatArea)
    chatContainer.scrollTop = chatContainer.scrollHeight
  }

  sendMessage = async () => {
    const { activeUser } = this.state;
    const { user } = this.props;
    const { chat_id } = activeUser;
    const { newMessage } = this.state;
    if (!newMessage) {
      return;
    }
    const msg = JSON.stringify({
      content: newMessage
    })
    const data = {
      msg,
      msg_type: 'text',
    }

    const { replyMessage } = this.state;
    if (replyMessage.msg_id) {
      const { msg_id } = replyMessage;
      data.reply_to = msg_id;
    }
    const res = await sendMessage(user, data, chat_id);
    if (res.status === 'err') {
      console.log(res.err)
    }
    this.fetchData();
    this.props.fetchData();
    this.setState({
      newMessage: '',
      replyMessage: {}
    })
  }

  sendFileMessage = async (type, attachment) => {
    const { activeUser } = this.state;
    const { user } = this.props;
    const { chat_id } = activeUser;
    const content = {};
    if (type === 'image') {
      content.imageUrl = attachment.link;
    } else if (type === 'video') {
      content.videoUrl = attachment.link;
    } else if (type === 'file') {
      content.fileUrl = attachment.link;
      content.fileName = attachment.name;
    } else {
      content.content = "Invalid data";
    }
    const msg = JSON.stringify(content)
    const data = {
      msg,
      msg_type: type,
    }
    await sendMessage(user, data, chat_id);
    this.fetchData();
    this.props.fetchData();
    this.setState({
      newMessage: '',
      replyMessage: {},
      showAttachmentModal: false,
    })
  }

  setReply = (replyMessage) => {
    this.setState({
      replyMessage,
    })
  }

  closeCallRoom = () => {
    this.setState({
      showCallRoom: false,
    })
  }
  
  toggleClearChatModal = () => {
    this.setState({
      showClearChatModal: !this.state.showClearChatModal
    });
  }

  download = (uri) => {
    let link = document.createElement("a");
    link.setAttribute('download', '');
    link.setAttribute('target', '_blank');
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  toggleAttachmentModal = () => {
    this.setState({
      showAttachmentModal: !this.state.showAttachmentModal,
    });
  }

  render() {
    const { activeUser = {}, showAttachmentModal } = this.state;
    const chats = this.state.chat.map(chat => Object.assign({}, chat));
    const momdiff = moment.duration(moment().diff(moment.utc(activeUser.lastseen).local()));
    const isOnline = activeUser.lastseen && momdiff.asMinutes() <= 5;

    let renderChats = chats.reverse().map((chat, i) => {
      let renderAvatar = () => {
        return (
          <div className="chat-avatar">
            <div className="avatar m-0">
              {chat.user.avatar ? <img src={chat.user.avatar} height="40" width="40" alt="profile"/> :
              <Avatar name={chat.user.fullname} />}
            </div>
          </div>
        )
      }
      const { msg } = chat;
      let message = {};
      try {
        message = JSON.parse(msg)
      } catch(e) {
        message = {
          content: msg,
        }
      }
      const { content, videoUrl, imageUrl, fileUrl, fileName, isMetadata } = message;
      let chatMessage = '';
      if (chat.msg_type === 'text') {
        chatMessage = <span>{content}</span>;
      } else if (chat.msg_type === 'image') {
        chatMessage = <Image src={imageUrl} size="medium" />;
      } else if (chat.msg_type === 'video') {
        chatMessage =  <ReactPlayer className='react-player' width='300px' height='200px' url={videoUrl} controls={true} />;
      } else if (chat.msg_type === 'file') {
        chatMessage = <a href={fileUrl} target="_blank" rel="noopener noreferrer" download>
          <span className="mr-1" style={{color: 'white'}}>{fileName}</span>
          <SemanticIcon name="file" size="large" className="mr-1" style={{color: 'white'}}/>
        </a>;
      }
      const { reply_to } = chat;
      let showReply = false;
      let replyMessageContent = '';
      let replyUser = {};
      if (reply_to) {
        showReply = true;
        const targetChat = chats.find(chat => chat.msg_id === reply_to.msg_id) || reply_to || {};
        const { msg } = targetChat || {};
        let message = {};
        replyUser = (targetChat || {}).user || {};
        if (msg) {
          try {
            message = JSON.parse(msg)
          } catch(e) {
            message = {
              content: '',
            }
          }
          message = JSON.parse(msg) || {};
          replyMessageContent = message.content;
          if (message.imageUrl) {
            replyMessageContent = (<span><SemanticIcon name="photo" size="large" style={{marginRight: '0.5rem'}}/>Image</span>)
          } else if (message.videoUrl) {
            replyMessageContent = (<span><SemanticIcon name="video" size="large" style={{marginRight: '0.5rem'}}/>Video</span>)
          } else if (message.attachment) {
            replyMessageContent = (<span><SemanticIcon name="file" size="large" style={{marginRight: '0.5rem'}}/>Attachment</span>)
          }
        } else {
          replyMessageContent = 'Deleted Message'
        }
      }

      const callStyle = {
        textAlign: 'center',
        backgroundColor: '#afafaf',
        display: 'inline-block',
        padding: '0.5rem 1rem',
        borderRadius: '15px',
        color: '#0f0f0f',
        marginBottom: '10px',
      }
      if (isMetadata) {
        return (
          <div>
            <div style={callStyle}>
              {chatMessage}
            </div>
          </div>
        )
      }

      return (
        <React.Fragment key={i}>
          <div
            className={`chat ${
              chat.isSent ? "chat-left" : "chat-right"
            }`}>
            {renderAvatar()}
            <div className="chat-body">
              <div className="chat-content">
                {showReply &&
                  <div className="text-left p-1" style={{borderRadius: '5px', backgroundColor: '#d3d3d3'}}>
                    <div className="text-bolo-600">{replyUser.fullname}</div>
                    <div>{replyMessageContent}</div>
                  </div>
                }
                <div>
                  {!chat.isSent &&
                    <span className="mr-1">
                      {!chat.read ?
                        <Icon.Check size={15} /> :
                        <Icon.Check size={15} style={{color: '#00ffff'}}/>
                      }
                    </span>
                  }
                  {chatMessage}
                  <span className="chat-actions">
                    <Dropdown icon="chevron down" direction='left'>
                      <Dropdown.Menu>
                        {chat.msg_type === 'image' &&
                          <Dropdown.Item text="Download" onClick={(e) => {this.download(imageUrl)}}></Dropdown.Item>
                        }
                        <Dropdown.Item text="Reply" onClick={(e) => {this.setReply(chat)}}></Dropdown.Item>
                        <Dropdown.Item text="Delete For All" onClick={(e) => {this.deleteMessage(chat.msg_id)}}></Dropdown.Item>
                        <Dropdown.Item text="Delete For Me" onClick={(e) => {this.deleteMessage(chat.msg_id, true)}}></Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      )
    })

    const { replyMessage } = this.state;
    const { msg } = replyMessage || {};
    let message = {};
    let replyMessageContent = '';
    const replyUser = replyMessage.user;
    if (msg) {
      try {
        message = JSON.parse(msg)
      } catch(e) {
        message = {
          content: '',
        }
      }
      message = JSON.parse(msg) || {};
      replyMessageContent = message.content;
      if (message.imageUrl) {
        replyMessageContent = (<span><SemanticIcon name="photo" size="large" style={{marginRight: '0.5rem'}}/>Image</span>)
      } else if (message.videoUrl) {
        replyMessageContent = (<span><SemanticIcon name="video" size="large" style={{marginRight: '0.5rem'}}/>Video</span>)
      } else if (message.attachment) {
        replyMessageContent = (<span><SemanticIcon name="file" size="large" style={{marginRight: '0.5rem'}}/>Attachment</span>)
      }
    }
    const { updateFromSocket } = this.props;

    return (
      <div className="chat-app-window" style={{height: '100%'}}>
          {!activeUser.user_id &&
            <div className={`start-chat-area ${ activeUser.uid === null ? "d-none" : "d-flex"}`} style={{height: '100%'}}>
              <span className="mb-1 start-chat-icon">
                <MessageSquare size={50} />
              </span>
              <h4
                className="py-50 px-1 sidebar-toggle start-chat-text"
                onClick={() => {
                  if (this.props.mql.matches === false) {
                    this.props.mainSidebar(true)
                  } else {
                    return null
                  }
                }}>
                Start Conversation
              </h4>
            </div>
          }
          <div
            style={{height: '100%', flexDirection: 'column'}}
            className={`active-chat ${
              !activeUser.user_id ? "d-none" : "d-flex"
            }`}>
            <div className="chat_navbar">
              <header className="chat_header d-flex justify-content-between align-items-center p-1">
                <div className="d-flex align-items-center">
                  <div
                    className="sidebar-toggle d-block d-lg-none mr-1"
                    onClick={() => this.props.mainSidebar(true)}>
                    <Menu size={24} />
                  </div>
                  <div
                    className="avatar user-profile-toggle m-0 m-0 mr-1"
                    onClick={() => this.props.showProfile()}>
                      {activeUser.avatar ?
                        <img
                          src={activeUser !== null ? activeUser.avatar : ""}
                          height="40"
                          width="40"
                          alt="profilePic"
                        /> :
                        <Avatar name={activeUser.fullname} />
                      }
                    <span
                      className={`
                    ${
                      activeUser !== null && !isOnline ? "avatar-status-offline" : "avatar-status-online"
                    }
                    `}
                    />
                  </div>
                  <h6 className="mb-0">
                    <div>{activeUser !== null && activeUser.fullname ? activeUser.fullname : "Unknown User"}</div>
                    {activeUser.lastseen && activeUser.lastseen !== -1 &&
                      <div>
                        <span>Last Seen: {moment.utc(activeUser.lastseen).local().format('DD-MMM-YYYY HH:mm')}</span>
                      </div>
                    }
                  </h6>
                </div>
                <span
                  className="favorite"
                  onClick={() => {
                  }}>
                    <Button.Ripple className="mr-1 mb-1 rounded-circle pl-1 pr-1" onClick={() => {this.setState({showCallRoom: true, callType: 'video'})}}>
                      <Icon.Video size={17} />
                    </Button.Ripple>
                    <Button.Ripple className="mr-1 mb-1 rounded-circle pl-1 pr-1" onClick={() => {this.setState({showCallRoom: true, callType: 'audio'})}}>
                      <Icon.Phone size={17} />
                    </Button.Ripple>
                    <Dropdown icon="ellipsis vertical" direction='left'>
                      <Dropdown.Menu>
                        <Dropdown.Item text="Clear Chat" onClick={this.toggleClearChatModal}></Dropdown.Item>
                        {this.props.activeUser.blocked ?
                          <Dropdown.Item text="Unblock" onClick={this.unblockUser}></Dropdown.Item> :
                          <Dropdown.Item text="Block" onClick={this.blockUser}></Dropdown.Item>
                        }
                        {this.props.activeUser.muted ?
                          <Dropdown.Item text="Unmute" onClick={this.unmuteUser}></Dropdown.Item> :
                          <Dropdown.Item text="Mute" onClick={this.muteUser}></Dropdown.Item>
                        }
                      </Dropdown.Menu>
                    </Dropdown>
                </span>
              </header>
            </div>
            <PerfectScrollbar style={{flex: '1'}} className="user-chats" options={{ wheelPropagation: false}} ref={el => {this.chatArea = el }}>
              <div className="chats">{renderChats}</div>
            </PerfectScrollbar>
            <div className="chat-app-form">
              <div>
                {replyMessageContent &&
                  <div className="mb-1 p-1 d-flex" style={{backgroundColor: '#c2c6dc', borderRadius: '5px'}}>
                    <div style={{flex: '1'}}>
                      <div className="text-bolo-600">{replyUser.fullname}</div>
                      <div>{replyMessageContent}</div>
                    </div>
                    <div className="p-1 cursor-pointer" onClick={(e) => {this.setState({replyMessage: {}})}}>
                      <Icon.X size={17} />
                    </div>
                  </div>
                }
              </div>
              <form className="chat-app-input d-flex align-items-center" onSubmit={e => {e.preventDefault(); this.sendMessage();}}>
                <Input type="text" className="message mr-1 ml-50" placeholder="Type your message" value={this.state.newMessage}
                  onChange={e => {
                    e.preventDefault()
                    this.setState({
                      newMessage: e.target.value
                    })
                  }}
                />
                <Button color="primary" type="button" disabled={activeUser.blocked} className="mx-1" onClick={this.toggleAttachmentModal}>
                  <SemanticIcon name="attach" size={15} />
                </Button>
                <Button color="primary" type="submit" disabled={activeUser.blocked}>
                  <Send className="d-lg-none" size={15} />
                  <span className="d-lg-block d-none">Send</span>
                </Button>
              </form>
            </div>
          </div>
          {this.state.showCallRoom &&
            <Callroom
              open={true}
              onClose={this.closeCallRoom}
              user={this.props.user}
              chat={activeUser}
              callType={this.state.callType}
              updateFromSocket={updateFromSocket}
            />}
          {this.state.showClearChatModal && <ClearChatModal onClose={this.toggleClearChatModal} clearChat={this.clearChat} />}
          {showAttachmentModal && <AttchmentModal onClose={this.toggleAttachmentModal} sendFileMessage={this.sendFileMessage} />}
        </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    user: state.auth.user,
    updateFromSocket: state.socket.newMessage
  }
}

const typeMap = {
  fileImageInput: 'image',
  fileVideoInput: 'video',
  fileInput: 'file',
}

class AttchmentModal extends Component {
  state = {}

  onUploadClick = (ref) => {
    this.refs[ref].click();
    this.setState({
      uploadType: typeMap[ref],
    })
  }

  uploadFile() {
    const { file, uploadType } = this.state;
    this.setState({
      uploading: true,
    });
    onUpload(file, (data) => {
      this.setState({
        uploading: false,
      });
      this.props.sendFileMessage(uploadType, data);
    }, (err) => {
      console.log(err);
    });
  }

  _handleImageChange = (event) => {
    event.preventDefault();

    const reader = new FileReader();
    const file = event.target.files[0];
    const self = this;

    reader.onloadend = () => {
      self.setState({
        file,
      });
      self.uploadFile();
    };

    // To clear currently loaded file from input
    event.target.value = null;
    reader.readAsDataURL(file);
  }

  render() {
    const { onClose } = this.props;
    const { uploading } = this.state;
    return (
      <Modal open={true} onClose={onClose} className="p-1 attachment-upload-modal">
        <Modal.Content className="m-0">
          Select Attchment type
          <div className="m-auto text-center pt-1">
            {uploading ?
              <div>Uploading and sending message ... </div> :
              <SemanticButton.Group>
                <SemanticButton className="position-relative" onClick={this.onUploadClick.bind(this, 'fileImageInput')}>
                  <span>Image</span>
                  <input type="file" accept="image/*" ref="fileImageInput" className="file-input" onChange={this._handleImageChange}></input>
                </SemanticButton>
                <SemanticButton.Or />
                <SemanticButton className="position-relative" onClick={this.onUploadClick.bind(this, 'fileVideoInput')}>
                  <span>Video</span>
                  <input type="file" accept="video/*" ref="fileVideoInput" className="file-input" onChange={this._handleImageChange}></input>
                </SemanticButton>
                <SemanticButton.Or />
                <SemanticButton className="position-relative" onClick={this.onUploadClick.bind(this, 'fileInput')}>
                  <span>File</span>
                  <input type="file" ref="fileInput" className="file-input" onChange={this._handleImageChange}></input>
                </SemanticButton>
              </SemanticButton.Group>
            }
          </div>
        </Modal.Content>
      </Modal>
    )
  }
}

const ClearChatModal = (props) => {
  return (
    <Modal open={true} onClose={props.onClose} style={{height: 'auto', width: '310px', top: '35%', left: '35%'}} className="p-1">
      <Modal.Content className="m-0">
        Are you sure, you want to clear chat ?
        <div className="m-auto text-center pt-1">
          <SemanticButton.Group>
            <SemanticButton onClick={props.onClose}>Cancel</SemanticButton>
            <SemanticButton.Or />
            <SemanticButton positive onClick={props.clearChat}>Clear</SemanticButton>
          </SemanticButton.Group>
        </div>
      </Modal.Content>
    </Modal>
  )
}

class Avatar extends React.Component {
  render() {
    const { name = '' } = this.props;
    const initial = (name || 'Unknown')[0];

    return (
      <span className="avatar-section">
        {initial ?
          <span className="avatar">{initial}</span> :
          <span className="user-icon icon-user-circle"></span>
        }
      </span>
    )
  }
}

const mapDispatchtoProps = (dispatch) => {
  return {
      dispatch
  };
};

export default connect(mapStateToProps, mapDispatchtoProps)(ChatLog)
