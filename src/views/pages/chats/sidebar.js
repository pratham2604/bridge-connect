import React, { Component } from 'react';
import * as Icon from "react-feather"
import { Menu } from 'semantic-ui-react'
import {
  Form,
  FormGroup,
  Input,
} from "reactstrap"
import { Search } from "react-feather"
import ChatHeads from './chatHeads';


class FixedSidebar extends Component {
  state = {
    isLoading: false, results: [], search: ''
  }

  render() {
    const { search } = this.state
    return (
      <div className="p-1 w-100">
        <div style={{display: 'flex'}} className="mt-2">
          <h3 className="text-bold-600">
            Chats
          </h3>
          
        </div>
        <div className="mt-2">
          <Form>
            <FormGroup className="position-relative has-icon-left">
              <Input
                type="text"
                className=""
                value={search}
                onChange={e => this.setState({ search: e.target.value })}
              />
              <div className="form-control-position px-1">
                <Search size={15} />
              </div>
            </FormGroup>
          </Form>
        </div>
        <ChatHeads showChat={this.props.showChat} search={search} contacts={this.props.contacts} chats={this.props.chats} fetchData={this.props.fetchData}/>
      </div>
    )
  }
}

export default FixedSidebar;