import React, { Component } from 'react';
import {TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap"
import classnames from 'classnames';
import "../../../assets/scss/pages/app-chat.scss"
import {
  getChats,
  getContacts,
  StartChat,
  getCallLog,
} from '../../../redux/actions/chat/index';
import moment from 'moment';
import { connect } from "react-redux";
import { Icon, Label } from 'semantic-ui-react';
const Tabs = ['Chats', 'Contacts', 'Calls'];

class Index extends Component {
  state = {
    active: 0,
    dataFetched: false,
    chatHeads: [],
  }

  toggle = tab => {
    if (this.state.active !== tab) {
      this.setState({ active: tab }, () => {
        this.props.fetchData();
      })
    }
  }

  render() {
    const { active } = this.state;
    const { contacts = [], chats = [], user } = this.props;
    let filteredContacts = contacts;
    const { search } = this.props;
    if (search) {
      filteredContacts = contacts.filter(contact => {
        return contact.fullname.toLowerCase().includes(search.toLowerCase());
      })
    }
    let filteredChats = chats;

    return (
      <div>
        <Nav tabs className="nav-fill">
          {Tabs.map((tab, index) => {
            return (
              <NavItem key={index}>
                <NavLink className={classnames({ active: active === index })} onClick={() => {this.toggle(index)}}>
                  {tab}
                </NavLink>
              </NavItem>
            )
          })}
        </Nav>
        <TabContent activeTab={active}>
          <TabPane tabId={0}>
            <ChatHeads showChat={this.props.showChat} contacts={filteredChats} />
          </TabPane>
          <TabPane tabId={1}>
            <ChatHeads showChat={this.props.showChat} contacts={filteredContacts} />
          </TabPane>
          <TabPane tabId={2}>
            <CallLog user={user} contacts={filteredContacts} isActive={active === 2}/>
          </TabPane>
        </TabContent>
      </div>
    )
  }
}

class CallLog extends Component {
  state = {
    calls: [],
  }

  async componentDidMount () {
    const { user } = this.props;
    const calls = await getCallLog(user);
    this.setState({
      calls,
    })
  }

  async componentDidUpdate(prevProps) {
    const { isActive } = prevProps;
    if (this.props.isActive && (this.props.isActive !== isActive)) {
      const { user } = this.props;
      const calls = await getCallLog(user);
      this.setState({
        calls,
      });
    }
  }

  render() {
    const { contacts } = this.props;
    const { calls } = this.state;
    const callsData = calls.map(call => {
      const { call_type, end_at, started_at, status, chat_id } = call;
      const user = contacts.find(contact => contact.chat_id === chat_id) || {};
      const momdiff = moment.duration(moment(end_at).diff(moment(started_at)));
      const duration = momdiff.asMinutes();
      return Object.assign({}, {
        fullname: user.fullname,
        avatar: user.avatar,
        call_type,
        status,
        duration: `${duration} mins`,
        end_at,
        started_at,
      })
    }).reverse();
    return (
      <div className="chat-application mb-1" style={{overflow: 'auto'}}>
        <div className="chat-sidebar">
          <div className="sidebar-content h-100" style={{width: '100%'}}>
            <div className="chat-user-list list-group mt-0" style={{width: '100%'}} options={{ wheelPropagation: false }}>
              <ul className="chat-users-list-wrapper media-list">
                {callsData.map((contact, index) => {
                  return (
                    <li key={index} className="">
                      <div className="pr-1">
                        <span className="avatar avatar-md m-0">
                          {contact.avatar ?
                            <img src={contact.avatar} height="38" width="38" alt="profile"/> :
                            <Avatar name={contact.fullname} />
                          }
                        </span>
                      </div>
                      <div className="user-chat-info">
                        <div className="contact-info">
                          <h5 className="text-bold-600 mb-0">{contact.fullname}</h5>
                          <p className="truncate">
                            <span>
                              {contact.status === 'incoming' ?
                                <Icon name="arrow left" style={{color: 'green'}} /> :
                                contact.status === 'missed' ?
                                <Icon name="redo" style={{color: 'red'}}/> :
                                contact.status === 'outgoing' ?
                                <Icon name="arrow right" style={{color: 'green'}}/> : <span></span>
                              }
                            </span>
                            {moment.utc(contact.started_at).local().format('DD-MM-YY HH:mm')}
                            {/* {lastMsg ? lastMsg.textContent : chat.about} */}
                            
                          </p>
                        </div>
                        <div className="contact-meta d-flex- flex-column">
                          <span className="float-right mb-25">
                            {contact.call_type === 'video' ? <Icon name="video" /> : <Icon name="call" />}
                            {/* {lastMsgMonth + " " + lastMsgDay} */}
                          </span>
                        </div>
                      </div>
                    </li>
                  )
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

class ChatHeads extends Component {
  render() {
    const { contacts } = this.props;
    // console.log(chats)

    return (
      <div className="chat-application mb-1" style={{overflow: 'auto'}}>
        <div className="chat-sidebar">
          <div className="sidebar-content h-100" style={{width: '100%'}}>
            <div className="chat-user-list list-group mt-0" style={{width: '100%'}} options={{ wheelPropagation: false }}>
              <ul className="chat-users-list-wrapper media-list">
                {contacts.map((contact, index) => {
                  const { last_msg = {}, unread_msg_count } = contact || {};
                  const { msg, msg_type } = last_msg || {};
                  let message = {};
                  try {
                    message = JSON.parse(msg)
                  } catch(e) {
                    message = {
                      content: msg,
                    }
                  }
                  let content = '';
                  if (msg_type === 'text') {
                    content = message.content;
                  } else if (msg_type === 'image') {
                    content = <span><Icon name="image" size="small" /><span className="ml-1">Image</span></span>;
                  } else if (msg_type === 'video') {
                    content = <span><Icon name="video" size="small" /><span className="ml-1">Video</span></span>;
                  } else if (msg_type === 'file') {
                    content = <span><Icon name="file" size="small" /><span className="ml-1">File</span></span>;;
                  }

                  return (
                    <li key={index}  onClick={() => { this.props.showChat(contact) }} className={`${false ? "active" : ""}`}>
                      <div className="pr-1">
                        <span className="avatar avatar-md m-0">
                          {contact.avatar ?
                            <img src={contact.avatar} height="38" width="38" alt="profile"/> :
                            <Avatar name={contact.fullname} />
                          }
                        </span>
                      </div>
                      <div className="user-chat-info">
                        <div className="contact-info">
                          <h5 className="text-bold-600 mb-0">{contact.fullname}</h5>
                          <p className="truncate">
                            {/* {lastMsg ? lastMsg.textContent : chat.about} */}
                            {content}
                          </p>
                        </div>
                        <div className="contact-meta d-flex- flex-column">
                          <span className="float-right mb-25">
                            {unread_msg_count > 0 && <Label color='teal'>{unread_msg_count}</Label>}
                            {/* {lastMsgMonth + " " + lastMsgDay} */}
                          </span>
                        </div>
                      </div>
                    </li>
                  )
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

class Avatar extends React.Component {
  render() {
    const { name = '' } = this.props;
    const initial = (name || 'Unknown')[0];

    return (
      <span className="avatar-section">
        {initial ?
          <span className="avatar">{initial}</span> :
          <span className="user-icon icon-user-circle"></span>
        }
      </span>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {
  getChats,
  getContacts,
  StartChat,
})(Index);