import React, { Component } from 'react';
import { Modal, Header, Button, Icon } from 'semantic-ui-react';
import JoinRoom from './joinroom';
import { getUser } from '../../../redux/actions/auth/loginActions'
import { acceptCall, getCallInfo } from '../../../redux/actions/chat/index'
import tone from '../../../assets/ringtone/tone.mp3';
import Avatar from './UserInfo/avatar';

class Notification extends Component {
  state = {
    joinedCall: false,
  }

  async componentDidMount() {
    const { callInfo, user } = this.props;
    const { sender_id } = callInfo;
    const caller = await getUser(user, sender_id);
    const call = await getCallInfo(user, callInfo.call_id);
    this.setState({
      caller,
      callInfo: call,
    })
    const player = document.getElementById("ringtone-player");
    player.play();
  }

  joinCall = () => {
    const { user } = this.props;
    const { callInfo } = this.state;
    const { chat_id, call_id } = callInfo || {};
    const agora_id = parseInt(user.user_id.replace(/\D/g,'').slice(0, 8));
    const response = acceptCall(user, chat_id, call_id, agora_id);
    this.setState({
      joinedCall: true,
    });
  }

  render() {
    const { joinedCall, caller = {}, callInfo = {} } = this.state;
    const { onClose, user, socket } = this.props;
    const { call_type } = callInfo;
    const className = joinedCall ? call_type === 'audio' ? 'chat-audio-call-room-modal' : 'chat-call-room-modal' : 'chat-call-modal';

    return (
      <Modal open={true} onClose={onClose} className={className}>
        {joinedCall ?
          <JoinRoom onClose={onClose} callInfo={callInfo} user={user} call_type={call_type} socket={socket} caller={caller}/> :
          <Modal.Content className="m-0 h-100">
            <div>
              <div className="text-center">
                <Avatar name={caller.fullname} style={{height: '5rem', width: '5rem', fontSize: '4rem', lineHeight: '5rem'}}/>
              </div>
              <Header as="h2" className="text-bold-600 text-center">
                {caller.fullname} is {call_type} calling you
              </Header>
              <div className="mx-auto text-center pt-1 mb-1">
                <Button icon color="blue" className="mr-1" onClick={this.joinCall}>
                  <Icon name="phone" />
                </Button>
                <Button icon color="red" onClick={onClose}>
                  <Icon name="times" />
                </Button>
                <audio id="ringtone-player">
                  <source src={tone} type="audio/mpeg" />
                </audio>
              </div>
            </div>
          </Modal.Content>
        }
      </Modal>
    )
  }
}

export default Notification;