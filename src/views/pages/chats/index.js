import React from "react"
import Sidebar from "react-sidebar"
import { ContextLayout } from "../../../utility/context/Layout"
import "../../../assets/scss/pages/app-email.scss"
import FixedSidebar from './sidebar';
import ChatBox from './chatBox';
import ReceiverSidebar from './UserInfo/index';
import { connect } from "react-redux";
import { getUser } from '../../../redux/actions/auth/loginActions'
import {
  getChats,
  getContacts,
  StartChat,
  getContactsList,
} from '../../../redux/actions/chat/index'
const mql = window.matchMedia(`(min-width: 992px)`)

class Index extends React.Component {
  state = {
    composeMailStatus: false,
    sidebarDocked: mql.matches,
    sidebarOpen: false,
    contact: {},
    showProfile: false,
    contacts: [],
    chats: [],
  }

  handleComposeSidebar = status => {
    if (status === "open") {
      this.setState({
        composeMailStatus: true
      })
    } else {
      this.setState({
        composeMailStatus: false
      })
    }
  }

  UNSAFE_componentWillMount() {
    mql.addListener(this.mediaQueryChanged)
  }

  componentWillUnmount() {
    mql.removeListener(this.mediaQueryChanged)
  }

  onSetSidebarOpen = open => {
    this.setState({ sidebarOpen: open })
  }

  mediaQueryChanged = () => {
    this.setState({ sidebarDocked: mql.matches, sidebarOpen: false })
  }

  handleMainAndComposeSidebar = () => {
    this.handleComposeSidebar("close")
    this.onSetSidebarOpen(false)
  }

  showChat = (contact) => {
    this.setState({
      contact,
    })
  }

  showProfile = () => {
    this.setState({
      showProfile: !this.state.showProfile
    })
  }

  async componentDidMount() {
    this.fetchData();
  }

  fetchData = async() => {
    const { user } = this.props;
    if (user.token) {
      const chats = await getChats(user);
      const contacts = await getContactsList(user);
      const filteredChats = (chats || []).filter(chat => !chat.is_group).filter((chat) => {
        return !!(chat.last_msg && chat.last_msg.msg)
      });
      const userIds = contacts.map(contact => contact);
      filteredChats.forEach(chat => {
        const { users } = chat;
        users.forEach(userId => {
          if (userId !== user.user_id && !userIds.some(id => id === userId)) {
            userIds.push(userId);
          }
        })
      })

      const users = await Promise.all(userIds.map(userId => getUser(user, userId)));
      const updatedUsers = users.map(user => {
        const chat = chats.find(chat => !chat.is_group && chat.users.some(userId => userId === user.user_id)) || {};
        return Object.assign({}, user, chat);
      }).filter(currentUser => currentUser.user_id !== user.user_id)

      const updatedChats = filteredChats.map(chat => {
        const userId = (chat.users || []).find(id => id !== user.user_id);
        const chatUser = (users || []).find(usr => usr.user_id === userId) || {};
        return Object.assign({}, chatUser, chat);
      });
      const activeUser = updatedUsers.find(contact => contact.user_id === this.state.contact.user_id) || this.state.contact || {};

      this.setState({
        contacts: updatedUsers,
        chats: updatedChats || [],
        contact: activeUser,
      })
    }
  }

  render() {
    const { contacts, chats } = this.state;
    return (
      <React.Fragment>
        <div className="custom-fixed-sidebar-container position-relative" style={{height: '100%'}}>
          <div
            className={`app-content-overlay ${
              this.state.composeMailStatus || this.state.sidebarOpen ? "show" : ""
            }`}
            onClick={this.handleMainAndComposeSidebar}
          />
          <ContextLayout.Consumer>
            {context => (
              <Sidebar
                sidebar={
                  <FixedSidebar showChat={this.showChat} contacts={contacts} chats={chats} fetchData={this.fetchData}/>
                }
                docked={this.state.sidebarDocked}
                open={this.state.sidebarOpen}
                sidebarClassName="sidebar-content custom-app-sidebar d-flex"
                touch={false}
                contentClassName="sidebar-children"
                pullRight={context.state.direction === "rtl"}>
                <div className="chat-application" style={{height: '100%'}}>
                  <ChatBox activeUser={this.state.contact} showProfile={this.showProfile} fetchData={this.fetchData}/>
                  <ReceiverSidebar
                    activeUser={this.state.contact}
                    receiverProfile={this.state.showProfile}
                    handleReceiverSidebar={this.showProfile}
                    fetchData={this.fetchData}
                  />
                </div>
              </Sidebar>
            )}
          </ContextLayout.Consumer>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {
  getChats,
  getContacts,
  StartChat,
})(Index);
