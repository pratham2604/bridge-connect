import React from "react"
import "../../../assets/scss/pages/app-email.scss"
import { connect } from "react-redux";
import { logout } from "../../../redux/actions/auth/loginActions"

class Index extends React.Component {
  state = {
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(logout());
  }

  render() {
    return (
      <React.Fragment>
        Logging out ...
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

const mapDispatchtoProps = (dispatch) => {
  return {
    dispatch
  };
};

export default connect(mapStateToProps, mapDispatchtoProps)(Index);
