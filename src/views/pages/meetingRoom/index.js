import React from "react";
import JoinPanel from './joinPanel';
import MeetingRoom from './meetingRoom';
import { connect } from "react-redux";

class Index extends React.Component {
  state = {
    audio: true,
    video: true,
    joinMeeting: false,
    meetingInfo: {},
  }

  toggle = (name) => {
    this.setState({
      [name]: !this.state[name],
    })
  }

  joinMeeting = (meetingId, meetingInfo) => {
    this.setState({
      joinMeeting: true,
      meetingId,
      meetingInfo,
    })
  }

  render() {
    const { video, audio, meetingId, joinMeeting, meetingInfo } = this.state;
    const { user, dispatch } = this.props;
    return (
      <React.Fragment>
        <div className="custom-fixed-sidebar-container position-relative" style={{height: '100%'}}>
          {joinMeeting ?
            <MeetingRoom meetingId={meetingId} user={user} audio={audio} video={video} dispatch={dispatch} meetingInfo={meetingInfo}/> :
            <JoinPanel user={user} audio={audio} video={video} toggle={this.toggle} joinMeeting={this.joinMeeting}/>
          }
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(Index);
