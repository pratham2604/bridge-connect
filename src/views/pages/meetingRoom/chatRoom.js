import React, { Component } from "react";
import { Tab, Icon, Button, Input } from 'semantic-ui-react';
import { getMeetingInfo, updateMeetingInfo } from "../../../redux/actions/meeting/index"
import { getUser } from '../../../redux/actions/auth/loginActions'
import ChatBox from './chatbox';

export default class ChatRoom extends Component {
  state = {
    active: 1,
    editMode: false,
  }

  async componentDidMount() {
    const { meetingId, user, dispatch } = this.props;
    const meetingInfo = await getMeetingInfo(user, meetingId);
    const { title, host_id } = meetingInfo || {};
    const isHost = host_id === user.user_id;
    this.setState({
      title,
      isHost,
    })
  }

  update = async () => {
    const { meetingId, user } = this.props;
    const data = {
      title: this.state.title,
    }
    const meetingInfo = await updateMeetingInfo(user, meetingId, data);
    const { title } = meetingInfo || {};
    this.setState({
      title,
      editMode: false,
    })
  }

  panes = [
    {
      menuItem: 'Members',
      render: () => (
        <Tab.Pane>
          <Members meetingId={this.props.meetingId} user={this.props.user} isHost={this.state.isHost}/>
        </Tab.Pane>
      ),
    },
    {
      menuItem: 'Chat',
      render: () => (
        <Tab.Pane>
          <ChatBox meetingId={this.props.meetingId} user={this.props.user} />
        </Tab.Pane>
      ),
    },
  ]

  render() {
    const { title, editMode, isHost } = this.state;
    return (
      <div>
        {isHost ?
          <div className="mb-1">
            {editMode ?
              <div>
                <Input value={title} onChange={(e) => {this.setState({title: e.target.value})}} />
                <Button onClick={this.update} className="ml-1">
                  Save
                </Button>
              </div> :
              <div>
                <span className="text-bold-600 mr-1">{title}</span>
                <Button icon onClick={() => {this.setState({editMode: true})}}>
                  <Icon name="pencil"/>
                </Button>
              </div>
            }
          </div> :
          <div className="mb-1">
            <span className="text-bold-600 mr-1">{title}</span>
          </div>
        }
        <Tab panes={this.panes} />
      </div>
    )
  }
}

class Members extends Component {
  state = {
    members: [],
  }

  makeHost = async (userId) => {
    const { meetingId, user } = this.props;
    const data = {
      host_id: userId,
    }
    const meetingInfo = await updateMeetingInfo(user, meetingId, data);
    console.log(meetingInfo)
    const { users = {}, host_id } = meetingInfo || {};
    const allowedUsers = Object.keys(users).map(key => users[key]).filter(userId => userId) || [];
    if (!allowedUsers.some(userId => userId === host_id)) {
      allowedUsers.push(host_id)
    }
    const members = await Promise.all(allowedUsers.map(userId => getUser(user, userId)));
    this.setState({
      members,
      host_id,
    })
  }

  async componentDidMount() {
    const { meetingId, user } = this.props;
    const meetingInfo = await getMeetingInfo(user, meetingId);
    const { users = {}, host_id } = meetingInfo || {};
    const allowedUsers = Object.keys(users).map(key => users[key]).filter(userId => userId) || [];
    if (!allowedUsers.some(userId => userId === host_id)) {
      allowedUsers.push(host_id)
    }
    const members = await Promise.all(allowedUsers.map(userId => getUser(user, userId)));
    this.setState({
      members,
      host_id,
    })
  }

  render() {
    const { isHost } = this.props;
    const { members, host_id } = this.state;
    return (
      <div>
        {members.map((member, index) => {
          return (
            <div className="d-flex mb-1">
              <div className="pr-1">
                <span className="avatar avatar-md m-0">
                  {member.avatar ?
                    <img src={member.avatar} height="38" width="38" alt="profile"/> :
                    <Avatar name={member.fullname} />
                  }
                </span>
              </div>
              <div className="user-chat-info d-flex justify-content-center" style={{flexDirection: 'column'}}>
                <h5 className="text-bold-600 mb-0">
                  {member.fullname}
                  {member.user_id === host_id && <span className="ml-1 text-bold-400 mr-1">(Host)</span>}
                  {isHost && member.user_id !== host_id && <Button onClick={this.makeHost.bind(this, member.user_id)}>Make Host</Button>}
                </h5>
              </div>
            </div>
          )
        })}
      </div>
    )
  }
}

class Avatar extends React.Component {
  render() {
    const { name = '' } = this.props;
    const initial = name[0];

    return (
      <span className="avatar-section">
        {initial ?
          <span className="avatar" style={{padding: '1px 7px'}}>{initial}</span> :
          <span className="user-icon icon-user-circle"></span>
        }
      </span>
    )
  }
}
