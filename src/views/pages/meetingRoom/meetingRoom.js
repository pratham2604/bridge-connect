import React, { Component } from "react";
import { Segment, Grid } from 'semantic-ui-react';
import * as Icon from "react-feather"
import VideoStream from './videoStream';
import { history } from "../../../history";
import ChatRoom from './chatRoom';
import { muteRemoteStream } from "../../../redux/actions/meeting/index";

const localStream = {
  getId: function() {
    return 'local_stream';
  }
}

class Call extends Component {
  state = {
    initialized: false,
    isScreenStreamInitialized: false,
    videoStream: {},
    remoteStreams: {},
    isAudio: this.props.audio,
    isVideo: this.props.video,
    remoteShareScreenStreams: {},
    isSharingScreen: false,
    activeStream: 'local_stream',
    swapIndex: 0,
    showChatBox: true,
    swapVideos: false,
  };

  componentDidMount() {
    const { user } = this.props;
    const videoStream = new VideoStream(user.user_id, this.updateRemoteStrams);
    this.setState({
      videoStream,
    });
  }

  updateRemoteStrams = (remoteStreams) => {
    this.setState({
      remoteStreams
    })
  }

  updateRemoteShareScreenStreams = (remoteShareScreenStreams) => {
    this.setState({
      remoteShareScreenStreams,
    })
  } 

  async componentDidUpdate(prevProps, prevState) {
    const { meetingId } = this.props;
    if (!this.state.initialized && meetingId !== "") {
      this.startRoom(meetingId);
    }

    const { remoteStreams, swapVideos } = this.state;
    const isRemoteStreamPresent = Object.keys(remoteStreams).length > 0;
    if (!swapVideos && isRemoteStreamPresent) {
      this.setState({
        swapVideos: true,
        swapIndex: 1,
      })
    }

    if (swapVideos && !isRemoteStreamPresent) {
      this.setState({
        swapIndex: 0,
        swapVideos: false,
      })
    }
  }

  startRoom = (roomId) => {
    const { videoStream } = this.state;
    const { user } = this.props;
    this.setState({
      showRoom: true,
      initialized: true,
      roomId,
    }, () => {
      videoStream.initLocalStream('local_stream', roomId, user.user_id, () => {
        const { isAudio, isVideo } = this.state;
        if (!isAudio) {
          this.toggleMic();
        }
        if (!isVideo) {
          this.toggleVideo();
        }
      })
    })
  }

  leaveMeeting = () => {
    const { videoStream } = this.state;
    videoStream.leaveMeeting();
    // this.props.leaveMeeting();
    history.push('/meetings')
  }

  toggleMic = () => {
    const { videoStream } = this.state;
    const isAudio = videoStream.toggleAudio();
    this.setState({
      isAudio,
    })
  }

  toggleVideo = () => {
    const { videoStream } = this.state;
    const isVideo = videoStream.toggleVideo();
    this.setState({
      isVideo,
    });
  }

  toggleShareScreen = () => {
    const { videoStream, isSharingScreen, roomId } = this.state;
    const { user } = this.props;
    videoStream.close();
    videoStream.stop();
    videoStream.leaveMeeting();
    let newStream = null;
    if (!isSharingScreen) {
      newStream = new VideoStream(user.user_id, this.updateRemoteStrams, true);
    } else {
      newStream = new VideoStream(user.user_id, this.updateRemoteStrams);
    }
    newStream.initLocalStream('local_stream', roomId, user.user_id, () => {});
    this.setState({
      videoStream: newStream,
      isSharingScreen: !isSharingScreen,
    });
  }

  swapIndex = (index) => {
    this.setState({
      swapIndex: index,
    })
  }

  toggleChat = () => {
    this.setState({
      showChatBox: !this.state.showChatBox
    })
  }

  muteRemoteStream = async (data) => {
    const { user, meetingId } = this.props;
    await muteRemoteStream(user, meetingId, data);
  }

  render() {
    const { isAudio, isVideo, videoStream , isSharingScreen, swapIndex, showChatBox } = this.state;
    const { roomId, meetingId, meetingInfo, user } = this.props;
    const { showRoom } = this.state;
    const { remoteStreams = {} } = videoStream;
    if (!showRoom) {
      return null;
    }

    const presentVideoStreams = [];
    presentVideoStreams.push({
      stream: localStream,
      element: <div id="local_stream" className="h-100 w-100"></div>
    });
    Object.keys(remoteStreams).forEach(key => {
      const stream = remoteStreams[key];
      const streamId = stream.getId();
      presentVideoStreams.push ({
        stream,
        element: <div key={streamId} id={`agora_remote ${streamId}`} className="h-100 w-100"/>
      });
    });
    let videoIndex = 0;

    return (
      <Grid className="h-100 m-1">
        <Grid.Column width={showChatBox ? 12 : 16}>
        <div className="d-flex flex-column position-relative h-100">
          {presentVideoStreams.map((streamElement, index) => {
            const { stream, element } = streamElement;
            if (index !== swapIndex) {
              videoIndex++;
            }
            return (
              <VideoStreamComponent 
                videoIndex={videoIndex - 1} 
                user={user} 
                meetingInfo={meetingInfo} 
                index={index} 
                key={index} 
                element={element} 
                stream={stream} 
                swapIndex={swapIndex} 
                onSwapIndex={this.swapIndex} 
                muteRemoteStream={this.muteRemoteStream}
              />
            )
          })}
          <Segment>
            <Grid>
              <Grid.Row className="p-1">
                <Grid.Column width={4}>
                  <h3 className="text-bold-600 ml-1">{roomId}</h3>
                </Grid.Column>
                <Grid.Column width={8}>
                  <div className="text-center">
                    {!isAudio ?
                      <Icon.MicOff size={20} className="mr-1 cursor-pointer" onClick={this.toggleMic}/> :
                      <Icon.Mic size={20} className="mr-1 cursor-pointer" onClick={this.toggleMic}/>
                    }
                    <Icon.PhoneOff size={20} className="mr-1 cursor-pointer" onClick={this.leaveMeeting}/>
                    {!isVideo ?
                      <Icon.VideoOff size={20} className="mr-1 cursor-pointer" onClick={this.toggleVideo}/> :
                      <Icon.Video size={20} className="mr-1 cursor-pointer" onClick={this.toggleVideo}/>
                    }                  
                    <Icon.Airplay size={20} className="mr-1 cursor-pointer" onClick={this.toggleShareScreen} style={{color: isSharingScreen ? 'green' : 'black'}}/>
                  </div>
                </Grid.Column>
                <Grid.Column width={4}>
                  <Icon.MessageSquare size={20} className="float-right mr-1 cursor-pointer" onClick={this.toggleChat}/>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Segment>
        </div>
        </Grid.Column>
        {showChatBox &&
          <Grid.Column width={4} className="h-100">
            <Segment className="h-100">
              <ChatRoom meetingId={meetingId} user={this.props.user} dispatch={this.props.dispatch}/>
            </Segment>
          </Grid.Column>
        }
      </Grid>
    );
  }
}

class VideoStreamComponent extends Component {
  render() {
    const { index, stream, element, swapIndex, onSwapIndex, meetingInfo = {}, user, videoIndex, muteRemoteStream } = this.props;
    const { members = [], users, host_id } = meetingInfo;
    const streamId = stream.getId();
    const member = members.find(member => {
      const { user_id = '' } = member;
      const agora_id = Object.keys(users).find(agoraId => users[agoraId] === user_id);

      if (streamId === 'local_stream') {
        member.username = `${member.fullname} (You)`
        return user.user_id === user_id;
      }

      member.username = member.fullname;
      return parseInt(agora_id) === parseInt(streamId);
    }) || {};

    const muteAudioStreamData = {
      user_id: member.user_id,
      media_type: 'audio',
      status: 'off'
    }

    const muteVideoStreamData = {
      user_id: member.user_id,
      media_type: 'video',
      status: 'off'
    }

    if (index === swapIndex) {
      return (
        <div className="h-100 local-stream-container position-relative">
          {element}
          <div className="remote-stream-overlay">
            <div className="d-flex flex-column h-100">
              <div className="text-bold-600 p-1" style={{backgroundColor: 'white'}}>
                {member.username}
              </div>
              <div style={{flex: 1}} className="text-center d-flex flex-column justify-content-center align-items-center">
              </div>
              {streamId !== 'local_stream' &&
                <div className="text-center p-1" style={{backgroundColor: 'white'}}>
                  <Icon.Mic className="mr-1" onClick={() => muteRemoteStream(muteAudioStreamData)}></Icon.Mic>
                  <Icon.Video onClick={() => muteRemoteStream(muteVideoStreamData)}></Icon.Video>
                </div>
              }
            </div>
          </div>
        </div>
      )
    }

    const left = `${(150 * (videoIndex)) + 5}px`;
    return (
      <div className="remote-stream-container" style={{left}}>
        {element}
        <div className="remote-stream-overlay">
          <div className="d-flex flex-column h-100">
            <div style={{backgroundColor: 'white', padding: '0.5rem'}}>
              {member.username}
            </div>
            <div onClick={() => {onSwapIndex(index)}} style={{flex: 1}} className="text-center d-flex flex-column justify-content-center align-items-center">
              <Icon.Eye></Icon.Eye>
            </div>
            {streamId !== 'local_stream' && 
              <div className="text-center" style={{backgroundColor: 'white', padding: '0.5rem'}}>
                <Icon.Mic className="mr-1" onClick={() => muteRemoteStream(muteAudioStreamData)}></Icon.Mic>
                <Icon.Video onClick={() => muteRemoteStream(muteVideoStreamData)}></Icon.Video>
              </div>
            }
          </div>
        </div>
      </div>
    )
  }
}

export default Call;