import React, { Component } from 'react';
import { Modal, Header, Button, Icon } from 'semantic-ui-react';
import { getUser } from '../../../redux/actions/auth/loginActions'
import { getMeetingInfo, rejectUser, allowUsers } from "../../../redux/actions/meeting/index"

class Notification extends Component {
  state = {
  }

  async componentDidMount() {
    const { meetingInfo, user } = this.props;
    const { user_id, meeting_id } = meetingInfo;
    const requestingUser = await getUser(user, user_id);
    const meeting = await getMeetingInfo(user, meeting_id);
    this.setState({
      requestingUser,
      meeting,
    })
  }

  allowUser = async () => {
    const { user } = this.props;
    const { meeting, requestingUser } = this.state;
    await allowUsers(user, meeting.id, requestingUser.user_id);
    this.props.onClose();
  }

  rejectUser = async () => {
    const { user } = this.props;
    const { meeting, requestingUser } = this.state;
    await rejectUser(user, meeting.id, requestingUser.user_id);
    this.props.onClose();
  }

  render() {
    const { meeting = {}, requestingUser = {} } = this.state;
    const { onClose } = this.props;
    console.log(meeting)

    return (
      <Modal open={true} onClose={onClose} className="chat-call-modal">
        <Modal.Content className="m-0 h-100">
          <div>
            <Header as="h2" className="text-bold-600 text-center">
              {requestingUser.fullname} wants to join meeting
            </Header>
            <div className="mx-auto text-center pt-1 mb-1">
              <Button icon color="blue" className="mr-1" onClick={this.allowUser}>
                <Icon name="phone" />
              </Button>
              <Button icon color="red" onClick={this.rejectUser}>
                <Icon name="times" />
              </Button>
            </div>
          </div>
        </Modal.Content>
      </Modal>
    )
  }
}

export default Notification;