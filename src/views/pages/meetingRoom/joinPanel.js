import React from "react";
import { Grid, Segment, Icon, Button } from 'semantic-ui-react';
import { history } from "../../../history";
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { getMeetingInfo, requestJoinMeeting, addToMeeting } from "../../../redux/actions/meeting/index"
import { getUser } from '../../../redux/actions/auth/loginActions'

const statusTextMap = {
  'not_requested': 'Join Meeting ?',
  'pending': 'Requested',
  'rejected': 'Meeting Declined'
}
class Index extends React.Component {
  state = {
    inviteStatus: 'not_requested'
  }

  timer = null

  toggle = (name) => {
    this.setState({
      [name]: !this.state[name],
    })
  }

  leaveMeeting = () => {
    history.push('/meetings');
  }

  async componentDidMount() {
    const { pathname } = window.location;
    const meetingId = pathname.split('/').pop();
    const { user } = this.props;
    const meetingInfo = await getMeetingInfo(user, meetingId);
    const { title, short_id, call_link } = meetingInfo || {};
    console.log(meetingInfo)
    this.setState({
      meetingTitle: title,
      short_id,
      call_link,
    })
  }

  joinMeeting = async () => {
    const { pathname } = window.location;
    const meetingId = pathname.split('/').pop();
    const { user } = this.props;
    const meetingInfo = await getMeetingInfo(user, meetingId);
    const { host_id, users = {} } = meetingInfo || {};
    const allowed_users = Object.keys(users).map(key => users[key]).filter(userId => userId) || [];
    const members = await Promise.all(allowed_users.map(userId => getUser(user, userId)));
    meetingInfo.members = members;
    const isCurrentUserAllowed = allowed_users.some(userId => userId === user.user_id);
    if (isCurrentUserAllowed) {
      this.redirectUser(meetingId, meetingInfo);
      return;
    }

    if (host_id === user.user_id && !isCurrentUserAllowed) {
      await addToMeeting(user, meetingId);
      this.redirectUser(meetingId, meetingInfo);
    } else {
      const request = await requestJoinMeeting(user, meetingId);
      this.setState({
        inviteStatus: request.msg
      });
      if (request.msg === 'pending') {
        this.timer = setInterval(async() => {
          const request = await requestJoinMeeting(user, meetingId);
          if (request.msg !== 'pending' && request.msg !== 'rejected') {
            await addToMeeting(user, meetingId);
            clearInterval(this.timer);
            this.redirectUser(meetingId, meetingInfo);
          } else if (request.msg === 'rejected') {
            clearInterval(this.timer);
            this.setState({
              inviteStatus: request.msg
            });
          } else {
            this.setState({
              inviteStatus: request.msg
            });
          }
        }, 500);
      } else if (request.status === 'ok') {
        if (!isCurrentUserAllowed) {
          await addToMeeting(user, meetingId);
        }
        this.redirectUser(meetingId, meetingInfo);
      } else if (request.msg === 'rejected') {
        this.setState({
          inviteStatus: request.msg
        });
      } else {
        // window.alert('Invalid meeting');
      }
    }
  }

  redirectUser = (meetingId, meetingInfo) => {
    this.props.joinMeeting(meetingId, meetingInfo);
  }

  render() {
    const { meetingTitle, short_id, call_link } = this.state;
    const { video, audio, toggle } = this.props;
    const audioColor = audio ? "blue" : "red";
    const videoColor = video ? "blue" : "red";
    const url = window.location.href;
    const copyColor = this.state.copied ? "blue" : "black";

    return (
      <Grid centered>
        <Grid.Column width={10}>
          <Segment className="mt-4">
            <h1 className="text-center">{meetingTitle}: {statusTextMap[this.state.inviteStatus]}</h1>
            <div className="text-center mt-2 d-flex justify-content-center">
              <div className="mx-1">
                <Icon name="phone" size="big" className="cursor-pointer" color={audioColor} onClick={() => toggle('audio')}/>
                <div>{audio? 'With Audio' : 'Without Audio'}</div>
              </div>
              <div className="mx-1">
                <Icon name="video camera" size="big" className="cursor-pointer" color={videoColor} onClick={() => toggle('video')}/>
                <div>{video? 'With Video' : 'Without Video'}</div>
              </div>
            </div>
            <div className="text-center my-2">
              <Button color="blue" className="mx-1 cursor-pointer" onClick={this.joinMeeting}>Join Meeting</Button>
              <Button color="red" className="mx-1 cursor-pointer" onClick={this.leaveMeeting}>Cancel</Button>
            </div>
            <div className="text-center mt-2 mb-1">
              <span>
                <b>Invite to meeting</b> : {url}
                <span className="ml-1">
                  <CopyToClipboard text={url} onCopy={() => this.setState({copied: true})}>
                    <Icon name="copy outline" size="large" className="cursor-pointer mr-1" color={copyColor}/>
                  </CopyToClipboard>
                </span>
              </span>
            </div>
            <div className="text-center">OR</div>
            <div className="text-center mt-1 mb-1">
              <span>
                <b>Invite using code</b> : {short_id}
                <span className="ml-1">
                  <CopyToClipboard text={short_id} onCopy={() => this.setState({copied: true})}>
                    <Icon name="copy outline" size="large" className="cursor-pointer mr-1" color={copyColor}/>
                  </CopyToClipboard>
                </span>
              </span>
            </div>
            <div className="text-center">OR</div>
            <div className="text-center mt-1 mb-1">
              <span>
                <b>Invite to meeting</b> : {call_link}
                <span className="ml-1">
                  <CopyToClipboard text={call_link} onCopy={() => this.setState({copied: true})}>
                    <Icon name="copy outline" size="large" className="cursor-pointer mr-1" color={copyColor}/>
                  </CopyToClipboard>
                </span>
              </span>
            </div>
          </Segment>
        </Grid.Column>
      </Grid>
    )
  }
}

export default Index
