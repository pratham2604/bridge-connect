import React, { Component } from 'react';
import { Container, Header, Grid } from 'semantic-ui-react';
import MenuBar from './menu';
import Footer from './footer';
import '../../../assets/scss/pages/home.scss';

const style = {
  color: '#434343',
  fontSize: '1.3rem',
  lineHeight: '1.4'
}

const headingStyle = {
  color: '#434343',
  fontSize: '2.5rem',
  lineHeight: '1.4',
  fontWeight: 'bold'
}

export default class extends Component {
  render() {
    return (
      <div className="home-container">
        <MenuBar fixed />
        <div style={{height: '4em'}}></div>
        <Container>
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <Header as="h1" style={headingStyle} className="mb-2 mt-2">PRIVACY POLICY</Header>
                <div style={style} className="mb-1">
                Thank you for using Bridge Connect services and features provided to you from the Website and the Application (collectively, the “Service”).
                </div>
                <div style={style} className="mb-1">
                Bridge Connect is a seamless networking application that brings in a secure and encrypted platform for all your personal, social and professional communications.
                </div>
                <div style={style} className="mb-1">
                The platform allows you to not only connect via chat, audio call or video calls but also conduct group meetings and webinars from a single application.
                </div>
                <div style={style} className="mb-1">
                Bridge Connect is committed to protecting your privacy and ensuring you have a delightful experience on our website and when you use our Service through the App.
                </div>
                <div style={style} className="mb-1">
                This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' is being used online. Personal information/ Privacy information is mentioned in the Information Technology Act, 2000 and the (Indian) Contract Act, 1872.
                </div>
                <div style={style} className="mb-1">
                Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our Service. This policy may be updated from time to time for reasons such as operational practices or regulatory changes, so we recommend that you review our Privacy Policy when returning to our Service.
                </div>
                <div style={style} className="mb-1 text-bold-600">
                Please note: Registering on Bridge Connect or continuing to use our Service indicates that you agree to the terms of use and privacy policy.
                </div>
                <div style={style} className="mb-1 text-bold-600">
                Collection of your Personal Data
                </div>
                <div style={style} className="mb-1">
                When you have a Bridge Connect account, we may collect Personal Data from or about you when you use or otherwise interact with our Service. We may gather the following categories of Personal Data about you:
                </div>
                <div className="mb-1">
                  <ul>
                    <li style={style}>
                    Information commonly used to identify you, such as your name, email address, phone numbers, and other similar identifiers
                    </li>
                    <li style={style}>
                    To help you streamline your communication with others, we may create a favorites list of your contacts for you, and you can create, join, or get added to groups and broadcast lists, and such groups and lists get associated with your account information.
                    </li>
                    <li style={style}>
                    Credit/debit card or other payment information
                    </li>
                    <li style={style}>
                    Information about your device, network, and internet connection, such as your IP address(es), MAC address, other device ID (UDID), device type, operating system type and version, and client version
                    </li>
                    <li style={style}>
                    Information about your usage of or other interaction with our Service (“Usage Information”)
                    </li>
                    <li style={style}>
                    Other information you upload, provide or create while using the service ("Customer Content"), as further detailed in the “Customer Content” section below
                    </li>
                  </ul>
                </div>
                <div style={style} className="mb-1">
                We collect this data to provide you with the best experience with our Service. Mostly, we gather Personal Data directly from you, directly from your devices, or directly from someone who communicates with you using Bridge Connect services, such as a broadcast/meeting host, participant, or caller. Some of our collection happens on an automated basis – that is, it’s automatically collected when you interact with our Website/App. In certain instances, you can choose whether to provide Personal Data to Bridge Connect, but note that you may be unable to access certain options and services if they require Personal Data that you have not provided. You can adjust certain settings to reduce the amount of Personal Data we automatically collect from you, such as by turning off optional cookies in your browser’s setting or by using our Cookie Preferences link at the bottom of the Bridge Connect homepage.
                </div>
                <div style={style} className="mb-1">
                We may also obtain information about you from a user who uses Bridge Connect. For example, a user may input your contact information when you are invited to a Bridge Connect broadcast, meeting or call. Similarly, a Bridge Connect broadcast/meeting host may record a Bridge Connect broadcast/meeting and store the recording on our system. If a Bridge Connect broadcast/meeting host decides to record a meeting, that person is responsible for obtaining any necessary consent from you before recording a broadcast/meeting.
                </div>
                <div style={style} className="mb-1">
                We may also gather some Personal Data from third-party partners. Sometimes, other companies who help us deliver the service (our service providers) and may collect or have access to information on our behalf when you use our Services. We have agreements with our service providers to ensure that they do not use any of the information that they collect on our behalf for their own commercial purposes or for the commercial purposes of some other company or third party. We may also receive Personal Data that third parties collect in other contexts, which we use to better understand our users, advertise and market, and enhance our services.
                </div>
                <div style={style} className="mb-1">
                By registering your login and password associated with a supported third-party provider account to login to Bridge Connect, you authorize Bridge Connect to access and use your personal information associated with such third-party account profile for the purpose of providing the alternative login functionality. If you choose to not provide Bridge Connect authorization to access and use such personal information, you will not be able to login to Bridge Connect using your third-party provider account login and password. Bridge Connect’s use of your personal information as described in this paragraph shall comply with Bridge Connect’s
                </div>
                <div style={style} className="mb-1 text-bold-600">
                More about Customer Content    
                </div>
                <div style={style} className="mb-1">
                Customer Content is information provided by the customer to Bridge Connect through the usage of the service. Customer Content includes the content contained in cloud recordings, and instant messages, files, whiteboards, and shared while using the service. Customer Content does not refer to data generated by Bridge Connect’s network and systems (i.e., data that Bridge Connect creates because the customer is using the system (e.g., meeting routing information and other meeting metadata).
                </div>
                <div style={style} className="mb-1 text-bold-600">
                More about Passive Customer Content Collection , including the use of cookies
                </div>
                <div style={style} className="mb-1">
                Cookies are small files that a site or its service provider transfers to your computer's hard drive through your web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information.
                </div>
                <div style={headingStyle} className="mb-1">
                We use cookies to:
                </div>
                <div className="mb-1">
                  <ul>
                    <li style={style}>
                    Understand your preferences based on previous or current site activity to provide improved services
                    </li>
                    <li style={style}>
                    Compile aggregate data about site traffic and site interactions to offer better site experiences and tools in the future.
                    </li>
                  </ul>
                </div>
                <div style={style} className="mb-2">
                You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since each browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.
                </div>
                <div style={style} className="mb-1">
                Bridge Connect, our third-party service providers, and advertising partners (e.g., Google Ads and Google Analytics) automatically collect some information about you when you use our Products, Services, using methods such as cookies and tracking technologies (further described below). Information automatically collected includes Internet protocol (IP) addresses, browser type, Internet service provider (ISP), referrer URL, exit pages, the files viewed on our site (e.g., HTML pages, graphics, etc.), operating system, date/time stamp, and/or clickstream data. We use this information to offer and improve our services, trouble shoot, and to improve our marketing efforts.
                </div>
                <div style={style} className="mb-1">
                Passive collection involves using cookies or similar technologies to analyze trends, administer the website, track users’ movements around the website, and gather information about our user base, such as location information at the city level (which we derive from IP addresses). Users can control the use of cookies at the individual browser level and through the “Cookie Preferences” link on our homepage. We will also make reasonable efforts to ensure that we do not use our Products to collect information when you visit websites offered by companies other than Bridge Connect. For more information regarding cookies or similar technologies.
                </div>
                <div style={headingStyle} className="mb-1">
                More about Broadcast and Meeting Recordings
                </div>
                <div style={style} className="mb-1">
                If you participate in a Recorded Meeting or you subscribe to Bridge Connect cloud recording services, we collect information from you in connection with and through such Recordings. This information may include Personal Data. Meeting hosts are responsible for notifying you if they are recording a meeting, and you will generally hear a notice or see an on-screen notification when recording is in progress.
                </div>
                <div style={style} className="mb-1 text-bold-600">
                How We Use and Disclose Personal Data
                </div>
                <div style={style} className="mb-1">
                We process your Personal Data (i) with your consent (where necessary), (ii) for the performance of any contract you have with us (such as your agreement with us that allows us to provide you with the Products, Services), and (iii) for other legitimate interests and business purposes including in a manner reasonably proportionate to:
                </div>
                <div className="mb-1">
                  <ul>
                    <li style={style} className="text-bold-600">
                    Providing, running, personalizing, improving, operating, maintaining our Products, Services
                    </li>
                  </ul>
                </div>
                <div style={style} className="mb-1">
                We may use all of the types of Personal Data that we collect for:
                </div>
                <div className="mb-1">
                  <ul>
                    <li style={style}>
                    Account configuration
                    </li>
                    <li style={style}>
                    Account maintenance
                    </li>
                    <li style={style}>
                    Enabling meetings, webinars and broadcasts between users and third-party participants
                    </li>
                    <li style={style}>
                    Hosting and storing personal data from meetings, broadcasts and webinars on behalf and at the direction of the meeting host
                    </li>
                    <li style={style}>
                    Fulfilling requests, you make related to the service
                    </li>
                    <li style={style}>
                    Protecting, investigating and deterring against fraudulent, harmful, unauthorized or illegal activity
                    </li>
                    <li style={style}>
                    Providing reports to users based on information collected from the use of our service
                    </li>
                    <li style={style}>
                    Processing your orders and delivering the Products, Services that you have ordered
                    </li>
                    <li style={style}>
                    Providing support and assistance for our Products & Services
                    </li>
                    <li style={style}>
                    Providing the ability to create personal profile areas and view protected content
                    </li>
                    <li style={style}>
                    Providing the ability to contact you and provide you with shipping and billing information
                    </li>
                    <li style={style}>
                    Providing customer feedback and support
                    </li>
                    <li style={style}>
                    Complying with our contractual and legal obligations, resolving disputes with users, enforcing our agreements
                    </li>
                  </ul>
                </div>
                <div style={style} className="mb-1">
                You can sign-up, and therefore consent, to receive email or newsletter communications from us. If you would like to discontinue receiving these communications, you can update your preferences by using the “Unsubscribe” link found in such emails or by emailing unsubscribe@bridgeconnect.world
                </div>
                <div style={style} className="mb-1">
                We send you push notifications from time to time in order to update you about any events or promotions that we may be running. If you no longer wish to receive these types of communications, you can turn them off at the device level. To ensure you receive proper notifications, we will need to collect certain information about your device such as operating system and user identification information.
                </div>
                <div style={style} className="mb-1">
                  <ul>
                    <li>
                    Comply with our legal obligations or the legal obligations of our customers.
                    </li>
                  </ul>
                </div>
                <div style={style} className="mb-1">
                This includes responding to a legally binding demand for information, such as a warrant issued by a law enforcement entity of competent jurisdiction, or as reasonably necessary to preserve Bridge Connect’s legal rights.
                </div>
                <div style={style} className="mb-1">
                  <ul>
                    <li>
                    A word about location data.
                    </li>
                  </ul>
                </div>
                <div style={style} className="mb-1">
                As discussed above, we may collect information about your broad geographic location (city-level location) when you are using our Products & Services or have them installed on your device. We use this information for service-related purposes (such as optimizing your connection to our data center), supporting compliance (such as by telling us where you’re located, which can determine what laws or regulations apply to you), and suggesting customizations to your experience with our Products & Services (e.g. your language preference). We do not “track” you using location data and we only use this information in connection with providing the best experience with the Products & Services to you.
                </div>
                <div style={style} className="mb-1">
                  <ul>
                    <li>
                    What role do service providers play?
                    </li>
                  </ul>
                </div>
                <div style={style} className="mb-1">
                Note that we may use third-party service providers and our affiliated entities to help us do any of the things discussed here, and they may have access to Personal Data related to the specific activity they are doing for us in the process. We forbid our service providers from selling Personal Data they receive from us or on our behalf, and require them to only use Personal Data in order to perform the services we have asked of them unless otherwise required by law.
                </div>
                <div style={style} className="mb-1 text-bold-600">
                Does Bridge Connect sell Personal Data?
                </div>
                <div style={style} className="mb-1">
                We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information.
                </div>
                <div style={style} className="mb-1">
                Depends what you mean by “sell.” We do not allow marketing companies, advertisers, or anyone else to access Personal Data in exchange for payment. Except as described above, we do not allow any third parties access to any Personal Data we collect in the course of providing services to users. We do not allow third parties to use any Personal Data obtained from us for their own purposes, unless it is with your consent (e.g. when you download an app from the Marketplace). So in our humble opinion, we don’t think most of our users would see us as selling their information, as that practice is commonly understood.
                </div>
                <div style={style} className="mb-1">
                That said, Bridge Connect does use certain standard advertising tools which require Personal Data (think, for example, Google Ads and Google Analytics). We use these tools to help us improve your advertising experience (such as serving advertisements on our behalf across the Internet, serving personalized ads on our website, and providing analytics services). Sharing Personal Data with the third-party provider while using these tools may fall within the extremely broad definition of the “sale” of Personal Data under certain state laws because those companies might use Personal Data for their own business purposes, as well as Bridge Connect’s purposes. For example, Google may use this data to improve its advertising services for all companies who use their services. (It is important to note advertising programs have historically operated in this manner. It is only with the recent developments in data privacy laws that such activities fall within the definition of a “sale”). If you opt out of “sale” of your info, your Personal Data that may have been used for these activities will no longer be shared with third parties.
                </div>
                <div style={headingStyle} className="mb-1">
                Data Subject Rights
                </div>
                <div style={style} className="mb-1">
                We do our best to give you reasonable controls over the Personal Data we process about you as set forth below. Depending on where you reside, you may be entitled to certain legal rights with respect to your Personal Data.
                </div>
                <div style={style} className="mb-1">
                Here are the types of requests you may make related to Personal Data about you, subject to applicable laws, rules, or regulations:
                </div>
                <div style={style} className="mb-1">
                  <ul>
                    <li>
                    <strong>Access</strong>: You can request more information about the Personal Data we hold about you. You can also request a copy of the Personal Data. If you are an Indian resident, you can request information about both the categories and specific pieces of data we have collected about you in the previous twelve months, the reason we collected it, the category of entities with whom we have shared your data and the reason for any disclosure.
                    </li>
                    <li>
                    <strong>Rectification</strong>: If you believe that any Personal Data we are holding about you is incorrect or incomplete, you can request that we correct or supplement such data. You can also correct some of this information directly by logging into your service account. Please contact us as soon as possible upon noticing any such inaccuracy or incompleteness.
                    </li>
                    <li>
                    <strong>Objection</strong>: You can contact us to let us know that you object to the collection or use of your Personal Data for certain purposes.
                    </li>
                    <li>
                    <strong>Opt Out of “Sales”</strong>: You can ask us to opt you out of certain advertising practices related to your Personal Data by clicking on the “Do Not ‘Sell’ My Personal Information” link. You can also use an authorized agent to submit a request to opt-out on your behalf if you provide the agent written permission to do so. We may require the agent to submit proof that you have authorized them to submit an opt-out request.
                    </li>
                  </ul>
                </div>
                <div style={style} className="mb-1">
                Bridge Connect does not exchange your Personal Data with third parties for payment, even if you do not opt-out of the “sale” of information. If you opt-out, we will adjust your preferences accordingly.
                </div>
                <div style={style} className="mb-1">
                  <ul>
                    <li>
                    <strong>Erasure</strong>: You can request that we erase some or all of your Personal Data from our systems.
                    </li>
                    <li>
                    <strong>Restriction of Processing</strong>: You can ask us to restrict further processing of your Personal Data.
                    </li>
                    <li>
                    <strong>Portability</strong>: You can ask for a copy of your Personal Data in a machine-readable format. You can also request that we transmit the data to another entity where technically feasible.
                    </li>
                    <li>
                    <strong>Withdrawal of Consent</strong>: If we are processing your Personal Data based on your consent (as indicated at the time of collection of such data), you may have the right to withdraw your consent at any time.
                    </li>
                    <li>
                    <strong>Right to File Complaint</strong>: You have the right to lodge a complaint about Bridge Connect’s practices with respect to your Personal Data with the supervisory authority of your country or EU Member State.
                    </li>
                  </ul>
                </div>
                <div style={style} className="mb-1">
                Under certain circumstances we will not be able to fulfill your request, such as if it interferes with our regulatory obligations, affects legal matters including a Bridge Connect user’s rights to data contained in their account, we cannot verify your identity, or it involves disproportionate cost or effort. But in any event, we will respond to your request within a reasonable timeframe and provide you an explanation.
                </div>
                <div style={style} className="mb-1 text-bold-600">
                In order to make such a request of us, please contact our Privacy Team at <a href="mailto:support@bridgeconnect.world">support@bridgeconnect.world</a>
                </div>
                <div style={style} className="mb-1">
                If you have a password protected Bridge Connect account, we will use your account information to verify your identity. If not, we will ask you to provide additional information needed to verify your identity. The type and amount of information we request will depend on the nature of your request, the sensitivity of the relevant information, and the risk of harm from unauthorized disclosure or deletion. If you are a California resident and would like to designate an authorized agent to exercise any of your rights, please contact <a href="mailto:support@bridgeconnect.world">support@bridgeconnect.world</a>
                </div>
                <div style={style} className="mb-1 text-bold-600">
                Data Retention
                </div>
                <div style={style} className="mb-1">
                How long we retain your Personal Data depends on the type of data and the purpose for which we process the data. We will retain your Personal Information for the period necessary to fulfill the purposes outlined in this Privacy Notice unless a longer retention period is required by law.
                </div>
                <div style={style} className="mb-1 text-bold-600">
                Security of your Personal Data
                </div>
                <div style={style} className="mb-1">
                Bridge Connect is committed to protecting the Personal Data you share with us. We utilize a combination of industry-standard security technologies, procedures, and organizational measures to help protect your Personal Data from unauthorized access, use, or disclosure. When we transfer credit card information over the Internet, we protect it using Transport Layer Security (TLS) encryption technology.
                </div>
                <div style={style} className="mb-1">
                We recommend you take every precaution in protecting your Personal Data when you are on the Internet. For example, change your passwords often, use a combination of upper and lower-case letters, numbers, and symbols when creating passwords, and make sure you use a secure browser. If you have any questions about the security of your Personal Data, you can contact us at our Toll Free Number ‎+91 1800 266 7890
                </div>
                <div style={style} className="mb-1 text-bold-600">
                Linked Websites and Third-Party Products & Services
                </div>
                <div style={style} className="mb-1">
                Our websites and services may provide links to other third-party websites and services which are outside our control and not covered by this policy. We encourage you to review the privacy policies posted on these (and all) sites you visit or services you use.
                </div>
                <div style={style} className="mb-1 text-bold-600">
                Transfer and Storage of Personal Data
                </div>
                <div style={style} className="mb-1">
                Our Products are generally hosted and operated in India through Bridge Connect and its service providers, though data may be collected from wherever our users are located. We may transfer your Personal Data to any Bridge Connect affiliate worldwide, or to third parties acting on our behalf for the purposes of processing or storage. Where local law requires, we may store data locally in order to comply with global regulations. By using any of our Products & Services or providing any Personal Data for any of the purposes stated above, you consent to the transfer and storage of your Personal Data, whether provided by you or obtained through a third party.
                </div>
                <div style={style} className="mb-1 text-bold-600">
                Contact Us
                </div>
                <div style={style} className="mb-1">
                If you have any privacy-related questions or comments related to this privacy policy, please send an email to <a href="mailto:customer.support@gobridgeit.com">customer.support@gobridgeit.com</a>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
        <Footer />
      </div>
    )
  }
}