import React, { Component } from 'react';
import { Container, Header, Grid } from 'semantic-ui-react';
import MenuBar from './menu';
import Footer from './footer';
import '../../../assets/scss/pages/home.scss';

const style = {
  color: '#434343',
  fontSize: '1.3rem',
  lineHeight: '1.4'
}

const headingStyle = {
  color: '#434343',
  fontSize: '2.5rem',
  lineHeight: '1.4',
  fontWeight: 'bold'
}

export default class extends Component {
  render() {
    return (
      <div className="home-container">
        <MenuBar fixed />
        <div style={{height: '4em'}}></div>
        <Container>
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <Header as="h1" style={headingStyle} className="mb-2 mt-2">BRIDGE CONNECT TERMS OF USE OF SERVICE</Header>
                <div style={style} className="mb-1 text-bold-600">
                EFFECTIVE: April 01, 2020
                <br/>
                IMPORTANT, READ CAREFULLY: YOUR USE OF AND ACCESS TO THE WEBSITE AND PRODUCTS AND SERVICES AND ASSOCIATED SOFTWARE (COLLECTIVELY, THE "SERVICES") OF AND ITS AFFILIATES ("Bridge Connect") IS CONDITIONED UPON YOUR COMPLIANCE WITH AND ACCEPTANCE OF THESE TERMS, WHICH INCLUDE YOUR AGREEMENT TO ARBITRATE CLAIMS. PLEASE REVIEW THOROUGHLY BEFORE ACCEPTING.
                </div>
                <div style={style} className="mb-1 text-bold-600">
                BY CLICKING/CHECKING THE "I AGREE" BUTTON/BOX, ACCESSING THE Bridge Connect WEBSITE OR BY UTILIZING THE Bridge Connect SERVICES YOU AGREE TO BE BOUND BY THESE TERMS AND CONDITIONS AND ALL EXHIBITS, ORDER FORMS, AND INCORPORATED POLICIES (THE “AGREEMENT”). THE Bridge Connect SERVICES ARE NOT AVAILABLE TO PERSONS WHO ARE NOT LEGALLY ELIGIBLE TO BE BOUND BY THESE TERMS OF SERVICE.
                </div>
                <div style={style} className="mb-1">
                Bridge Connect will provide the Services, and you may access and use the Services, in accordance with this Agreement. If You order Services through our website <a href="https://www.gobridgeit.com/">https://www.gobridgeit.com/</a> or an order form (if issued when the services are LIVE for the market), the Order Form may contain additional terms and conditions and information regarding the Services you are ordering. Unless otherwise expressly set forth in any such additional terms and conditions applicable to the specific Service which You choose to use, those additional terms are hereby incorporated into this Agreement in relation to Your use of that Service.
                </div>
                <div style={style} className="mb-1">
                <strong>System Requirements</strong>. Use of the Services requires one or more compatible devices, Internet access (fees may apply), and certain software (fees may apply), and may require obtaining updates or upgrades from time to time. Because the use of the Services involves hardware, software, and Internet access, Your ability to access and use the Services may be affected by the performance of these factors. High-speed Internet access is recommended. You acknowledge and agree that such system requirements, which may be changed from time to time, are Your responsibility.
                </div>
                <div style={style} className="mb-1">
                  <ol start="1">
                    <li><strong>DEFINITIONS</strong>. The following definitions will apply in this Agreement, and any reference to the singular includes a reference to the plural and vice versa. Service-specific definitions are found in Exhibit A. “Affiliate” means, with respect to a Party, any entity that directly or indirectly controls, is controlled by or is under common control with that Party. For purposes of this Agreement, “control” means an economic or voting interest of at least fifty percent (50%) or, in the absence of such economic or voting interest, the power to direct or cause the direction of the management and set the policies of such entity. “End User” means a Host or Participant (as defined in Exhibit A) who uses the Services. "Initial Subscription Term" means the initial subscription term for a Service as specified in an Order Form. "Service Effective Date" means the date an Initial Subscription Term begins as specified in an Order Form. "Renewal Term" means the renewal subscription term for a Service commencing after the Initial Subscription Term or another Renewal Term as specified in an Order Form.</li>
                    <li>
                      <div>
                      <strong>SERVICES</strong>. Bridge Connect will provide the Services as described on the Order Form, and standard updates to the Services that are made generally available by Bridge Connect during the term. Bridge Connect may, in its sole discretion, discontinue the Services or modify the features of the Services from time to time without prior notice.
                      </div>
                      <ol style={{listStyleType: 'lower-alpha'}}>
                        <li>
                        <strong>Beta Services</strong>. Bridge Connect may, from time to time, offer access to services that are classified as Beta version. Access to and use of Beta versions may be subject to additional agreements. Bridge Connect makes no representations that a Beta version will ever be made generally available and reserves the right to discontinue or modify a Beta version at any time without notice. Beta versions are provided AS-IS, may contain bugs, errors or other defects, and Your use of a Beta version is at Your sole risk.
                        </li>
                      </ol>
                    </li>
                    <li>
                      <div>
                      <strong>USE OF SERVICES AND YOUR RESPONSIBILITIES</strong>. You may only use the Services according to the terms of this Agreement. You are solely responsible for Your and Your End Users’ use of the Services and shall abide by, and ensure compliance with, all Laws in connection with Your and each End User’s use of the Services, including but not limited to laws related to recording, intellectual property, privacy and export control. Use of the Services is void where prohibited.
                      </div>
                      <ol style={{listStyleType: 'lower-alpha'}}>
                        <li>
                        <strong>Registration Information</strong>. You may be required to provide information about Yourself in order to register for and/or use certain Services. You agree that any such information shall be accurate. You may also be asked to choose a user name and password. You are entirely responsible for maintaining the security of Your username and password and agree not to disclose such to any third party.
                        </li>
                        <li>
                        <strong>Your Content</strong>. You agree that You are solely responsible for the content ("Content") sent or transmitted by You or displayed or uploaded by You in using the Services and for compliance with all Laws pertaining to the Content, including, but not limited to, laws requiring You to obtain the consent of a third party to use the Content and to provide appropriate notices of third party rights. You represent and warrant that You have the right to upload the Content to Bridge Connect and that such use does not violate or infringe on any rights of any third party. Under no circumstances will Bridge Connect be liable in any way for any (a) Content that is transmitted or viewed while using the Services, (b) errors or omissions in the Content, or (c) any loss or damage of any kind incurred as a result of the use of, access to, or denial of access to Content. Although Bridge Connect is not responsible for any Content, Bridge Connect may delete any Content, at any time without notice to You, if Bridge Connect becomes aware that it violates any provision of this Agreement or any law.
                        </li>
                        <li>
                        <strong>Recordings</strong>. You are responsible for compliance will all recording laws. The host can choose to record Bridge Connect meetings and Webinars. By using the Services, you are giving Bridge Connect consent to store recordings for any or all Bridge Connect meetings or webinars that you join, if such recordings are stored in our systems. You will receive a notification (visual or otherwise) when recording is enabled. To operate our global service, we need to store and distribute content and data in our data centres and systems around the world, including outside your country of residence. This infrastructure may be operated or controlled by Bridge Connect or its affiliates
                        </li>
                        <li>
                          <strong>Prohibited Use</strong>Prohibited Use. You agree that You will not use, and will not permit any End User to use, the Services to: (i) modify, disassemble, decompile, prepare derivative works of, reverse engineer or otherwise attempt to gain access to the source code of the Services; (ii) knowingly or negligently use the Services in a way that abuses, interferes with, or disrupts Bridge Connect’s networks, Your accounts, or the Services; (iii) engage in activity that is illegal, fraudulent, false, or misleading, (iv) transmit through the Services any material that may infringe the intellectual property or other rights of third parties; (v) build or benchmark a competitive product or service, or copy any features, functions or graphics of the Services; or (vi) use the Services to communicate any message or material that is harassing, libelous, threatening, obscene, indecent, would violate the intellectual property rights of any party or is otherwise unlawful, that would give rise to civil liability, or that constitutes or encourages conduct that could constitute a criminal offense, under any applicable law or regulation; (vii) upload or transmit any software, Content or code that does or is intended to harm, disable, destroy or adversely affect performance of the Services in any way or which does or is intended to harm or extract information or data from other hardware, software or networks of Bridge Connect or other users of Services; (viii) engage in any activity or use the Services in any manner that could damage, disable, overburden, impair or otherwise interfere with or disrupt the Services, or any servers or networks connected to the Services or Bridge Connect's security systems. (ix) use the Services in violation of any Bridge Connect policy or in a manner that violates applicable law, including but not limited to anti-spam, export control, privacy, and anti-terrorism laws and regulations and laws requiring the consent of subjects of audio and video recordings, and You agree that You are solely responsible for compliance with all such laws and regulations.
                        </li>
                        <li>
                        <strong>Limitations on Use</strong>. You may not reproduce, resell, or distribute the Services or any reports or data generated by the Services for any purpose unless You have been specifically permitted to do so under a separate agreement with Bridge Connect. You may not offer or enable any third parties to use the Services purchased by You, display on any website or otherwise publish the Services or any Content obtained from a Service (other than Content created by You) or otherwise generate income from the Services or use the Services for the development, production or marketing of a service or product substantially similar to the Services.
                        </li>
                      </ol>
                    </li>
                    <li>
                    <strong>RESPONSIBILITY FOR END USERS</strong>. You are responsible for the activities of all End Users who access or use the Services through your account and you agree to ensure that any such End User will comply with the terms of this Agreement and any Bridge Connect policies. Bridge Connect assumes no responsibility or liability for violations. If You become aware of any violation of this Agreement in connection with use of the Services by any person, please contact Bridge Connect at compliance@bridgeconnect.world. Bridge Connect may investigate any complaints and violations that come to its attention and may take any (or no) action that it believes is appropriate, including, but not limited to issuing warnings, removing the content or terminating accounts and/or User profiles. Under no circumstances will Bridge Connect be liable in any way for any data or other content viewed while using the Services, including, but not limited to, any errors or omissions in any such data or content, or any loss or damage of any kind incurred as a result of the use of, access to, or denial of access to any data or content.
                    </li>
                  </ol>
                </div>
                <div style={style} className="mb-1">
                We do not control or direct what people and others do or say, and we are not responsible for their actions or conduct (whether online or offline) or any content that they share (including offensive, inappropriate, obscene, unlawful and other objectionable content).
                </div>
                <div style={style} className="mb-1">
                We cannot predict when issues may arise with our Products. Accordingly, our liability shall be limited to the fullest extent permitted by applicable law, and under no circumstances will we be liable to you for any lost profits, revenues, information or data, or consequential, special, indirect, exemplary, punitive or incidental damages arising out of or related to these Terms.
                </div>
                <div style={style} className="mb-1">
                  <ol start="5">
                    <li>
                      <strong>Bridge Connect Obligations for Content</strong>. Bridge Connect will maintain reasonable physical and technical safeguards to prevent unauthorized disclosure of or access to Content, in accordance with industry standards. Bridge Connect will notify You if it becomes aware of unauthorized access to Content. Bridge Connect will not access, view or process Content except (a) as provided for in this Agreement and in Bridge Connect’s Privacy Policy; (b) as authorized or instructed by You, (c) as required to perform its obligations under this Agreement; or (d) as required by Law. Bridge Connect has no other obligations with respect to Content.
                    </li>
                    <li>
                      <strong>ELIGIBILITY</strong>. You affirm that You are of legal age and are otherwise fully able and competent to enter into the terms, conditions, obligations, affirmations, representations, and warranties set forth in this Agreement, and to abide by and comply with this Agreement. Although we cannot absolutely control whether minors gain unauthorized access to the Services, access may be terminated without warning if we believe that You are underage or otherwise ineligible.
                    </li>
                    <li>
                      <strong>Professional Use</strong>. You may subscribe to and use the Services for business purposes, and You agree, if You are an individual, that the Services are being purchased in a business or professional capacity.
                    </li>
                    <li>
                      <strong>CHARGES AND CANCELLATION</strong>. You agree that Bridge Connect may charge to Your credit card or other payment mechanism selected by You and approved by Bridge Connect ("Your Account") all amounts due and owing for the Services, including taxes and service fees, set up fees, subscription fees, or any other fee or charge associated with Your Account. Bridge Connect may change prices at any time, including changing from a free service to a paid service and charging for Services that were previously offered free of charge; provided, however, that Bridge Connect will provide you with prior notice and an opportunity to terminate Your Account if Bridge Connect changes the price of a Service to which you are subscribed and will not charge you for a previously free Service unless you have been notified of the applicable fees and agreed to pay such fees. You agree that in the event Bridge Connect is unable to collect the fees owed to Bridge Connect for the Services through Your Account, Bridge Connect may take any other steps it deems necessary to collect such fees from You and that You will be responsible for all costs and expenses incurred by Bridge Connect in connection with such collection activity, including collection fees, court costs, and attorneys' fees. You further agree that Bridge Connect may collect interest at the lesser of 1.5% per month or the highest amount permitted by law on any amounts not paid when due. You may cancel your subscription at any time. If you cancel, you will not be billed for any additional terms of service, and service will continue until the end of the current Subscription Term. If you cancel, you will not receive a refund for any service already paid for.
                    </li>
                    <li>
                      <strong>TERMINATION</strong>. The Bridge Connect website contains information on how to terminate Your Account. If you have purchased a Service for a specific term, such termination will be effective on the last day of the then-current term. Your Order Form may provide that a Renewal Term will begin automatically unless either party provides notice of termination at least thirty (30) days prior to the commencement of the next Renewal Term. If You fail to comply with any provision of this Agreement, Bridge Connect may terminate this Agreement immediately and retain any fees previously paid by You. Sections 1 and 3 through 20, inclusive, shall survive any termination of this Agreement. Upon any termination of this Agreement, You must cease any further use of the Services. If at any time You are not happy with the Services, Your sole remedy is to cease using the Services and follow this termination process.
                    </li>
                  </ol>
                </div>
                <div style={style} className="mb-1">
                You should know that we may need to change the username for your account in certain circumstances (for example, if someone else claims the username and it appears unrelated to the name that you use in everyday life). We will inform you in advance if we have to do this and explain why.
                </div>
                <div style={style} className="mb-1">
                We always appreciate your feedback and other suggestions about our products and services. But you should know that we may use them without any restriction or obligation to compensate you, and we are under no obligation to keep them confidential.
                </div>
                <div style={style} className="mb-1">
                  <ol start="10">
                    <li>
                      <strong>PROPRIETARY RIGHTS</strong>. Bridge Connect and/or its suppliers, as applicable, retain ownership of all proprietary rights in the Services and all trade names, trademarks, service marks, logos, and domain names ("Bridge Connect Marks") associated or displayed with the Services. You may not frame or utilize framing techniques to enclose any Bridge Connect Marks or other proprietary information (including images, text, page layout, or form) of Bridge Connect without express written consent. You may not use any meta tags or any other "hidden text" utilizing Bridge Connect Marks without Bridge Connect's express written consent.
                    </li>
                    <li>
                      <strong>COPYRIGHT</strong>. You may not post, modify, distribute, or reproduce in any way copyrighted material, trademarks, rights of publicity or other proprietary rights without obtaining the prior written consent of the owner of such proprietary rights. Bridge Connect may deny access to the Services to any User who is alleged to infringe another party's copyright. Without limiting the foregoing, if You believe that Your copyright has been infringed, please notify Bridge Connect at compliance@bridgeconnect.world
                    </li>
                    <li>
                      <strong>EXPORT RESTRICTIONS</strong>. You acknowledge that the Services or portion thereof may be subject to the export control laws of India and other applicable country export control and trade sanctions laws (“Export Control and Sanctions Laws”). You and your End Users may not access, use, export, re-export, divert, transfer or disclose any portion of the Services or any related technical information or materials, directly or indirectly, in violation of any applicable export control or trade sanctions law or regulation. (i) You and your End Users are not identified on any Indian government restricted party lists; and (ii) that no Content created or submitted by You or your End Users is subject to any restriction on disclosure, transfer, download, export or re-export under the Export Control Laws. You are solely responsible for complying with the Export Control Laws and monitoring them for any modifications.
                    </li>
                    <li>
                      <strong>NO HIGH RISK USE</strong>. The Services are not designed or licensed for use in hazardous environments requiring fail-safe controls, including without limitation operation of nuclear facilities, aircraft navigation/communication systems, air traffic control, and life support or weapons systems. The Services shall not be used for or in any HIGH-RISK environment.
                    </li>
                    <li>
                      <strong>INJUNCTIVE RELIEF</strong>. You acknowledge that any use of the Services contrary to this Agreement, or any transfer, sublicensing, copying or disclosure of technical information or materials related to the Services, may cause irreparable injury to Bridge Connect, its Affiliates, suppliers and any other party authorized by Bridge Connect to resell, distribute, or promote the Services ("Resellers"), and under such circumstances Bridge Connect, its Affiliates, suppliers and Resellers will be entitled to equitable relief, without posting a bond or other security, including, but not limited to, preliminary and permanent injunctive relief.
                    </li>
                    <li>
                      <strong>NO WARRANTIES</strong>. You understand and agree that the services are provided "as is" and Bridge Connect, its affiliates, suppliers and resellers expressly disclaim all warranties of any kind, express or implied, including without limitation any warranty of merchantability, fitness for a particular purpose or non-infringement. Bridge Connect, its affiliates, suppliers and resellers make no warranty or representation regarding the results that may be obtained from the use of the services, regarding the accuracy or reliability of any information obtained through the services or that the services will meet any user's requirements, or be uninterrupted, timely, secure or error-free. The use of the services is at your sole risk. Any material and/or data downloaded or otherwise obtained through the use of the services is at your own discretion and risk. You will be solely responsible for any damage to you resulting from the use of the services. The entire risk arising out of use or performance of the services remains with you. Bridge Connect does not assume any responsibility for the retention of any user information or communications between users. Bridge Connectcannot guarantee and does not promise any specific results from use of the services. Use is at your own risk.
                    </li>
                    <li>
                      <strong>INDEMNIFICATION</strong>. You agree to indemnify, defend and hold harmless Bridge Connect, its affiliates, officers, directors, employees, consultants, agents, suppliers and Resellers from any and all third party claims, liability, damages and/or costs (including, but not limited to, a ttorneys' fees) arising from Your use of the Services, Your violation of this Agreement or the infringement or violation by You or any other user of Your account, of any intellectual property or other rights of any person or entity or applicable law.
                    </li>
                    <li>
                      <strong>LIMITATION OF LIABILITY</strong>. To the maximum extent permitted by applicable law, in no event will Bridge Connect or its affiliates, suppliers or resellers be liable for any special, incidental, indirect, exemplary or consequential damages whatsoever (including, without limitation, damages for loss of business profits, business interruption, loss of business information, or any other pecuniary loss or damage) arising out of the use of or inability to use the services or the provision of or failure to provide technical or other support services, whether arising in tort (including negligence) contract or any other legal theory, even if Bridge Connect, its affiliates, suppliers or resellers have been advised of the possibility of such damages. In any case, Bridge Connect's, its affiliates', suppliers' and resellers' maximum cumulative liability and your exclusive remedy for any claims arising out of or related to this agreement will be limited to the amount actually paid by you for the services (if any) in the twelve (12) months preceding the event or circumstances giving rise to such claims. Because some states and jurisdictions do not allow the exclusion or limitation of liability, the above limitation may not apply to you.
                    </li>
                    <li>
                      <strong>PRIVACY AND OTHER POLICIES</strong>. Use of the Services is also subject to Bridge Connect's Privacy Policy, a link to which is located at the footer on Bridge Connect's website. The Privacy Policy and all policies noticed at https://www.gobridgeit.com/legal are incorporated into this Agreement by this reference. Additionally, You understand and agree that Bridge Connect may contact You via e-mail or otherwise with information relevant to Your use of the Services, regardless of whether You have opted out of receiving marketing communications or notices.
                    </li>
                  </ol>
                </div>
                <div style={style} className="mb-1 text-bold-600">
                MISCELLANEOUS
                </div>
                <div style={style} className="mb-1">
                  <ol style={{listStyleType: 'upper-alpha'}}>
                    <li>
                    <strong>Choice of Law and Forum</strong>. This Agreement shall be governed by and construed under the laws of the India The Parties consent to the exclusive jurisdiction of the courts located at Mumbai in the state of Maharashtra.
                    </li>
                    <li>
                      <div style={style} className="mb-1">
                      <strong>Disputes</strong> A dispute is any controversy between You and Bridge Connect concerning the Services, any software related to the Services, the price of the Services, Your account, Bridge Connect’s advertising, marketing, or communications, Your purchase transaction or billing, or any term of this Agreement, under any legal theory including contract, warranty, tort, statute, or regulation, except disputes relating to the enforcement or validity of Your or Bridge Connect’s intellectual property rights. As part of the best efforts process to resolve disputes, and before initiating arbitration proceedings, each party agrees to provide notice of the dispute to the other party, including a description of the dispute, what efforts have been made to resolve it, and what the disputing party is requesting as resolution, to legal@Bridge Connect.us.
                      </div>
                      <div style={style} className="mb-1">
                      <strong>Arbitration Procedure</strong>. Disputes not resolved pursuant to Section A shall be resolved through arbitration. The Arbitration & Conciliation Act 1996 will conduct any arbitration under its Commercial Arbitration Rules. Arbitration hearings will take place in the state of Maharashtra at Mumbai. A single arbitrator will be appointed. The arbitrator must: (a) follow all applicable substantive law; (b) follow applicable statutes of limitations; (c) honor valid claims of privilege; (d) issue a written decision including the reasons for the award. The arbitrator may award damages, declaratory or injunctive relief, and costs (including reasonable attorneys’ fees). Any arbitration award may be enforced (such as through a judgment) in any court with jurisdiction.
                      </div>
                      <div style={style} className="mb-1">
                      <strong>Requirement to File Within One Year</strong>. Notwithstanding any other statute of limitations, a claim or dispute under this Agreement must be filed or notice for arbitration must be given within one year of when it could first be filed, or such claim will be permanently barred.
                      </div>
                      <div style={style} className="mb-1">
                      <strong>Waiver and Severability</strong>. Failure by either Party to exercise any of its rights under, or to enforce any provision of, this Agreement will not be deemed a waiver or forfeiture of such rights or ability to enforce such provision. If we fail to enforce any of these Terms, it will not be considered a waiver. Any amendment to or waiver of these Terms must be made in writing and signed by us.
                      </div>
                      <div style={style} className="mb-1">
                      If any provision of this Agreement is held by a court of competent jurisdiction to be illegal, invalid or unenforceable, that provision will be amended to achieve as nearly as possible the same economic effect of the original provision and the remainder of this Agreement will remain in full force and effect.
                      </div>
                    </li>
                    <li>
                      <div style={style} className="mb-1">
                      <strong>General Provisions</strong>. This Agreement embodies the entire understanding and agreement between the Parties respecting the subject matter of this Agreement and supersedes any and all prior understandings and agreements between the parties respecting such subject matter, except that if You or Your company have executed a separate written agreement or you have signed an order form referencing a separate agreement governing your use of the Services, then such agreement shall control to the extent that any provision of this Agreement conflicts with the terms of such agreement. Bridge Connect may elect to change or supplement the terms of this Agreement from time to time at its sole discretion. Bridge Connect will exercise commercially reasonable business efforts to provide notice to You of any material changes to this Agreement. Within ten (10) business days of posting changes to this Agreement (or ten (10) business days from the date of the notice, if such is provided), they will be binding on You. If You do not agree with the changes, You should discontinue using the Services. If You continue using the Services after such ten-business-day period, You will be deemed to have accepted the changes to the terms of this Agreement. In order to participate in certain Services, You may be notified that You are required to download software and/or agree to additional terms and conditions. Unless expressly outlined in such additional terms and conditions, those additional terms are hereby incorporated into this Agreement. This Agreement has been prepared in the English Language and such a version shall be controlling in all respects and any non-English version of this Agreement is solely for accommodation purposes.
                      </div>
                      <div className="mb-1">
                      <div className="text-bold-600">Objectionable Content</div>
                      <ul>
                        <li>
                          <div style={style} className="mb-1 text-bold-600">
                            Hate Speech
                          </div>
                          <div style={style} className="mb-1">
                          We do not allow hate speech on Bridge Connect because it creates an environment of intimidation and exclusion and in some cases may promote real-world violence.
                          </div>
                          <div style={style} className="mb-1">
                          We define hate speech as a direct attack on people based on what we call protected characteristics — race, ethnicity, national origin, religious affiliation, sexual orientation, caste, sex, gender, gender identity, and serious disease or disability. We also provide some protections for immigration status. We define attack as violent or dehumanizing speech, statements of inferiority, or calls for exclusion or segregation. We separate attacks into three tiers of severity, as described below.
                          </div>
                          <div style={style} className="mb-1">
                          Sometimes people share content containing someone else’s hate speech for the purpose of raising awareness or educating others. In some cases, words or terms that might otherwise violate our standards are used self-referentially or in an empowering way. People sometimes express contempt in the context of a romantic break-up. Other times, they use gender-exclusive language to control membership in a health or positive support group, such as a breastfeeding group for women only. In all of these cases, we allow the content but expect people to clearly indicate their intent, which helps us better understand why they shared it. Where the intention is unclear, we may remove the content.
                          </div>
                          <div style={style} className="mb-1">
                          We allow humor and social commentary related to these topics. In addition, we believe that people are more responsible when they share this kind of commentary using their authentic identity.
                          </div>
                        </li>
                        <li>
                          <div style={style} className="mb-1 text-bold-600">
                          Violent and Graphic Content
                          </div>
                          <div style={style} className="mb-1">
                          We remove content that glorifies violence or celebrates the suffering or humiliation of others because it may create an environment that discourages participation. We allow graphic content (with some limitations) to help people raise awareness about issues. We know that people value the ability to discuss important issues like human rights abuses or acts of terrorism. We also know that people have different sensitivities with regard to graphic and violent content. For that reason, we add a warning label to especially graphic or violent content so that it is not available to people under the age of eighteen and so that people are aware of the graphic or violent nature before they click to see it.
                          </div>
                        </li>
                        <li>
                          <div style={style} className="mb-1 text-bold-600">
                          Adult Nudity and Sexual Activity
                          </div>
                          <div style={style} className="mb-1">
                          We restrict the display of nudity or sexual activity because some people in our community may be sensitive to this type of content. Additionally, we default to removing sexual imagery to prevent the sharing of non-consensual or underage content. Restrictions on the display of sexual activity also apply to digitally created content unless it is posted for educational, humorous, or satirical purposes. Our nudity policies have become more nuanced over time. We understand that nudity can be shared for a variety of reasons, including as a form of protest, to raise awareness about a cause, or for educational or medical reasons. Where such intent is clear, we make allowances for the content.
                          </div>
                        </li>
                        <li>
                          <div style={style} className="mb-1 text-bold-600">
                          Sexual Solicitation
                          </div>
                          <div style={style} className="mb-1">
                          We restrict sexually explicit language that may lead to solicitation because some audiences within our global community may be sensitive to this type of content and it may impede the ability for people to connect with their friends and the broader community.
                          </div>
                        </li>
                        <li>
                          <div style={style} className="mb-1 text-bold-600">
                          Cruel and Insensitive
                          </div>
                          <div style={style} className="mb-1">
                          We believe that people share and connect more freely when they do not feel targeted based on their vulnerabilities. As such, we have higher expectations for content that we call cruel and insensitive, which we define as content that targets victims of serious physical or emotional harm.
                          </div>
                        </li>
                      </ul>
                      </div>
                      <div style={style} className="mb-1">
                      We remove explicit attempts to mock victims and mark as cruel implicit attempts, many of which take the form of memes and GIFs.
                      </div>
                    </li>
                  </ol>
                </div>
                <div style={style} className="mb-1 text-bold-600">
                Exhibit A Services Description
                </div>
                <div style={style} className="mb-1">
                This Exhibit A to the Terms of Service (“TOS”) describes the Services that may be ordered on an Order Form, or provided by Bridge Connect, and sets forth further Service-specific terms and conditions that may apply to Bridge Connect’s provision and Customer’s use of the Services. Capitalized terms not defined herein shall have the meanings assigned to them in the TOS.
                </div>
                <div style={style} className="mb-1">
                  <ol style={{listStyleType: 'upper-alpha'}}>
                    <li>
                      <div><strong>Definitions</strong>. For purposes of this Service Description, the following definitions will apply: “Host” means an individual who is an identified employee, contractor, or agent to whom You assign the right to host Meetings. A Host may hold an unlimited number of Meetings, but only one Meeting at a time. A Host subscription may not be shared or used by anyone other than the individual assigned to be a Host.</div>
                      <div><strong>“Meeting”</strong> means a Bridge Connect Audio / Video meeting.</div>
                      <div><strong>“Participant”</strong> means an individual, other than the Host, who accesses or uses the Services, with or without the permission and knowledge of the Host.</div>
                      <div><strong>“Bridge Connect Documentation”</strong> means this Exhibit, the Bridge Connect website ( <a href="https://www.gobridgeit.com/">https://www.gobridgeit.com/</a> ) and any additional description of the Services which may be incorporated into this Agreement.</div>
                      <div><strong>“Bridge Connect Meeting Services”</strong> means the various video conferencing, web conferencing, webinar, meeting room, screen sharing and other collaborative services offered by Bridge Connect Video that Customer may order.</div>
                      <div><strong>“Bridge Connect Phone Services”</strong> means voice connectivity services, including, but not limited to, interconnected VoIP services, provisioning of direct dial numbers, and related services offered by Bridge Connect (“Bridge Connect Voice”) that Customer may order on an Order Form.</div>
                    </li>
                    <li>
                    <strong>Bridge Connect Meeting Services</strong>. Bridge Connect Meeting Services enable Hosts to schedule and start Meetings and to allow Participants to join Meetings for the purpose of collaborating using voice, video, and screen sharing functionality. Every meeting will have one Host. Chat features allow for out-of-session one-on-one or group collaboration. Further features, functionality, and solutions are described at <a href="https://www.gobridgeit.com/">https://www.gobridgeit.com/</a>
                    </li>
                    <li>
                      <div style={style} className="mb-1">
                      <strong>Bridge Connect Phone Services</strong>. The following sets forth the further terms and conditions that apply to the Bridge Connect Phone Services.
                      </div>
                      <div style={style} className="mb-1">
                        <ol start="1">
                          <li>
                            <div><strong>Definitions</strong> For purposes of the Bridge Connect Phone Services, the following definitions apply:</div>
                            <div className="mb-1"><strong>“Device”</strong> means the device assigned to a virtual extension or individual digital line set up within an account or by Bridge Connect at Your direction or request.</div>
                            <div className="mb-1"><strong>“Phone Host”</strong> means the individual assigned to a number that enables the use of the Phone Service. A Phone Host is a “Host” for purposes of the definition of End User.</div>
                            <div className="mb-1"><strong>“Bridge Connect Phone Calling Plan”</strong> means the pricing structure that enables Phone Hosts and End Users to access the PSTN. Calling plans may be “Metered” or “Unlimited” as defined on the Order Form.</div>
                            <div><strong>“Bridge Connect Phone Commitment”</strong> means the minimum monthly bundle of minutes that a Bridge Connect Phone Metered Calling Plan Customer commits to use in connection with Bridge Connect Phone Services.</div>
                          </li>
                          <li>
                            <div>
                            <strong>Description of Services</strong>. Bridge Connect Phone Services are cloud-based phone services that use voice over internet protocol (VoIP) to provide You with the following services and functionalities (as selected on an Order Form):
                            <ol style={{listStyleType: 'lower-alpha'}}>
                              <li><strong>Bridge Connect Phone Service</strong>. Bridge Connect Phone Service is a cloud-based phone service that allows two-way voice calling and private branch exchange (PBX) functionality, including, but not limited to, the following features: unlimited extension-to-extension calling (On-Net Access), auto-attendant/ interactive voice response (IVR), call routing, call queuing, music on hold, call history, caller identification (outbound and inbound), call forwarding, call transfer, and call recording.</li>
                              <li><strong>Public Switched Telephone Network Communications (PSTN) Access</strong>. Phone Hosts and End Users can be enabled to make and receive calls to the PSTN and be assigned a direct inward dialing phone number (DID) via a Bridge Connect Phone Calling Plan.</li>
                              <li><strong>Additional Bridge Connect Phone Services</strong>. Additional functionality such as enabling common area phones, and additional Toll-Free and DID phone numbers may be purchased as described on the Order Form.</li>
                            </ol>
                            </div>
                          </li>
                          <li>
                            <div>
                              <strong>Billing and Invoicing</strong>. Bridge Connect will bill You on behalf of Bridge Connect Voice based on the Charges set forth on the Order Form. Charges based on usage, or overage amounts that exceed the Bridge Connect Phone Commitment, will be billed in arrears, the month following the month a Charge is incurred. No adjustment will be made, or credit or refund given, for usage that is less than the Bridge Connect Phone Commitment.
                              <ol style={{listStyleType: 'lower-alpha'}}>
                                <li><strong>Bridge Connect Phone Service</strong>. The on-Net capability will be provisioned by default for all Bridge Connect Meeting Services. Phone Hosts may access and use On-Net services at no charge for so long as the underlying license to the Bridge Connect Meeting Service remains active.</li>
                                <li><strong>Bridge Connect Phone Service</strong> You acknowledge and agree that Bridge Connect Phone Services are subject to certain Taxes and Fees (including, but not limited to, assessments for universal service) that do not apply to Bridge Connect Meeting Services. Accordingly, Bridge Connect shall invoice You for Taxes and Fees associated with the Charges.</li>
                              </ol>
                            </div>
                          </li>
                          <li><strong>Reasonable Use and Right to Review</strong>. Bridge Connect Voice offers unlimited and metered Phone Calling Plans. These plans are subject to this Bridge Connect Voice Communications, Inc. Reasonable Use Policy. Bridge Connect Phone Calling Plans are for normal and reasonable business use; unreasonable use is prohibited. Use of Bridge Connect Phone may qualify as unreasonable if You (a) engage in business activities that involve continual, uninterrupted, or consistently excessive use of Bridge Connect Phone Services, (b) make any misrepresentations to Bridge Connect Voice that materially affect volume or type of use of Bridge Connect Phone Services, (c) engage in the fraudulent or illegal use of Bridge Connect Phone Services, including any activity that violates telemarketing laws or regulations, or (d) use Bridge Connect Phone Services in any manner that harms Bridge Connect Voice’s network or facilities or interferes with the use of the service by other customers. Use that is inconsistent with the types and levels of usage by typical business customers on the same plan may be used as an indicator of abnormal or unreasonable use, including but not limited to abnormal call lengths; abnormal call frequency; abnormal call duration; abnormal calling patterns that indicate an attempt to evade enforcement of this Bridge Connect Voice Communications, Inc. Reasonable Use Policy. Bridge Connect Voice reserves the right to review Your use to determine if it is consistent with this Bridge Connect Voice Communications, Inc. Reasonable Use Policy. In the event Bridge Connect Voice determines that You may be engaging in unreasonable use, Bridge Connect Voice will determine the appropriate remedy and will take action to remedy any unreasonable use, including, at its sole discretion, discussing the use with You, moving You to an appropriate Bridge Connect Phone Calling Plan, terminating certain Hosts, and/or otherwise modifying, suspending or terminating Your Bridge Connect Phone services.</li>
                          <li><strong>Termination of Bridge Connect Meeting Services</strong>. Access to Bridge Connect Phone Services requires a corresponding license to Bridge Connect Meeting Services. In the event that the Bridge Connect Meeting Service license is terminated, the equivalent access to Bridge Connect Phone Services will also be terminated. At such time, You will be billed for any incurred usage charges, and will not be credited for any pre-paid amounts toward the Bridge Connect Phone Commitment.</li>
                          <li><strong>Bridge Connect Voice Policies</strong>. You acknowledge and agree that the Bridge Connect Voice Communications, Inc. policies found at https://www.gobridgeit.com/legal apply to Your use of Bridge Connect Phone Services.</li>
                          <li><strong>Equipment</strong>. Bridge Connect Voice does not supply any devices or other equipment used in connection with the Bridge Connect Phone Services, and accordingly Bridge Connect Phone does not provide any guarantees as to the quality or operability of such Devices and equipment when used to access Bridge Connect Phone Services. However, Bridge Connect Voice does test certain Devices and equipment to determine whether such Devices and equipment are supported on the Bridge Connect Phone platform (although it has not tested all possible Devices and equipment available in the marketplace). The summary of Devices and equipment to date that Bridge Connect Voice has determined are supported by the Bridge Connect Phone platform may be provided on request. You should consult with Bridge Connect Voice prior to deploying any other Devices and equipment.</li>
                        </ol>
                      </div>
                    </li>
                  </ol>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
        <Footer />
      </div>
    )
  }
}