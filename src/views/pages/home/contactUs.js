import React from 'react';
import { Grid, Header, Icon, Form, Button } from 'semantic-ui-react';
import {
  FormGroup,
  Input,
} from "reactstrap"

export default () => {
  return (
    <div className="home-page-contact-us pb-2">
      <Grid centered className="p-2" stackable>
        <Grid.Row className="pb-0">
          <Grid.Column width="16">
            <div className="m-1 d-flex flex-column justify-content-center h-100">
              <div className="pl-5 content">
                <Header as="h1" className="mb-0" style={{color: '#3ea3f7', fontSize: '2.5em'}}>
                  Don’t hesitate to Contact Us
                </Header>
                <Header as="h3" className="mt-1 mb-2">
                  We always love to hear from you... for any query in reference to Product, Sales or even Collaboration, feel free to reach out.
                </Header>
              </div>
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width="10">
            <Form className="pl-5 ml-1 content">
              <FormGroup>
                <Input type="text" id="name" name="fullname" placeholder="Enter your name"/>
              </FormGroup>
              <FormGroup>
                <Input type="text" id="name" name="email" placeholder="Enter your email"/>
              </FormGroup>
              <FormGroup>
                <Input type="text" id="name" name="phone" placeholder="Enter your Mobile"/>
              </FormGroup>
              <FormGroup>
                <Input type="text" id="name" name="message" placeholder="Enter your message"/>
              </FormGroup>
              <Button type="submit">Submit</Button>
            </Form>
          </Grid.Column>
          <Grid.Column width="6">
            <Header as="h2">Contact Info</Header>
            <a className="" href="mailto:support@gobridgeit.com">
              <Icon name="mail" className="mr-1"></Icon><span style={{color: '#000', fontSize: '1.25em'}}>support@gobridgeit.com</span>
            </a>
            <Header as="h2">Stay Connected</Header>
            <a className="" rel="noopener noreferrer" href="https://www.facebook.com/gobridgeit" target="_blank">
              <Icon name="facebook" className="mr-1" size="large"></Icon>
            </a>
            <a className="" rel="noopener noreferrer" href="https://twitter.com/gobridgeit" target="_blank">
              <Icon name="twitter" className="mr-1" size="large"></Icon>
            </a>
            <a className="" rel="noopener noreferrer" href="https://www.instagram.com/gobridgeit/" target="_blank">
              <Icon name="instagram" className="mr-1" size="large"></Icon>
            </a>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}