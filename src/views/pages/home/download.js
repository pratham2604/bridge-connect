import React from 'react';
import { Grid, Image, Header, Icon } from 'semantic-ui-react';
import AppStore from '../../../assets/img/pages/home/appstore.png';
import PlayStore from '../../../assets/img/pages/home/playstore.png';
import Download from '../../../assets/img/pages/home/download.png'

export default () => {
  return (
    <div className="home-page-banner home-page-download pb-3">
      <Grid centered className="pl-2 pr-2 pt-2 pb-0" stackable>
        <Grid.Row className="pb-0">
          <Grid.Column width="9">
            <div className="m-1 d-flex flex-column justify-content-center h-100">
              <div className="pl-5 content">
                <Header as="h1" className="mb-0" style={{color: '#3ea3f7', fontSize: '2.5em'}}>
                  Download Bridge Connect
                </Header>
                <Header as="h3" className="mt-1 mb-2">
                  Bridge Connect official application can now be downloaded for Windows. Features include
                </Header>
                <div>
                  <div className="mb-1" style={{fontSize: '1.25em'}}>
                    <Icon name="hand point right outline" className="mr-1"></Icon>
                    Single / Group Chat
                  </div>
                  <div className="mb-1" style={{fontSize: '1.25em'}}>
                    <Icon name="hand point right outline" className="mr-1"></Icon>
                    Attachments
                  </div>
                  <div className="mb-1" style={{fontSize: '1.25em'}}>
                    <Icon name="hand point right outline" className="mr-1"></Icon>
                    Audio / Video Call
                  </div>
                  <div className="mb-1" style={{fontSize: '1.25em'}}>
                    <Icon name="hand point right outline" className="mr-1"></Icon>
                    Broadcast
                  </div>
                  <div className="mb-1" style={{fontSize: '1.25em'}}>
                    <Icon name="hand point right outline" className="mr-1"></Icon>
                    Screen Share
                  </div>
                </div>
                <div className="mb-1">
                  <a className="d-inline-block mr-1" rel="noopener noreferrer" href="https://apps.apple.com/in/app/bridge-connect/id1442281765" target="_blank">
                    <Image src={AppStore} />
                  </a>
                  <a className="d-inline-block mr-1" rel="noopener noreferrer" href="https://play.google.com/store/apps/details?id=com.bridge.viztarInfotech" target="_blank">
                    <Image src={PlayStore} />
                  </a>
                </div>
              </div>
            </div>
          </Grid.Column>
          <Grid.Column width="7">
            <Image src={Download} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}