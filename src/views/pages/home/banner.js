import React from 'react';
import { Grid, Image, Header } from 'semantic-ui-react';
import AppStore from '../../../assets/img/pages/home/appstore.png';
import PlayStore from '../../../assets/img/pages/home/playstore.png';
import MobileHand from '../../../assets/img/pages/home/mobile-with-hand1.png'

export default () => {
  return (
    <div className="home-page-banner">
      <Grid centered className="pl-2 pr-2 pt-2 pb-0" stackable>
        <Grid.Row className="pb-0">
          <Grid.Column width="10">
            <div className="m-1 d-flex flex-column justify-content-center h-100">
              <div className="pl-5 content">
                <Header as="h1" className="mb-0" style={{color: '#3ea3f7', fontSize: '2.5em'}}>
                  Seamless Networking Experience
                </Header>
                <Header as="h3" className="mt-1 mb-2">
                  Enhancing Communication. Enhancing Productivity.
                </Header>
                <div>
                  <a className="d-inline-block mr-1" rel="noopener noreferrer" href="https://apps.apple.com/in/app/bridge-connect/id1442281765" target="_blank">
                    <Image src={AppStore} />
                  </a>
                  <a className="d-inline-block mr-1" rel="noopener noreferrer" href="https://play.google.com/store/apps/details?id=com.bridge.viztarInfotech" target="_blank">
                    <Image src={PlayStore} />
                  </a>
                </div>
              </div>
            </div>
          </Grid.Column>
          <Grid.Column width="6">
            <Image src={MobileHand} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}