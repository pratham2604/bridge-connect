import React from 'react';
import { Grid, Image, Header } from 'semantic-ui-react';
import Swiper from 'react-id-swiper';

import Comm1 from '../../../assets/img/pages/home/communication-1.png';
import Comm2 from '../../../assets/img/pages/home/communication-2.png';
import Comm3 from '../../../assets/img/pages/home/communication-3.png';
import Comm4 from '../../../assets/img/pages/home/communication-4.png';

import AudioNote from '../../../assets/img/pages/home/audionote.jpg';
import Videomeet from '../../../assets/img/pages/home/videomeet.jpg';
import Private from '../../../assets/img/pages/home/private.jpg';
import Secret from '../../../assets/img/pages/home/secret.jpg';

import 'swiper/swiper.scss'

export default () => {
  const params = {
    effect: 'coverflow',
    grabCursor: true,
    centeredSlides: true,
    slidesPerView: 'auto',
    loop: true,
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    },
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true
    },
    pagination: {
      el: '.swiper-pagination'
    }
  }

  return (
    <div className="home-page-commuincation">
      <Grid centered className="p-2" stackable>
        <Grid.Row>
          <Grid.Column>
            <Header as="h1" className="text-bold-400 text-center">
              Multidimensional Communication Platform
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width="4">
            <div className="m-1 d-flex flex-column justify-content-center h-100">
              <div className="">
                <div className="mb-1 text-center">
                  <Image src={Comm1} className="m-auto"/>
                  <Header as="h2" className="text-bold-400">Audio Note</Header>
                  <div className="text-muted">With a preview option before send</div>
                </div>
                <div className="mb-1 text-center">
                  <Image src={Comm2} className="m-auto" />
                  <Header as="h2" className="text-bold-400">Live Meetings</Header>
                  <div className="text-muted">Start/Join Meetings instantly</div>
                </div>
              </div>
            </div>
          </Grid.Column>
          <Grid.Column width="8">
            <div>
              <Swiper {...params}>
                <div style={{height: '35em', width: '20em', borderRadius: '10px'}}>
                  <Image src={AudioNote} fluid style={{borderRadius: '10px', height: '100%'}}/>
                </div>
                <div style={{height: '35em', width: '20em', borderRadius: '10px'}}>
                  <Image src={Videomeet} fluid style={{borderRadius: '10px', height: '100%'}}/>
                </div>
                <div style={{height: '35em', width: '20em', borderRadius: '10px'}}>
                  <Image src={Secret} fluid style={{borderRadius: '10px', height: '100%'}}/>
                </div>
                <div style={{height: '35em', width: '20em', borderRadius: '10px'}}>
                  <Image src={Private} fluid style={{borderRadius: '10px', height: '100%'}}/>
                </div>
              </Swiper>
            </div>
          </Grid.Column>
          <Grid.Column width="4">
            <div className="m-1 d-flex flex-column justify-content-center h-100">
              <div className="">
                <div className="mb-1 text-center">
                  <Image src={Comm3} className="m-auto"/>
                  <Header as="h2" className="text-bold-400">Secret Replies</Header>
                  <div className="text-muted">Message individuals secretly in group</div>
                </div>
                <div className="mb-1 text-center">
                  <Image src={Comm4} className="m-auto" />
                  <Header as="h2" className="text-bold-400">Private and Secure</Header>
                  <div className="text-muted">All chats are encrypted.</div>
                </div>
              </div>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}
