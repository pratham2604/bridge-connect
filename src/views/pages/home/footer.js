import React from 'react';
import { Grid } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

export default () => {
  return (
    <div className="home-page-footer">
      <Grid centered className="pl-2 pr-2 pt-2 pb-0" stackable>
        <Grid.Row className="pb-0">
          <Grid.Column width="3" className="text-center">
            <Link to="/privacy-policy">
              <span className="mb-1" style={{color: 'white', fontSize: '1.25em'}}>Privacy Policy</span>
            </Link>
          </Grid.Column>
          <Grid.Column width="3" className="text-center">
            <Link to="/terms">
              <span className="mb-1" style={{color: 'white', fontSize: '1.25em'}}>Terms of Use</span>
            </Link>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row className="pb-0 mb-2">
          <Grid.Column width="4" className="mb-1 text-center" style={{color: 'white', fontSize: '1.25em'}}>
            Copyright © 2017-2020 Bridge Connect
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}