import React, { Component } from 'react';

import Banner from './banner';
import Communication from './communication';
import KnowApp from './knowapp';
import MenuBar from './menu';
import Features from './features';
import Download from './download';
import ContactUs from './contactUs';
import Footer from './footer';

import '../../../assets/scss/pages/home.scss';

export default class extends Component {
  render() {
    return (
      <div className="home-container">
        <MenuBar fixed />
        <div style={{height: '4em'}}></div>
        <Banner />
        <Communication />
        <KnowApp />
        <Features />
        <Download />
        <ContactUs />
        <Footer />
      </div>
    )
  }
}