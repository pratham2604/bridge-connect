import React from 'react';
import { Image, Container, Menu, Header } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

import Logo from '../../../assets/img/logo/Bridge-Logo.png';

export default (props) => {
  const { fixed } = props;
  return (
    <Menu fixed={fixed ? 'top' : ''}>
      <Container className="home-menu">
        <Menu.Item header as={Link} to="/">
          <Image size='mini' src={Logo} style={{ marginRight: '1.5em' }} />
          <Header as="h2" className="mt-0 site-title">Connecting Minds</Header>
        </Menu.Item>
        <Menu.Menu position='right' className="header-login-menu">
          <Menu.Item as={Link} to="/login" style={{fontWeight: "bold"}}>Login</Menu.Item>
        </Menu.Menu>
      </Container>
    </Menu>
  )
}