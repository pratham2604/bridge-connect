import React from 'react';
import { Grid, Image, Header } from 'semantic-ui-react';
import Waterwheel1 from '../../../assets/img/pages/home/waterwheel1.png';
import Waterwheel2 from '../../../assets/img/pages/home/waterwheel2.png';
import Waterwheel3 from '../../../assets/img/pages/home/waterwheel3.png';
import Waterwheel4 from '../../../assets/img/pages/home/waterwheel4.png'

export default () => {
  return (
    <div className="home-page-features p-2">
      <Grid centered className="p-2" stackable>
        <Grid.Row>
          <Grid.Column>
            <Header as="h1" className="text-bold-400 text-center" style={{color: 'gray'}}>
              Features
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width="3">
            <div className="m-1 d-flex flex-column justify-content-center h-100">
              <Image src={Waterwheel1} className="m-auto"/>
              <h4 className="mt-1 text-center text-bold-400">Real Time Document Collaboration</h4>
            </div>
          </Grid.Column>
          <Grid.Column width="3">
            <div className="m-1 d-flex flex-column justify-content-center h-100">
              <Image src={Waterwheel2} className="m-auto"/>
              <h4 className="mt-1 text-center text-bold-400">Pay Per View Webcasts</h4>
            </div>
          </Grid.Column>
          <Grid.Column width="3">
            <div className="m-1 d-flex flex-column justify-content-center h-100">
              <Image src={Waterwheel3} className="m-auto"/>
              <h4 className="mt-1 text-center text-bold-400">Chat Meet Webinars</h4>
            </div>
          </Grid.Column>
          <Grid.Column width="3">
            <div className="m-1 d-flex flex-column justify-content-center h-100">
              <Image src={Waterwheel4} className="m-auto"/>
              <h4 className="mt-1 text-center text-bold-400">Live Whiteboard</h4>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}
