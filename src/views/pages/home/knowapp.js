import React from 'react';
import { Grid, Image, Header } from 'semantic-ui-react';
import Chat from '../../../assets/img/pages/home/chat.png';
import FileWord from '../../../assets/img/pages/home/file-word-regular.svg';
import AudioVideo from '../../../assets/img/pages/home/audiovideo.png';
import Livestream from '../../../assets/img/pages/home/livestream.png';
import VideoConf from '../../../assets/img/pages/home/videoconf.png';
import Platform from '../../../assets/img/pages/home/platform.png'

export default () => {
  return (
    <div className="home-page-know-app">
      <Grid centered className="p-2" stackable>
        <Grid.Row>
          <Grid.Column>
            <Header as="h1" className="text-bold-400 text-center">
              Know Your App
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width="4">
            <div className="m-1 d-flex flex-column justify-content-center h-100">
              <div className="">
                <div className="mb-2 text-center">
                  <Image src={Chat} className="m-auto"/>
                  <Header as="h2" className="text-bold-400">Chat</Header>
                  <div className="text-muted">When the mind connects, click on chat to further discuss your ideas and thoughts. It is both personal and efficient.</div>
                </div>
                <div className="mb-2 text-center">
                  <Image src={FileWord} className="m-auto" style={{width: '40px'}}/>
                  <Header as="h2" className="text-bold-400">File and Screen sharing</Header>
                  <div className="text-muted">Share data with your network through "File Sharing" or share your screen for live demonstrations through "Screen Sharing".</div>
                </div>
              </div>
            </div>
          </Grid.Column>
          <Grid.Column width="4">
            <div className="m-1 d-flex flex-column justify-content-center h-100">
              <div className="">
                <div className="mb-2 text-center">
                  <Image src={AudioVideo} className="m-auto"/>
                  <Header as="h2" className="text-bold-400">Audio / Video Call</Header>
                  <div className="text-muted">Stay connected with your professional and social network through audio and video calls. It is time to express yourself.</div>
                </div>
                <div className="mb-2 text-center">
                  <Image src={Livestream} className="m-auto" />
                  <Header as="h2" className="text-bold-400">LIVE Streaming</Header>
                  <div className="text-muted">Nothing speaks louder than your enthusiasm. Go live on Bridge Connect anytime, anywhere to spread inspiration.</div>
                </div>
              </div>
            </div>
          </Grid.Column>
          <Grid.Column width="4">
            <div className="m-1 d-flex flex-column justify-content-center h-100">
              <div className="">
                <div className="mb-2 text-center">
                  <Image src={VideoConf} className="m-auto"/>
                  <Header as="h2" className="text-bold-400">Conferencing</Header>
                  <div className="text-muted">Be it "one to many" or "many to many" every network will enjoy an uninterrupted and quality conferencing and broadcasting experience.</div>
                </div>
                <div className="mb-2 text-center">
                  <Image src={Platform} className="m-auto" />
                  <Header as="h2" className="text-bold-400">Cross-Platform</Header>
                  <div className="text-muted">Enjoy cross device compatibility. Bridge Connect is compatible with every device and every browser present in the market.</div>
                </div>
              </div>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}
