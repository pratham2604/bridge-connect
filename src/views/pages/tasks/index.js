import React from "react"
import Sidebar from "react-sidebar"
import { ContextLayout } from "../../../utility/context/Layout"
import FixedSidebar from './sidebar';
import TaskBoard from './taskBoard';
import "../../../assets/scss/pages/app-email.scss"
const mql = window.matchMedia(`(min-width: 992px)`)

class Index extends React.Component {
  state = {
    composeMailStatus: false,
    sidebarDocked: mql.matches,
    sidebarOpen: false,
    activeTab: 'design'
  }

  handleComposeSidebar = status => {
    if (status === "open") {
      this.setState({
        composeMailStatus: true
      })
    } else {
      this.setState({
        composeMailStatus: false
      })
    }
  }

  UNSAFE_componentWillMount() {
    mql.addListener(this.mediaQueryChanged)
  }

  componentWillUnmount() {
    mql.removeListener(this.mediaQueryChanged)
  }

  onSetSidebarOpen = open => {
    this.setState({ sidebarOpen: open })
  }

  mediaQueryChanged = () => {
    this.setState({ sidebarDocked: mql.matches, sidebarOpen: false })
  }

  handleMainAndComposeSidebar = () => {
    this.handleComposeSidebar("close")
    this.onSetSidebarOpen(false)
  }

  changeTab = (id) => {
    this.setState({
      activeTab: id,
    })
  }

  render() {
    const { activeTab } = this.state;
    return (
      <React.Fragment>
        <div className="custom-fixed-sidebar-container position-relative" style={{height: '100%'}}>
          <div
            className={`app-content-overlay ${
              this.state.composeMailStatus || this.state.sidebarOpen ? "show" : ""
            }`}
            onClick={this.handleMainAndComposeSidebar}
          />
          <ContextLayout.Consumer>
            {context => (
              <Sidebar
                sidebar={
                  <FixedSidebar changeTab={this.changeTab} />
                }
                docked={this.state.sidebarDocked}
                open={this.state.sidebarOpen}
                sidebarClassName="sidebar-content custom-app-sidebar d-flex"
                touch={false}
                contentClassName="sidebar-children"
                pullRight={context.state.direction === "rtl"}>
                  {activeTab === 'design' && <TaskBoard/>}
                  {activeTab === 'frontend' && <TaskBoard/>}
                  {activeTab === 'backend' && <TaskBoard/>}
                  {activeTab === 'marketing' && <TaskBoard/>}
              </Sidebar>
            )}
          </ContextLayout.Consumer>
        </div>
      </React.Fragment>
    )
  }
}

export default Index
