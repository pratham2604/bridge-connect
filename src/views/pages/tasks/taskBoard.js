import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import * as Icon from "react-feather"
import {
  FormGroup,
  Input,
  Card
} from "reactstrap"
import { Search } from "react-feather"
import moment from 'moment';

const tagMap = {
  'Completed': '#E3F2CB',
  'Pending Approval': '#FEEDDC',
  'No Stage': '#E6E5E6',
  'Approved': '#D5EFFF',
  'Need Review': '#FFE3E3'
}

class Index extends Component {
  state = {
  }

  render() {
    return (
      <Grid className="w-100">
        <Grid.Column width={16}>
          <div className="mt-1">
            <Grid className="w-100">
              <Grid.Column width={8}>
                <h1 className="tex-bold-600 ml-2">
                  Project Name
                </h1>
              </Grid.Column>
              <Grid.Column width={8}>
                <div className="text-right d-flex" style={{flexDirection: 'row-reverse'}}>
                  <div style={{width: '15em'}}>
                  <FormGroup className="position-relative has-icon-left mb-0 ml-1">
                    <Input
                      type="text"
                      className=""
                      value={this.state.value}
                      onChange={e => this.setState({ value: e.target.value })}
                    />
                    <div className="form-control-position px-1">
                      <Search size={15} />
                    </div>
                  </FormGroup>
                  </div>
                  <span className="d-flex flex-column justify-content-center ml-1">
                    <Icon.Bell size={20} />
                  </span>
                  <span className="ml-1 d-flex flex-column justify-content-center">
                    <Icon.PlusCircle size={20} />
                  </span>
                </div>
              </Grid.Column>
            </Grid>
            {/* <Grid className="w-100 ml-1">
              <Grid.Column width={8}>
                <Button style={{cursor: 'pointer'}}>
                  <Icon.User size={20}/>+
                  <div className="ml-2 d-inline-block">Invite User</div>
                </Button>
              </Grid.Column>
              <Grid.Column width={4}>
                <div className="d-flex mt-1 mb-1">
                  <Checkbox toggle />
                  <h4 className="mt-0 ml-1 text-bold-400 mb-2">Autoplay next video</h4>
                </div>
              </Grid.Column>
            </Grid> */}
            <Grid className="w-100 ml-1">
              {taskList.map((taskColumn, index) => {
                return (
                  <Grid.Column width={4}>
                    <h3 className="text-bold-400">{taskColumn.title}</h3>
                    <div className="d-flex">
                      <span className="d-flex flex-column justify-content-center text-muted">
                        <Icon.PlusCircle size={20} />
                      </span>
                      <div className="ml-1 text-muted">Add New Item</div>
                    </div>
                    {taskColumn.tasks.map((task, index) => {
                      const { tags } = task;
                      return (
                        <Card className="mt-1 p-1">
                          {tags.map(tag => {
                            const backgroundColor = tagMap[tag];
                            return (
                              <div className="d-inline-block w-auto" style={{backgroundColor, padding: '4px'}}>{tag}</div>
                            )
                          })}
                          <div className="mb-2 mt-1">{task.description}</div>
                          <div>{task.completed}</div>
                          <div className="mt-1">
                            <Icon.Clock size={20} className="mr-1"/>
                            Due {moment(task.date).format('MMM DD, YYYY')}
                          </div>
                        </Card>
                      )
                    })}
                  </Grid.Column>
                )
              })}
            </Grid>
          </div>
        </Grid.Column>
      </Grid>
    )
  }
}

const taskList = [{
  title: 'ToDo / Planned',
  tasks: [{
    tags: ['Need Review'],
    description: 'Sint culpa duis aliquip sit non do excepteur non consectetur elit reprehenderit aliqua laboris.',
    completed: '4/15',
    due: '03-03-2020',
    comments: ['as', '22'],
    attachments: ['', '']
  }, {
    tags: ['No Stage'],
    description: 'Aliquip nisi officia consequat ex et commodo pariatur in.',
    completed: '2/5',
    due: '03-03-2020',
    attachments: ['', '']
  }, {
    tags: ['Pending Approval'],
    description: 'Id nulla qui dolore reprehenderit ea irure.',
    completed: '8/8',
    due: '03-03-2020',
    comments: ['as', '22'],
    attachments: ['', '']
  }]
}, {
  title: 'In Progress / Editing',
  tasks: [{
    tags: ['Completed'],
    description: 'Aliquip nisi officia consequat ex et commodo pariatur in.',
    completed: '3/6',
    due: '03-03-2020',
    comments: ['as', '22'],
    attachments: ['', '']
  }, {
    tags: ['Approved'],
    description: 'Laboris amet et eu duis veniam laborum labore non consequat sint ea deserunt.',
    completed: '11/32',
    due: '03-03-2020',
    attachments: ['', '']
  }]
}, {
  title: 'Final Check / Approve',
  tasks: [{
    tags: ['Need Review'],
    description: 'Sint culpa duis aliquip sit non do excepteur non consectetur elit reprehenderit aliqua laboris.',
    completed: '4/15',
    due: '03-03-2020',
    comments: ['as', '22'],
    attachments: ['', '']
  }, {
    tags: ['No Stage'],
    description: 'Aliquip nisi officia consequat ex et commodo pariatur in.',
    completed: '2/5',
    due: '03-03-2020',
    attachments: ['', '']
  }, {
    tags: ['Pending Approval'],
    description: 'Id nulla qui dolore reprehenderit ea irure.',
    completed: '8/8',
    due: '03-03-2020',
    comments: ['as', '22'],
    attachments: ['', '']
  }]
}]

export default Index;