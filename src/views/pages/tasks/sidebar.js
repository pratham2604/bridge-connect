import React, { Component } from 'react';
import * as Icon from "react-feather"
import { Button } from 'reactstrap';

class FixedSidebar extends Component {
  state = {
    activeTab: 'design'
  }

  selectTab = (id) => {
    this.setState({
      activeTab: id
    });
    this.props.changeTab(id);
  }

  render() {
    const { activeTab } = this.state;
    return (
      <div className="p-1 w-100">
        <div style={{display: 'flex'}} className="mt-2">
          <h1 className="text-bold-600">
            Boards
          </h1>
          <div style={{flex: 1}} className="text-right">
            <Button.Ripple outline style={{padding: '0.9rem'}}>
              <Icon.Plus size={20} />
            </Button.Ripple>
          </div>
        </div>
        <div className="mt-2">
          <div className="text-muted mt-1 mb-1">
            Projects
          </div>
          <div>
            {MENU.map((menu, index) => {
              const backgroundColor = menu.id === activeTab ? '#EFF7FF' : '#FFF';
              const color = menu.id === activeTab ? '#007AFF': '#333';
              return (
                <div key={index} className="menu p-1" style={{display: 'flex', cursor: 'pointer', backgroundColor, color }} onClick={this.selectTab.bind(this, menu.id)}>
                  {menu.icon}
                  <h4 className="text-bold-600 mt-0 text-left pl-1" style={{flex: '1'}}>{menu.title}</h4>
                </div>
              )
            })}
          </div>
        </div>
        <div className="mt-2">
          <div className="text-muted mt-1 mb-1">
            Users
          </div>
          {/* <Users /> */}
        </div>
      </div>
    )
  }
}

const MENU = [{
  id: 'design',
  title: 'Design',
  icon: <Icon.Activity size={20} className="mr-1" />
}, {
  id: 'frontend',
  title: 'Front-End',
  icon: <Icon.Hash size={20} className="mr-1" />
}, {
  id: 'backend',
  title: 'Back-End',
  icon: <Icon.Award size={20} className="mr-1" />
}, {
  id: 'marketing',
  title: 'Marketing',
  icon: <Icon.Battery size={20} className="mr-1" />
}]

export default FixedSidebar;