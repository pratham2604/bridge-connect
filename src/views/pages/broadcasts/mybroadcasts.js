import React, { Component } from 'react';
import { Grid, Image, Header, Button } from 'semantic-ui-react';
import {
  Card,
  CardImg,
  CardBody,
} from "reactstrap"
import PlayModal from './play';
import { getMyBroadCasts, getBroadCast } from '../../../redux/actions/broadcasts/index'
import { connect } from "react-redux";
import * as Icon from "react-feather"
import { getUser } from '../../../redux/actions/auth/loginActions'
import {CopyToClipboard} from 'react-copy-to-clipboard';
import NoData from '../../../assets/img/svg/no-data.svg';
import moment from 'moment';
import { history } from '../../../history';

class Index extends Component {
  state = {
    activeIndex: 0,
    showModal: false,
    broadcasts: []
  }

  componentDidMount() {
    const { user, getMyBroadCasts } = this.props;
    getMyBroadCasts(user,  async (data) => {
      const { result } = data;
      const userIds = result.map(item => item.creator_id);
      const broadcastsIds = result.map(item => item.id);
      const users = await Promise.all(userIds.map(userId => getUser(user, userId)));
      const broadcastsData = await Promise.all(broadcastsIds.map(id => getBroadCast(user, id)));
      const broadcasts = result.map(item => Object.assign({}, item, {
        user: users.find(user => user.user_id === item.creator_id),
        attachments: (broadcastsData.find(data => data.id === item.id) || {}).attachments
      }));
      this.setState({
        broadcasts,
      })
    });
  }

  updateIndex = (index) => {
    this.setState({
      activeIndex: index,
    })
  }

  toggleModal = (selectedBroadcast) => {
    if (selectedBroadcast.video_type === 'live') {
      return;
    }
    history.push(`/play-broadcast/${selectedBroadcast.id}`);
  }

  render() {
    const { showModal, broadcasts, selectedBroadcast = {} } = this.state;
    if (broadcasts.length === 0) {
      return (
        <div className="mt-2">
          <Grid className="w-100" centered>
            <Grid.Column width={10}>
              <Image src={NoData} size="big" />
              <Header as="h3" className="text-bold-600 mt-2 text-center">
                You don't have any broadcasts yet. Do you want to create one?
              </Header>
              <div className="m-auto text-center">
                <Button onClick={this.props.changeTab.bind(this, 'create-broadcast')} color="blue">
                  Create Broadcast
                </Button>
              </div>
            </Grid.Column>
          </Grid>
        </div>
      )
    }

    return (
      <div className="mt-2">
        <Grid className="w-100" centered>
          {broadcasts.map((broadcast, index) => {
            const style = {
              textOverflow: 'ellipsis',
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              display: 'inline-block',
              width: '70%'
            }
            const activeVideoBanner = ((broadcast.attachments || {}).banners || [])[0];
            let activeVideoUrl = ((broadcast.attachments || {}).videos || [])[0];
            const activeVideoUser = (broadcast.user || {})
            const { title, description, id, start_at, cost } = broadcast;
            const momentDate = moment(start_at, 'YYYY-MM-DD HH:mm:ss')
            const { origin } = window.location;
            let copyLink = `${origin}/broadcasts/${id}`;
            const { video_type } = broadcast;
            if (video_type === 'live') {
              const { origin } = window.location;
              activeVideoUrl = `${origin}/live-broadcast/${id}`;
              copyLink = activeVideoUrl;
            }
            return (
              <Grid.Column width={5} key={index}>
                <Card className="h-100" onClick={this.toggleModal.bind(this, broadcast)}>
                  {(video_type !== 'live' && activeVideoBanner) && <CardImg top className="img-fluid" src={activeVideoBanner} alt="card image cap" style={{maxHeight: '14em'}}/>}
                  <CardBody className="d-flex" style={{flexDirection: 'column-reverse'}}>
                    <div className="d-flex w-100">
                      <div className="mr-1">
                        <div className="text-muted">{momentDate.format('MMM')}</div>
                        <h2 className="text-bold-600 mt-1">{momentDate.format('DD')}</h2>
                      </div>
                      <div style={{flex: '1'}} className="w-100">
                        <h3 className="text-bold-600 mb-0 w-70" style={{wordBreak: "break-all", width: '70%'}}>{title}</h3>
                        <h5 className="text-bold-400 mb-0 mt-0 w-70" style={{wordBreak: "break-all", width: '70%'}}>{description}</h5>
                        <div className="mt-1 w-100">
                          <a href={activeVideoUrl} rel="noopener noreferrer" target="_blank" className="mr-1" style={style}>{activeVideoUrl}</a>
                          <CopyToClipboard text={copyLink}>
                            <Icon.Copy size={17} className="cursor-pointer mr-1"/>
                          </CopyToClipboard>
                        </div>
                        <div className="text-muted mt-1">
                          <span className="pr-1">{momentDate.format('ddd')}</span>
                          <span className="pr-1">{momentDate.format('HH:mm')}</span>
                          <span>{`Video by ${activeVideoUser.fullname}`}</span>
                        </div>
                        <div className="text-muted mt-1">
                          Cost: {cost > 0 ? cost : 'Free'}
                        </div>
                        {/* <div>
                          {members.slice(0, 3).map((member, index) => {
                            return (
                              <Image src={member.photoUrl} avatar />
                            )
                          })}
                        </div> */}
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </Grid.Column>
            )
          })}
        </Grid>
        {showModal && <PlayModal video={selectedBroadcast} onClose={this.toggleModal} open={showModal}/>}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {
  getMyBroadCasts,
})(Index);