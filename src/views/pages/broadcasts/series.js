import React, { Component } from 'react';
import { Button, Checkbox, Grid, Image } from 'semantic-ui-react';
import {
  Card,
  CardImg,
  CardImgOverlay,
} from "reactstrap"
import PlayModal from './play';

class Index extends Component {
  state = {
    activeIndex: 0,
    showModal: false,
  }

  updateIndex = (index) => {
    this.setState({
      activeIndex: index,
    })
  }

  toggleModal = () => {
    this.setState({
      showModal: !this.state.showModal
    })
  }

  render() {
    const { activeIndex, showModal } = this.state;
    const activeVideo = VIDEO_LIST[activeIndex];

    return (
      <Grid className="w-100 mr-0 ml-0">
        <Grid.Row>
          <Grid.Column width={11}>
            <Card className="text-white overlay-img-card mt-1 ml-1" style={{height: '400px'}}>
              <CardImg src={activeVideo.thumbnail} alt="overlay img" style={{height: '400px'}}/>
              <CardImgOverlay className="overlay-black d-flex flex-column justify-content-center">
                <Button style={{width: '300px', margin: 'auto'}} onClick={this.toggleModal}>
                  <h3 className="text-bold-600 mb-1">Play</h3>
                  <div className="text-muted">${activeVideo.cost} credits will be used</div>
                </Button>
              </CardImgOverlay>
            </Card>
            <div className="d-flex m-1">
              <div className="pr-1">
                {activeVideo.description}
              </div>
              <div>
                <div>{`Video by ${activeVideo.author}`}</div>
                <div>
                  <a href={activeVideo.videoUrl} target="_blank" rel="noopener noreferrer">
                    {activeVideo.videoUrl}
                  </a>
                </div>
              </div>
            </div>
          </Grid.Column>
        <Grid.Column width={5} style={{backgroundColor: 'white'}}>
          <div className="p-1 mb-2">
            <h2 className="text-bold-600 mt-1">Series Broadcasts</h2>
            <div className="d-flex mt-1 mb-1">
              <Checkbox toggle />
              <h4 className="mt-0 ml-1 text-bold-400 mb-2">Autoplay next video</h4>
            </div>
            <Grid style={{height: '300px', overflow: 'auto'}}>
              {VIDEO_LIST.map((video, index) => {
                const { thumbnail, name, author, cost, duration } = video;
                const activeVideo = activeIndex === index;
                const backgroundColor = activeVideo ? '#EFF7FF' : '';
                return (
                  <Grid.Row key={index} className="p-0 mb-1" style={{cursor: 'pointer', backgroundColor}} onClick={this.updateIndex.bind(this, index)}>
                    <Grid.Column width={5}>
                      <Image src={thumbnail} rounded fluid style={{height: '75px'}}/>
                    </Grid.Column>
                    <Grid.Column width={11} className="p-1">
                      <div className="d-flex mb-1">
                        <div style={{flex: '1'}}>{name}</div>
                        <div>{duration}</div>
                      </div>
                      <div className="d-flex">
                        <div style={{flex: '1'}}>{author}</div>
                        <div>{cost}</div>
                      </div>
                    </Grid.Column>
                  </Grid.Row>
                )
              })}
            </Grid>
          </div>
        </Grid.Column>
        </Grid.Row>
        <PlayModal video={activeVideo} onClose={this.toggleModal} open={showModal}/>
      </Grid>
    )
  }
}

const VIDEO_LIST = [{
  thumbnail: 'https://i.imgur.com/IyEp7mf.jpg',
  videoUrl: 'https://www.youtube.com/watch?v=oUFJJNQGwhk',
  name: 'Video 1',
  author: 'Pratham',
  description: 'Image / Photo / Video Caption Dolore eiusmod officia ipsum aute irure nostrud dolor reprehenderit dolor in consequat ad sit. Eiusmod dolor adipisicing pariatur occaecat commodo labore officia anim aute esse mollit excepteur.',
  cost: '300',
  duration: '12:30'
}, {
  thumbnail: 'https://i.imgur.com/YxwaxND.png',
  videoUrl: 'https://www.facebook.com/facebook/videos/10153231379946729/',
  name: 'Video 2',
  author: 'Pratham',
  description: 'Image / Photo / Video Caption Dolore eiusmod officia ipsum aute irure nostrud dolor reprehenderit dolor in consequat ad sit. Eiusmod dolor adipisicing pariatur occaecat commodo labore officia anim aute esse mollit excepteur.',
  cost: '475',
  duration: '30:13'
}, {
  thumbnail: 'https://i.imgur.com/JPEAk21.jpg',
  videoUrl: 'https://vimeo.com/90509568',
  name: 'Video 3',
  author: 'Pratham',
  description: 'Image / Photo / Video Caption Dolore eiusmod officia ipsum aute irure nostrud dolor reprehenderit dolor in consequat ad sit. Eiusmod dolor adipisicing pariatur occaecat commodo labore officia anim aute esse mollit excepteur.',
  cost: '200',
  duration: '24:12'
}, {
  thumbnail: 'https://i.imgur.com/vJnYXEe.jpg',
  videoUrl: 'https://www.twitch.tv/videos/12783852',
  name: 'Video 4',
  author: 'Pratham',
  description: 'Image / Photo / Video Caption Dolore eiusmod officia ipsum aute irure nostrud dolor reprehenderit dolor in consequat ad sit. Eiusmod dolor adipisicing pariatur occaecat commodo labore officia anim aute esse mollit excepteur.',
  cost: '500',
  duration: '50:12'
}, {
  thumbnail: 'https://i.imgur.com/phpRZWZ.jpeg',
  videoUrl: 'https://www.dailymotion.com/video/x5e9eog',
  name: 'Video 4',
  author: 'Pratham',
  description: 'Image / Photo / Video Caption Dolore eiusmod officia ipsum aute irure nostrud dolor reprehenderit dolor in consequat ad sit. Eiusmod dolor adipisicing pariatur occaecat commodo labore officia anim aute esse mollit excepteur.',
  cost: '700',
  duration: '45:12'
}]

export default Index;