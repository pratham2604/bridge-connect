import React, { Component } from 'react';
import { Button, Grid } from 'semantic-ui-react';
import {
  Card,
  CardImg,
  CardImgOverlay,
} from "reactstrap"
import PlayModal from './play';
import axios from 'axios';

class Index extends Component {
  state = {
    activeIndex: 0,
    showModal: false,
  }

  getBroadCast = (user, id) => {
    const { token } = user;
    return axios.get(`../api/v1/broadcasts/${id}/`, {
      headers: {
        Authorization: `Token ${token}`
      }
    }).then(response => {
      return response.data.result;
    })
  }

  getUser = (user, userId) => {
    const { token } = user;
    return axios.get(`/api/v1/users/${userId}/`, {
      headers: {
        Authorization: `Token ${token}`
      }
    }).then(response => {
      return response.data;
    })
  }

  async componentDidMount() {
    const { id, user } = this.props;
    const broadcast = await this.getBroadCast(user, id);
    const creator = await this.getUser(user, broadcast.creator_id);
    broadcast.user = creator;
    this.setState({
      broadcast,
    })
  }

  updateIndex = (index) => {
    this.setState({
      activeIndex: index,
    })
  }

  toggleModal = () => {
    this.setState({
      showModal: !this.state.showModal
    })
  }

  render() {
    const { broadcast, showModal } = this.state;
    const activeVideo = broadcast || {};
    const activeVideoBanner = ((activeVideo.attachments || {}).banners || [])[0];
    const activeVideoUrl = ((activeVideo.attachments || {}).videos || [])[0];
    const activeVideoUser = (activeVideo.user || {})

    return (
      <Grid className="w-100 mr-0 ml-0">
        <Grid.Row>
          <Grid.Column width={11}>
            <Card className="text-white overlay-img-card mt-1 ml-1" style={{height: '400px'}}>
              <CardImg src={activeVideoBanner} alt="overlay img" style={{height: '400px'}}/>
              <CardImgOverlay className="overlay-black d-flex flex-column justify-content-center">
                <Button style={{width: '300px', margin: 'auto'}} onClick={this.toggleModal}>
                  <h3 className="text-bold-600 mb-1">Play</h3>
                  {/* <div className="text-muted">${activeVideo.cost} credits will be used</div> */}
                </Button>
              </CardImgOverlay>
            </Card>
            <div className="d-flex m-1">
              <div className="pr-1">
                {activeVideo.description}
              </div>
              <div>
                <div>{`Video by ${activeVideoUser.fullname}`}</div>
                <div>
                  <a href={activeVideoUrl} target="_blank" rel="noopener noreferrer">
                    {activeVideoUrl}
                  </a>
                </div>
              </div>
            </div>
          </Grid.Column>
        
        </Grid.Row>
        <PlayModal video={activeVideo} onClose={this.toggleModal} open={showModal}/>
      </Grid>
    )
  }
}

export default Index;