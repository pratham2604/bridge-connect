import React, { Component } from 'react';
import { Grid, Image } from 'semantic-ui-react';
import * as Icon from "react-feather"
import Select from "react-select"
import {
  Card,
  CardBody,
  CardImg,
} from "reactstrap"
import moment from 'moment';

class Index extends Component {
  render() {
    return (
      <Grid className="w-100">
        <Grid.Column width={16}>
          <div className="mt-1">
            <Grid className="w-100">
              <Grid.Column width={8}>
                <h1 className="tex-bold-600 ml-2">
                  Upcoming Events
                </h1>
              </Grid.Column>
              <Grid.Column width={8}>
                <div className="text-right d-flex" style={{flexDirection: 'row-reverse'}}>
                  <span className="ml-1 d-flex flex-column justify-content-center">
                    <Icon.Sliders size={20} />
                  </span>
                  <div style={{width: '15em'}}>
                    <Select
                      className="React ml-1"
                      classNamePrefix="select"
                      defaultValue={SORT_OPTIONS[0]}
                      name="color"
                      
                      options={SORT_OPTIONS}
                    />
                  </div>
                  <span className="ml-1 d-flex flex-column justify-content-center">
                    Sort By 
                  </span>
                  <span className="d-flex flex-column justify-content-center">
                    <Icon.Search size={20} />
                  </span>
                </div>
              </Grid.Column>
            </Grid>
          </div>
          <div className="mt-2">
            <Grid className="w-100" centered>
              {EVENTS.map((event) => {
                const { imageUrl, date, title, location, members } = event;
                const momentDate = moment(date, 'DD-MM-YYYY HH:mm')
                return (
                  <Grid.Column width={5}>
                    <Card>
                      <CardImg top className="img-fluid" src={imageUrl} alt="card image cap" style={{maxHeight: '14em'}}/>
                      <CardBody>
                        <div className="d-flex">
                          <div className="mr-1">
                            <div className="text-muted">{momentDate.format('MMM')}</div>
                            <h2 className="text-bold-600 mt-1">{momentDate.format('DD')}</h2>
                          </div>
                          <div style={{flex: '1'}}>
                            <h3 className="text-bold-600 mb-0">{title}</h3>
                            <div className="text-muted">
                              <span className="pr-1">{momentDate.format('ddd')}</span>
                              <span className="pr-1">{momentDate.format('HH:mm')}</span>
                              <span>{location}</span>
                            </div>
                            <div>
                              {members.slice(0, 3).map((member, index) => {
                                return (
                                  <Image src={member.photoUrl} avatar />
                                )
                              })}
                            </div>
                          </div>
                        </div>
                      </CardBody>
                    </Card>
                  </Grid.Column>
                )
              })}
            </Grid>
          </div>
        </Grid.Column>
      </Grid>
    )
  }
}

const EVENTS = [{
  imageUrl: 'https://i.imgur.com/phpRZWZ.jpeg',
  date: '22/04/2020 12:15',
  title: 'Coding Event',
  location: 'New York',
  members: [{
    displayName: 'Alex',
    photoUrl: 'https://i.imgur.com/phpRZWZ.jpeg'
  }]
}, {
  imageUrl: 'https://i.imgur.com/phpRZWZ.jpeg',
  date: '22-04-2020 12:15',
  title: 'Coding Event',
  location: 'New York',
  members: [{
    displayName: 'Alex',
    photoUrl: 'https://i.imgur.com/phpRZWZ.jpeg'
  }]
}, {
  imageUrl: 'https://i.imgur.com/phpRZWZ.jpeg',
  date: '22-04-2020 12:15',
  title: 'Coding Event',
  location: 'New York',
  members: [{
    displayName: 'Alex',
    photoUrl: 'https://i.imgur.com/phpRZWZ.jpeg'
  }]
}, {
  imageUrl: 'https://i.imgur.com/phpRZWZ.jpeg',
  date: '22-04-2020 12:15',
  title: 'Coding Event',
  location: 'New York',
  members: [{
    displayName: 'Alex',
    photoUrl: 'https://i.imgur.com/phpRZWZ.jpeg'
  }]
}, {
  imageUrl: 'https://i.imgur.com/phpRZWZ.jpeg',
  date: '22-04-2020 12:15',
  title: 'Coding Event',
  location: 'New York',
  members: [{
    displayName: 'Alex',
    photoUrl: 'https://i.imgur.com/phpRZWZ.jpeg'
  }]
}, {
  imageUrl: 'https://i.imgur.com/phpRZWZ.jpeg',
  date: '22-04-2020 12:15',
  title: 'Coding Event',
  location: 'New York',
  members: [{
    displayName: 'Alex',
    photoUrl: 'https://i.imgur.com/phpRZWZ.jpeg'
  }]
}, {
  imageUrl: 'https://i.imgur.com/phpRZWZ.jpeg',
  date: '22-04-2020 12:15',
  title: 'Coding Event',
  location: 'New York',
  members: [{
    displayName: 'Alex',
    photoUrl: 'https://i.imgur.com/phpRZWZ.jpeg'
  }]
}, {
  imageUrl: 'https://i.imgur.com/phpRZWZ.jpeg',
  date: '22-04-2020 12:15',
  title: 'Coding Event',
  location: 'New York',
  members: [{
    displayName: 'Alex',
    photoUrl: 'https://i.imgur.com/phpRZWZ.jpeg'
  }]
}, {
  imageUrl: 'https://i.imgur.com/phpRZWZ.jpeg',
  date: '22-04-2020 12:15',
  title: 'Coding Event',
  location: 'New York',
  members: [{
    displayName: 'Alex',
    photoUrl: 'https://i.imgur.com/phpRZWZ.jpeg'
  }]
}]

const SORT_OPTIONS = [{
  label: 'Most Followers',
  value: 'most-follower',
}, {
  label: 'Most Popular',
  value: 'most-popular',
}]

export default Index;