import React, { Component } from 'react';
import { connect } from "react-redux";
import { Grid, Segment, Icon, Button } from 'semantic-ui-react';
import Broadcast from './liveBroadcast/meetingRoom';
import '../../../assets/scss/pages/broadcast.scss';
import { ContainerProvider } from './live/container';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { history } from "../../../history";
import { getBroadCast } from '../../../redux/actions/broadcasts/index'
import { setupBroadcastClient } from '../../../redux/actions/socket/index'

class Index extends Component {
  state = {
    isHost: true,
    joined: false,
  }

  async componentDidMount() {
    const { user, location } = this.props;
    const { pathname = '' } = location || {};
    const broadcastId = pathname.split('/').pop();
    const broadcast = await getBroadCast(user, broadcastId);
    const { creator_id } = broadcast || {};
    const isHost = creator_id === user.user_id;
    this.setState({
      broadcast,
      isHost,
      broadcastId,
    });
  }

  selectRole = (role) => {
    this.setState({
      isHost: role === 'host',
    });
  }

  joinBroadcast = () => {
    this.setState({
      joined: true,
    }, () => {
      this.connectToSocket();
    })
  }

  connectToSocket = () => {
    const { broadcastId } = this.state;
    const { user, dispatch } = this.props;
    setupBroadcastClient(user.token, dispatch, broadcastId);
  }

  render() {
    const { isHost, joined } = this.state;
    const { user, dispatch } = this.props;
    const { pathname } = window.location;
    const channel = pathname.split('/').pop();
    return (
      <ContainerProvider>
        <div className="m-1 h-100">
          <Grid className="h-100">
            <Grid.Column className="h-100">
              {joined ?
                <Broadcast channel={channel} isHost={isHost} user={user} dispatch={dispatch} /> :
                <JoinPanel host={isHost} selectRole={this.selectRole} channel={channel} joinBroadcast={this.joinBroadcast}/>
              }
              
            </Grid.Column>
          </Grid>
        </div>
      </ContainerProvider>
    )
  }
}

class JoinPanel extends Component {
  state = {}

  leaveMeeting = () => {
    history.push('/broadcasts');
  }

  render() {
    const { host, selectRole } = this.props;
    const url = window.location.href;
    const copyColor = this.state.copied ? "blue" : "black";

    return (
      <Grid centered>
        <Grid.Column width={10}>
          <Segment className="mt-4">
            <h1 className="text-center">Join Broadcost ?</h1>
            <div className="text-center mt-2 d-flex justify-content-center">
              <div className="mx-1">
                <Button.Group>
                  <Button positive={host} onClick={selectRole.bind(this, 'host')}>Host</Button>
                  <Button.Or />
                  <Button positive={!host} onClick={selectRole.bind(this, 'audience')}>Audience</Button>
                </Button.Group>
              </div>
            </div>
            <div className="text-center my-2">
              <Button color="blue" className="mx-1 cursor-pointer" onClick={this.props.joinBroadcast}>Join Broadcast</Button>
              <Button color="red" className="mx-1 cursor-pointer" onClick={this.leaveMeeting}>Cancel</Button>
            </div>
            <div className="text-center mt-2 mb-2">
              <span>
                <b>Invite to broadcast</b> : {url}
                <span className="ml-1">
                  <CopyToClipboard text={url} onCopy={() => this.setState({copied: true})}>
                    <Icon name="copy outline" size="large" className="cursor-pointer mr-1" color={copyColor}/>
                  </CopyToClipboard>
                </span>
              </span>
            </div>
          </Segment>
        </Grid.Column>
      </Grid>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(Index);