import React, { Component } from "react";
import { Tab } from 'semantic-ui-react';
import ChatBox from './liveBroadcast/chatbox';

export default class ChatRoom extends Component {
  state = {
    active: 1,
  }

  panes = [
  //   {
  //   menuItem: 'Members',
  //   render: () => (
  //     <Tab.Pane>
  //       <Members broadcastId={this.props.broadcastId} user={this.props.user} />
  //     </Tab.Pane>
  //   ),
  // },
   {
    menuItem: 'Chat',
    render: () => (
      <Tab.Pane>
        <ChatBox roomId={this.props.broadcastId} user={this.props.user} />
      </Tab.Pane>
    ),
  }]

  render() {
    return (
      <Tab panes={this.panes} className="mt-1"/>
    )
  }
}