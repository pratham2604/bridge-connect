import React, { Component, Fragment } from 'react';
import { Grid, Button, Icon } from 'semantic-ui-react';
import Select from "react-select"
import {
  FormGroup,
  Input,
} from "reactstrap"
import Flatpickr from "react-flatpickr";
import moment from 'moment';
import "flatpickr/dist/themes/light.css";
import "../../../assets/scss/plugins/forms/flatpickr/flatpickr.scss"
import { createBroadCast, onUpload } from  '../../../redux/actions/broadcasts/index'
import { getContactsList } from "../../../redux/actions/chat/index"
import { getUser } from '../../../redux/actions/auth/loginActions'
import { connect } from "react-redux";
import { history } from '../../../history';
import { v4 as uuidv4 } from 'uuid';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import '../../../assets/scss/pages/app-chat.scss';

const VIDEO_OPTIONS = [{
  label: 'Recorded',
  value: 'recorded',
}, {
  label: 'Live',
  value: 'live',
}]

const CATEGORIES = [{
  label: 'Private',
  value: 'private',
}, {
  label: 'Public',
  value: 'public',
}]

class Index extends Component {
  state = {
    title: '',
    cost: 0,
    type: VIDEO_OPTIONS[0],
    category: CATEGORIES[1],
    date: new Date(),
    time: new Date(),
    description: '',
    attachment: '',
    thumbnail: '',
    isLiveBroadcast: false,
    broadcastId: '',
    contacts: [],
    selectedContacts: [],
  }

  async componentDidMount () {
    const { user } = this.props;
    const contacts = await getContactsList(user);
    const users = await Promise.all(contacts.map(userId => getUser(user, userId)));
    this.setState({
      contacts: users,
    });
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    })
  }

  onSelect = (name, option) => {
    this.setState({
      [name]: option,
    })
    if (name === 'type') {
      this.setState({
        isLiveBroadcast: option.value === 'live',
        broadcastId: option.value === 'live' ? uuidv4() : '',
      });
    }

    if (name === 'category' && option.value === 'public') {
      this.setState({
        selectedContacts: [],
      })
    }
  }

  onSelectDate = (date) => {
    this.setState({
      date,
    })
  }

  onSelectTime = (time) => {
    this.setState({
      time,
    })
  }

  startBroadcast = (res) => {
    const { broadcast_id } = res;
    const pathname = `/live-broadcast/${broadcast_id}`;
    const data = {
      isHost: true,
    }
    history.push({
      pathname,
      state: data,
    });
  }

  onUpload = (type, data) => {
    if (type === 'image') {
      this.setState({
        thumbnail: data.link
      })
    } else if (type === 'video') {
      this.setState({
        attachment: data.link
      })
    }
  }

  onSubmit = () => {
    if (this.state.isLiveBroadcast) {
      const { title, category, cost, description } = this.state;
      if (!title || !category.value || !description) {
        window.alert('Please fill necessary information');
        return;
      }

      const start_date = moment().format('YYYY-MM-DD');
      const start_time = moment().format('HH:mm');
      const start_at = `${start_date}T${start_time}:00.0000`

      const data = Object.assign({}, {
        title
      }, {
        video_type: 'live',
        category: category.value,
        desc: description,
        description: description,
        cost: parseInt(cost),
        start_at: start_at,
        attachments: {
          videos: [],
          banners: [],
        },
      });
      const { createBroadCast, user } = this.props;
      createBroadCast(data, user, (res) => {
        this.startBroadcast(res);
      });
      return;
    }
  
    const { title, cost, type, category, date, time, description, attachment, thumbnail } = this.state;
    if (!title || !category.value || !attachment || !thumbnail || !type.value) {
      window.alert('Please fill necessary information');
      return;
    }
    const start_date = moment(date[0]).format('YYYY-MM-DD');
    const start_time = moment(time[0]).format('HH:mm');
    const start_at = `${start_date}T${start_time}:00.0000`
    const data = Object.assign({}, {
      title
    }, {
      video_type: type.value,
      category: category.value,
      desc: description,
      description: description,
      cost: parseInt(cost),
      start_at: start_at,
      attachments: {
        videos: [attachment],
        banners: [thumbnail],
      },
      users: this.state.selectedContacts.map(option => option.value)
    });
    const { createBroadCast, user } = this.props;
    createBroadCast(data, user, () => {
      window.alert('Your broadcast has been created. You can view it in my broadcasts');
      this.props.changeTab('my-broadcast')
    });
  }

  render() {
    const { title, cost, type, category, date, time, description, isLiveBroadcast, broadcastId, contacts } = this.state;
    const copyColor = this.state.copied ? "blue" : "black";
    const { origin } = window.location;
    const url = `${origin}/live-broadcast/${broadcastId}`;
    const contactOptions = contacts.map(contact => {
      return Object.assign({}, {
        label: contact.fullname,
        value: contact.user_id,
      })
    })

    return (
      <Grid className="w-100">
        <Grid.Column width={16}>
          <div className="mt-1">
            <Grid className="w-100">
              <Grid.Column width={8}>
                <h1 className="tex-bold-600 ml-2">
                  Create Broadcast
                </h1>
                <div className="text-muted  ml-2">
                  Configure Broadcast Options
                </div>
              </Grid.Column>
            </Grid>
          </div>
          <div className="mt-2">
            <Grid className="w-100 ml-2" centered>
              <Grid.Column width={8}>
              <FormGroup>
                <h3 className="text-bold-600">Broadcast Title</h3>
                <Input type="text" id="title" name="title" placeholder="The Power of You" value={title} onChange={this.onChange}/>
              </FormGroup>
              </Grid.Column>
              <Grid.Column width={4}>
              <FormGroup>
                <h3 className="text-bold-600">Cost</h3>
                <Input type="number" id="cost" name="cost" placeholder="500 Credits" value={cost} onChange={this.onChange}/>
              </FormGroup>
              </Grid.Column>
              <Grid.Column width={4}>
              <FormGroup>
                <h3 className="text-bold-600">Video Type</h3>
                <Select
                  className="React"
                  classNamePrefix="select"
                  value={type}
                  name="type"
                  options={VIDEO_OPTIONS}
                  onChange={(option) => {this.onSelect('type', option)}}
                />
              </FormGroup>
              </Grid.Column>
            </Grid>
            <Grid className="w-100 ml-2">
              <Grid.Column width={8}>
              <FormGroup>
                <h3 className="text-bold-600">Broadcast Category</h3>
                <Select
                  className="React"
                  classNamePrefix="select"
                  defaultValue={category}
                  name="category"
                  options={CATEGORIES}
                  onChange={(option) => {this.onSelect('category', option)}}
                />
              </FormGroup>
              </Grid.Column>
              {!isLiveBroadcast &&
                <Grid.Column width={4}>
                  <h3 className="text-bold-600">Broadcast Date</h3>
                  <Flatpickr
                    className="form-control"
                    value={date}
                    name="date"
                    onChange={this.onSelectDate}
                  />
                </Grid.Column>
              }
              {!isLiveBroadcast &&
                <Grid.Column width={4}>
                <h3 className="text-bold-600">Broadcast Time</h3>
                <Flatpickr
                  className="form-control"
                  value={time}
                  name="time"
                  options={{
                    enableTime: true,
                    noCalendar: true,
                    dateFormat: "H:i",
                  }}
                  onChange={this.onSelectTime}
                />
                </Grid.Column>
              }
            </Grid>
            <Grid className="w-100 ml-2">
              <Grid.Column width={8}>
              <h3 className="text-bold-600">Broadcast Description</h3>
              <Input
                  type="textarea"
                  name="description"
                  id="exampleText"
                  rows="3"
                  placeholder="Broadcast Description"
                  value={description}
                  onChange={this.onChange}
                />
              </Grid.Column>
              {!isLiveBroadcast &&
                <RecordedBroadcast onUpload={this.onUpload}/>
              }
            </Grid>
            {category.value === 'private' && <Grid className="w-100 ml-2">
              <Grid.Column width={8}>
                <h3 className="text-bold-600">Invite Users</h3>
                <Select isMulti name="users" options={contactOptions} className="React mt-1" classNamePrefix="select" onChange={(options) => {
                    this.setState({
                      selectedContacts: options
                    })
                  }}
                />
              </Grid.Column>
            </Grid>}
            {isLiveBroadcast &&
              <Grid className="w-100 ml-2">
                <Grid.Column width={16}>
                  <div>
                  <span>
                    <b>Invite to broadcast</b> : {url}
                    <span className="ml-1">
                      <CopyToClipboard text={url} onCopy={() => this.setState({copied: true})}>
                        <Icon name="copy outline" size="large" className="cursor-pointer mr-1" color={copyColor}/>
                      </CopyToClipboard>
                    </span>
                  </span>
                  </div>
                </Grid.Column>
              </Grid>
            }
            <Grid className="w-100 ml-2" centered>
              <Grid.Column width={8}>
                <Button style={{cursor: 'pointer'}} className="" fluid primary color="grey" onClick={this.onSubmit}>
                  <span style={{color: 'white'}}>{!isLiveBroadcast ? 'Create' : 'Go Live'}</span>
                </Button>
              </Grid.Column>
            </Grid>
          </div>
        </Grid.Column>
      </Grid>
    )
  }
}

const typeMap = {
  fileImageInput: 'image',
  fileVideoInput: 'video',
  fileInput: 'file',
}

const linkTypeMap = {
  image: 'imageLink',
  video: 'videoLink',
}

class RecordedBroadcast extends Component {
  state = {}

  onUploadClick = (ref) => {
    this.refs[ref].click();
    this.setState({
      uploadType: typeMap[ref],
    })
  }

  uploadFile() {
    const { file, uploadType } = this.state;
    this.setState({
      uploading: true,
    });
    const uploadLinkType = linkTypeMap[uploadType]; 
    onUpload(file, (data) => {
      this.setState({
        uploading: false,
        [uploadLinkType]: data.link,
      });
      this.props.onUpload(uploadType, data);
    }, (err) => {
      console.log(err);
    });
  }

  _handleImageChange = (event) => {
    event.preventDefault();

    const reader = new FileReader();
    const file = event.target.files[0];
    const self = this;

    reader.onloadend = () => {
      self.setState({
        file,
      });
      self.uploadFile();
    };

    // To clear currently loaded file from input
    event.target.value = null;
    reader.readAsDataURL(file);
  }

  render() {
    const { uploading, uploadType, imageLink, videoLink } = this.state;
    const uploadingVideo = uploading && uploadType === 'video';
    const uploadingImage = uploading && uploadType === 'image';
    return (
      <Fragment>
        <Grid.Column width={4}>
          <FormGroup className="text-center">
            <h3 className="text-bold-600">Broadcast Video</h3>
            <div>
              <Button icon loading={uploadingVideo} style={{left: 0}} onClick={this.onUploadClick.bind(this, 'fileVideoInput')}>
                {!uploadingVideo && <Icon name="attach" size={15} />}
                {!uploadingVideo && <input type="file" accept="video/*" ref="fileVideoInput" className="attachment-file-input" onChange={this._handleImageChange}></input>}
              </Button>
            </div>
            <a href={videoLink} target="_blank" rel="noopener noreferrer" style={{wordBreak: 'break-all'}}>{videoLink}</a>
          </FormGroup>
        </Grid.Column>
        <Grid.Column width={4}>
          <FormGroup className="text-center">
            <h3 className="text-bold-600">Broadcast Thumbnail</h3>
            <div>
            <Button icon loading={uploadingImage} style={{left: 0}} onClick={this.onUploadClick.bind(this, 'fileImageInput')}>
              {!uploadingImage && <Icon name="attach" size={15} />}
              {!uploadingImage && <input type="file" accept="image/*" ref="fileImageInput" className="attachment-file-input" onChange={this._handleImageChange}></input>}
            </Button>
            </div>
            <a href={imageLink} target="_blank" rel="noopener noreferrer" style={{wordBreak: 'break-all'}}>{imageLink}</a>
          </FormGroup>
        </Grid.Column>
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {
  createBroadCast,
})(Index);