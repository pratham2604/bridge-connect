import AgoraRTC from "agora-rtc-sdk";
const APP_ID = '8e203d0a624f402aaff54f7a38e2781d';

class VideoStream {
  localStream = {};

  remoteStreams = {};
  isAudio = true;
  isVideo = true;

  constructor(userId, updateStreamsinParent, shareScreen = false) {
    this.localStream = AgoraRTC.createStream({
      streamID: userId,
      audio: true,
      video: !shareScreen,
      screen: !!shareScreen
    });
    this.client = AgoraRTC.createClient({ mode: "live", codec: "h264" });
    this.initClient();
    this.updateStreamsinParent = updateStreamsinParent;
  }

  close = () => {
    this.localStream.close();
  }

  stop = () => {
    this.localStream.stop();
  }

  shareScreenInit = (userId) => {
    this.localStream = AgoraRTC.createStream({
      streamID: userId,
      audio: true,
      video: false,
      screen: true
    });
  }

  initClient = () => {
    this.client.init(APP_ID, () => {
      console.log('AgoraRTC client initialized');
    }, (err) => {
      console.log("AgoraRTC client init failed", err);
    });
    this.subscribeToClient(this.client);
  };

  initLocalStream = (id, roomId, userId) => {
    this.localStream.init(() => {
      console.log("getUserMedia successfully");
      this.localStream.play(id);
      this.joinChannel(roomId, userId);
    }, (err) => {
      console.log("getUserMedia failed", err);
    });
  };

  joinChannel = (roomId, userId) => {
    this.client.join(null, roomId, userId, (uid) => {
      const logMessage = "User " + uid + " join channel successfully";
      console.log(logMessage);
      this.client.publish(this.localStream, (err) => {
        console.log("Publish local stream error: " + err);
      });

      this.client.on("stream-published", (evt) => {
        const logMessage = "Publish local stream successfully";
        console.log(logMessage);
      });
    }, (err) => {
      console.log("Join channel failed", err);
    });
  };

  subscribeToClient = () => {
    this.client.on("stream-added", this.onStreamAdded);
    this.client.on("stream-subscribed", this.onRemoteClientAdded);
    this.client.on("stream-removed", this.onStreamRemoved);
    this.client.on("peer-leave", this.onPeerLeave);
  };

  onStreamAdded = (event) => {
    console.log("on stream added ##################", event)
    let stream = event.stream;
    const message = "New stream added: " + stream.getId()
    console.log(message);
    const { remoteStreams } = this;
    const newId = stream.getId()
    const updatedStreams = Object.assign({}, remoteStreams, {
      [newId]: stream,
    });

    this.updateStreams(updatedStreams);
    // Subscribe after new remoteStreams state set to make sure
    // new stream dom el has been rendered for agora.io sdk to pick up
    this.client.subscribe(stream, (err) => {
      console.log("Subscribe stream failed", err);
    });
  };

  onReplaceTrack = (track) => {
    console.log(track);
    this.localStream.replaceTrack(track);
  }

  onStreamRemoved = evt => {
    const me = this;
    const stream = evt.stream;
    if (stream) {
      const streamId = stream.getId();
      const { remoteStreams } = me;
      stream.stop();
      delete remoteStreams[streamId];
      this.updateStreams(remoteStreams);
      console.log("Remote stream is removed " + stream.getId());
    }
  };

  onPeerLeave = evt => {
    const me = this;
    const stream = evt.stream;
    if (stream) {
      const streamId = stream.getId();
      const { remoteStreams } = me;
      stream.stop();
      delete remoteStreams[streamId];
      this.updateStreams(remoteStreams);
      console.log("Remote stream is removed " + stream.getId());
    }
  };

  onRemoteClientAdded = event => {
    console.log("on remote client is addede @@@@@@@@@@@@@@@@@@")
    const remoteStream = event.stream;
    const message = 'Remote stream added ' + remoteStream.getId();
    console.log(message);
    this.remoteStreams[remoteStream.getId()].play(
      "agora_remote " + remoteStream.getId()
    );
    this.updateStreams(this.remoteStreams);
  };

  updateStreams = (remoteStreams) => {
    this.remoteStreams = remoteStreams;
    this.updateStreamsinParent(remoteStreams);
  }

  leaveMeeting = () => {
    this.localStream.close();
    this.client.leave();
    this.updateStreams(this.remoteStreams);
  }

  toggleAudio = () => {
    if (this.isAudio) {
      this.localStream.muteAudio();
    } else {
      this.localStream.unmuteAudio();
    }
    this.isAudio = !this.isAudio;
    return this.isAudio;
  }

  toggleVideo = () => {
    if (this.isVideo) {
      this.localStream.muteVideo();
    } else {
      this.localStream.unmuteVideo();
    }
    this.isVideo = !this.isVideo;
    return this.isVideo;
  }
}

export default VideoStream;