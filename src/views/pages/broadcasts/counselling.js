import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';

class Index extends Component {
  render() {
    return (
      <Grid className="w-100">
        <Grid.Column width={16}>
          <div className="text-center mt-3">
            <h1 className="tex-bold-600">
              Coming Soon ...
            </h1>
          </div>
        </Grid.Column>
      </Grid>
    )
  }
}

export default Index;