import React, { Component } from 'react';
import * as Icon from "react-feather"
import { Button, Card } from 'reactstrap';
import Payment from './payments';
import { connect } from "react-redux";
import { getOrders } from '../../../redux/actions/broadcasts/index';

class FixedSidebar extends Component {
  state = {
    activeTab: 'my-broadcast',
    showModal: false,
  }

  selectTab = (id) => {
    this.setState({
      activeTab: id
    });
    this.props.changeTab(id);
  }

  toggleModal = () => {
    this.setState({
      showModal: !this.state.showModal
    })
  }

  async componentDidMount() {
    const { user } = this.props;
    const data = await getOrders(user);
    console.log(data);
  }

  render() {
    const { user } = this.props;
    const { activeTab, showModal } = this.state;
    return (
      <div className="p-1 w-100">
        <div style={{display: 'flex'}} className="mt-2">
          <h1 className="text-bold-600">
            Broadcasts
          </h1>
          <div style={{flex: 1}} className="text-right">
            <Button.Ripple outline style={{padding: '0.9rem'}} onClick={this.selectTab.bind(this, 'create-broadcast')}>
              <Icon.Plus size={20} />
            </Button.Ripple>
          </div>
        </div>
        <div className="mt-2">
          <div className="text-muted mt-1 mb-1">
            Quick Actions
          </div>
          <div>
            {MENU.slice(4, 7).map((menu, index) => {
              const backgroundColor = menu.id === activeTab ? '#EFF7FF' : '#FFF';
              const color = menu.id === activeTab ? '#007AFF': '#333';
              return (
                <div key={index} className="menu p-1" style={{display: 'flex', cursor: 'pointer', backgroundColor, color }} onClick={this.selectTab.bind(this, menu.id)}>
                  {menu.icon}
                  <h4 className="text-bold-600 mt-0 text-left pl-1" style={{flex: '1'}}>{menu.title}</h4>
                </div>
              )
            })}
          </div>
          <div className="text-muted mt-1 mb-1">
            Category
          </div>
          <div>
            {MENU.slice(0, 3).map((menu, index) => {
              const backgroundColor = menu.id === activeTab ? '#EFF7FF' : '#FFF';
              const color = menu.id === activeTab ? '#007AFF': '#333';
              return (
                <div key={index} className="menu p-1" style={{display: 'flex', cursor: 'pointer', backgroundColor, color }} onClick={this.selectTab.bind(this, menu.id)}>
                  {menu.icon}
                  <h4 className="text-bold-600 mt-0 text-left pl-1" style={{flex: '1'}}>{menu.title}</h4>
                </div>
              )
            })}
          </div>
          <div className="mt-1 w-100">
            <Card>
              <div style={{display: 'flex'}} className="p-1">
                <div style={{flex: '1'}}>
                  <div className="text-muted">Credit Balance</div>
                  <h3 className="mt-1">
                    5000
                  </h3>
                </div>
                <Button.Ripple outline className="round text-right" style={{padding: '0.5em 0.75em'}} onClick={this.toggleModal}>
                  <Icon.Plus size={20} />
                </Button.Ripple>
              </div>
            </Card>
          </div>
          <div>
            {MENU.slice(7, 9).map((menu, index) => {
              const backgroundColor = menu.id === activeTab ? '#EFF7FF' : '#FFF';
              const color = menu.id === activeTab ? '#007AFF': '#333';
              return (
                <div key={index} className="menu p-1" style={{display: 'flex', cursor: 'pointer', backgroundColor, color }} onClick={this.selectTab.bind(this, menu.id)}>
                  <h4 className="text-bold-600 mt-0 text-center pl-1" style={{flex: '1'}}>{menu.title}</h4>
                </div>
              )
            })}
          </div>
        </div>
        <Payment onClose={this.toggleModal} open={showModal} user={user}/>
      </div>
    )
  }
}

const MENU = [{
  id: 'featured',
  title: 'Featured',
  icon: <Icon.Activity size={20} className="mr-1" />
}, {
  id: 'free',
  title: 'Free',
  icon: <Icon.Airplay size={20} className="mr-1" />
}, {
  id: 'upcoming',
  title: 'Upcoming',
  icon: <Icon.Award size={20} className="mr-1" />
}
, {
  id: 'live-broadcast',
  title: 'Live Broadcast',
  icon: <Icon.Battery size={20} className="mr-1" />
}
, {
  id: 'create-broadcast',
  title: 'Create Broadcast',
  icon: <Icon.Hash size={20} className="mr-1" />
}, {
  id: 'my-broadcast',
  title: 'My Broadcasts',
  icon: <Icon.Film size={20} className="mr-1" />
}, {
  id: 'public-broadcast',
  title: 'Public Broadcasts',
  icon: <Icon.Home size={20} className="mr-1" />
}, {
  id: 'mentorship-program',
  title: 'Mentorship program',
}, {
  id: 'counselling-program',
  title: 'Counselling program',
}]

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {
})(FixedSidebar);