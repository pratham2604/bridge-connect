import React, { Component } from 'react';
import { Button, Grid, Modal } from 'semantic-ui-react';
import {
  FormGroup,
  Input,
} from "reactstrap"
import Axios from 'axios';
const APP_ID = 'rzp_test_QAqKkL8Tf3FaIJ';
const short = require('short-uuid');
const translator = short(); 

class Index extends Component {
  state = {
    amount: 0,
  }

  onChange = (e) => {
    const { value } = e.target;
    this.setState({
      amount: value,
    })
  }

  onSubmit = () => {
    const { amount } = this.state;
    if (amount > 0) {
      this.placeOrder(amount * 100)
    } else {
      window.alert("Please enter amount greater than 0")
    }
  }

  placeOrder = async (amount) => {
    const { user } = this.props;
    const { token } = user;
    try {
      const response = await Axios.post('api/v1/payments/orders/', {
        amount,
        receipt: `receipt_${translator.new()}`,
      }, {
        headers: {
          Authorization: `Token ${token}`
        }
      });
      
      const { data } = response || {};
      const { order_id } = data;
      const modifiedData = {
        order_id,
        amount_due: amount,
      }
      this.openRazorPay(modifiedData);
    } catch (error) {
      const { message, response = {} } = error;
      const { description } = response.data || {};
      this.setState({
        apiError: description || message,
        isError: true,
        loading: false,
      });
    }
  }

  openRazorPay = (order) => {
    const { amount_due, order_id } = order;
    const orderInfo = {
      amount: amount_due,
      currency: 'INR',
      order_id: order_id,
    };
    const customerInfo = {
      prefill: {
        name: 'Prathamesh Palyekar',
        email: 'prathamesh@test.com',
        contact: '9898989898'
      },
    };
    const companyInfo = {
      name: 'Test Corp',
      description: 'Test Transaction',
    };
    const razorPayData = Object.assign({}, orderInfo, customerInfo, companyInfo, {
      key_id: APP_ID,
      handler: this.verifyPayment,
      notes: {
        address: 'Pratham Corp office'
      },
      theme: {
        color: '#F37254'
      }
    });
    const razorpay = new window.Razorpay(razorPayData);
    razorpay.open();
  }

  verifyPayment = async (paymentData) => {
    const { user } = this.props;
    const { token } = user;
    const modifiedData = {
      order_id: paymentData.razorpay_order_id,
      payment_id: paymentData.razorpay_payment_id,
      signature: paymentData.razorpay_signature,
    }
    try {
      await Axios.post('/api/v1/payments/orders/verify', modifiedData, {
        headers: {
          Authorization: `Token ${token}`
        }
      });
      this.setState({
        loading: false,
        paymentSuccess: true,
      });
      this.props.onClose();
    } catch (error) {
      const { message, response = {} } = error;
      const { description } = response.data || {};
      this.setState({
        apiError: description || message,
        isError: true,
        loading: false,
      });
      window.alert(description || message);
    }
  }

  render() {
    const { open, onClose } = this.props;
    const { amount } = this.state;

    return (
      <Modal open={open} onClose={onClose} style={{height: 'auto', width: '400px', top: '35%', left: '35%'}} className="p-1 payment-modal">
        <Grid className="w-100 ml-0 mr-0 h-100" centered>
          <Grid.Column width={16} className="text-center">
            <FormGroup>
              <h3 className="text-bold-600">Amount</h3>
              <Input type="number" id="cost" name="amount" placeholder="500 Credits" value={amount} onChange={this.onChange}/>
            </FormGroup>
            <Button style={{cursor: 'pointer'}} className="" fluid primary color="grey" onClick={this.onSubmit}>
              <span style={{color: 'white'}}>Add Credits</span>
            </Button>
          </Grid.Column>
        </Grid>
      </Modal>
    )
  }
}

export default Index;