import React, { useEffect, useState, useMemo } from 'react'
import { Segment, Grid } from 'semantic-ui-react';
import * as Icon from "react-feather"
import { useGlobalState, useGlobalMutation } from './container'
import useStream from './use-stream'
import RTCClient from './rtc-client'
import StreamPlayer from './stream-player'
import { history } from '../../../../history';
import ChatRoom from './chatRoom';

const MeetingPage = (props) => {
  const stateCtx = useGlobalState()
  const mutationCtx = useGlobalMutation()
  
  const client = new RTCClient()
  if (!client._created) {
    client.createClient({ codec: 'vp8', mode: 'live' })
    client._created = true
  }
  const localClient = client;

  const [localStream, currentStream] = useStream(localClient)
  const [muteVideo, setMuteVideo] = useState(true)
  const [muteAudio, setMuteAudio] = useState(true)
  const [sharingScreen, setScreenSharing] = useState(false)
  const [showChatBox, toggleChat] = useState(false)

  const config = useMemo(() => {
    return {
      token: null,
      channel: props.channel,
      muteVideo: muteVideo,
      muteAudio: muteAudio,
      uid: props.uid,
      host: props.isHost,
      // beauty: stateCtx.beauty
    }
  }, [muteVideo, muteAudio, props.channel, props.isHost, props.uid])

  // useEffect(() => {
  //   return () => {
  //     localClient && localClient.leave(() => mutationCtx.clearAllStream())
  //   }
  // }, [localClient])

  useEffect(() => {
    if (
      config.channel &&
      localClient._created &&
      localClient._joined === false
    ) {
      localClient
        .join(config)
        .then((uid) => {
          if (config.host) {
            localClient.publish()
          }
          // mutationCtx.updateConfig({ uid })
          mutationCtx.stopLoading()
        })
        .catch((err) => {
        })
    }
  }, [localClient._joined, localClient._created])

  const handleClick = (name) => {
    return (evt) => {
      evt.stopPropagation()
      switch (name) {
        case 'video': {
          muteVideo
            ? localStream.muteVideo()
            : localStream.unmuteVideo()
          setMuteVideo(!muteVideo)
          break
        }
        case 'audio': {
          muteAudio
            ? localStream.muteAudio()
            : localStream.unmuteAudio()
          setMuteAudio(!muteAudio)
          break
        }
        case 'screen': {
          if (sharingScreen) {
            mutationCtx.setScreen(false)
            localClient
              .createRTCStream({
                token: null,
                channel: props.channel,
                muteVideo: muteVideo,
                muteAudio: muteAudio,
                uid: props.uid,
                host: props.isHost,
              })
              .then(() => {
                setScreenSharing(false);
                localClient.publish()
                mutationCtx.setScreen(false)
              })
              .catch((err) => {
                console.log(err)
                mutationCtx.toastError(`Media ${err.info}`)
                // history.push('/')
              })
          } else {
            localClient
              .createScreenSharingStream({
                token: null,
                channel: props.channel,
                uid: props.uid,
              })
              .then((result) => {
                console.log(result)
                setScreenSharing(true);
                localClient.publish()
                mutationCtx.setScreen(true)
              })
              .catch((err) => {
                console.log(err)
              })
          }
          break
        }
        case 'profile': {
          break
        }
        default:
          throw new Error(`Unknown click handler, name: ${name}`)
      }
    }
  }

  const handleDoubleClick = (stream) => {
    mutationCtx.setCurrentStream(stream)
  }

  // const otherStreams = useMemo(() => {
  //   return stateCtx.streams.filter(
  //     (it) => it.getId() !== currentStream.getId()
  //   )
  // }, [stateCtx.streams, currentStream])

  return (
    <Grid className="h-100">
      <Grid.Column width={showChatBox ? 12 : 16}>
        <div className="meeting h-100 d-flex" style={{flexDirection: 'column'}}>
          <div className="current-view" style={{flex: '1'}}>
            {currentStream ? (
              <StreamPlayer
                className={'main-stream-profile'}
                showProfile={stateCtx.profile}
                local={
                  config.host
                    ? currentStream &&
                      localStream &&
                      currentStream.getId() === localStream.getId()
                    : false
                }
                stream={currentStream}
                onDoubleClick={handleDoubleClick}
                uid={currentStream.getId()}
                domId={`stream-player-${currentStream.getId()}`}
              >
              </StreamPlayer>
            ) : null}
            <div className="stream-container">
              {(stateCtx.otherStreams || []).filter((stream) => {
                console.log(stream, currentStream);
                return true;
              }).map((stream, index) => (
                <StreamPlayer
                  className={'stream-profile'}
                  showProfile={stateCtx.profile}
                  local={
                    config.host
                      ? stream.getId() === localStream && localStream.getId()
                      : false
                  }
                  key={`${index}${stream.getId()}`}
                  stream={stream}
                  isPlaying={stream.isPlaying()}
                  uid={stream.getId()}
                  domId={`stream-player-${stream.getId()}`}
                  onDoubleClick={handleDoubleClick}
                  showUid={true}
                ></StreamPlayer>
              ))}
            </div>
          </div>
          {config.host && (
            <Segment>
              <Grid>
                <Grid.Row className="p-1">
                  <Grid.Column width={4}></Grid.Column>
                  <Grid.Column width={8}>
                    <div className="text-center">
                      {!muteAudio ?
                        <Icon.MicOff size={20} className="mr-1 cursor-pointer" onClick={handleClick('audio')}/> :
                        <Icon.Mic size={20} className="mr-1 cursor-pointer" onClick={handleClick('audio')}/>
                      }
                      <Icon.PhoneOff size={20} className="mr-1 cursor-pointer" onClick={() => {
                        localClient.leave().then(() => {
                          mutationCtx.clearAllStream()
                          // mutationCtx.resetState()
                          history.push('/broadcasts')
                        })
                      }}/>
                      {!muteVideo ?
                        <Icon.VideoOff size={20} className="mr-1 cursor-pointer" onClick={handleClick('video')}/> :
                        <Icon.Video size={20} className="mr-1 cursor-pointer" onClick={handleClick('video')}/>
                      }                  
                      <Icon.Airplay size={20} className="mr-1 cursor-pointer" onClick={handleClick('screen')}/>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={4}>
                    <Icon.MessageSquare size={20} className="float-right mr-1 cursor-pointer" onClick={(e) => {toggleChat(!showChatBox)}}/>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          )}
          {!config.host && (
            <Segment>
              <Grid>
                <Grid.Row className="p-1">
                  <Grid.Column width={4}></Grid.Column>
                  <Grid.Column width={8}>
                    <div className="text-center">
                      <Icon.PhoneOff size={20} className="mr-1 cursor-pointer" onClick={() => {
                        localClient.leave().then(() => {
                          mutationCtx.clearAllStream()
                          // mutationCtx.resetState()
                          history.push('/broadcasts')
                        })
                      }}/>
                      <Icon.MessageSquare size={20} className="mr-1 cursor-pointer" onClick={(e) => {toggleChat(!showChatBox)}}/>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={4}></Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          )}
        </div>
      </Grid.Column>
      {showChatBox &&
        <Grid.Column width={4} className="h-100">
          <Segment className="h-100">
            <ChatRoom broadcastId={props.channel} user={props.user}/>
          </Segment>
        </Grid.Column>
      }
    </Grid>
  )
}

export default React.memo(MeetingPage)
