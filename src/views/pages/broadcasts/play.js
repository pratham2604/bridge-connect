import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import * as Icon from "react-feather"
import ReactPlayer from 'react-player'
import ChatRomm from './chatRoom';
import { getBroadCast } from '../../../redux/actions/broadcasts/index'
import { connect } from "react-redux";
import { history } from '../../../history';

class Index extends Component {
  state = {
    activeIndex: 0,
    broadcast: {},
  }

  async componentDidMount() {
    const { user, location } = this.props;
    const { pathname = '' } = location || {};
    const broadcastId = pathname.split('/').pop();
    const broadcast = await getBroadCast(user, broadcastId);
    this.setState({
      broadcast,
    });
  }

  updateIndex = (index) => {
    this.setState({
      activeIndex: index,
    })
  }

  goBack = () => {
    history.push('/broadcasts');
  }

  render() {
    const { broadcast } = this.state;
    const { attachments = {}, title } = broadcast;
    const videoUrl = (attachments.videos || [])[0];
    const { user, location } = this.props;
    const { pathname = '' } = location || {};
    const broadcastId = pathname.split('/').pop();

    return (
      <div>
        <Grid className="w-100 ml-0 mr-0 h-100">
          <Grid.Column width={12}>
            <div className="h-100">
            <div className="d-flex w-100 p-1">
              <div onClick={this.goBack} style={{flex: 1, cursor: 'pointer'}}>
                <Icon.ArrowLeft size={20}/>
                <h4 className="text-bold-400 d-inline pl-1">Back</h4>
                <h1 className="text-bold-600 d-inline pl-1">{title}</h1>
              </div>
              <div>
                <Icon.Info size={20} />
              </div>
            </div>
            <div>
              <ReactPlayer
                className='react-player'
                width='100%'
                height='calc(100vh - 64px)'
                url={videoUrl}
                controls={true}
              />
            </div>
            </div>
          </Grid.Column>
          <Grid.Column width={4} className="" style={{backgroundColor: '#333', opacity: '1', height: '100vh'}}>
            <ChatRomm broadcastId={broadcastId} user={user} />
          </Grid.Column>
        </Grid>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {
})(Index);