import React from "react"
import Sidebar from "react-sidebar"
import { ContextLayout } from "../../../utility/context/Layout"
import "../../../assets/scss/pages/app-email.scss"
import FixedSidebar from './sidebar';
import Featured from './featured';
import Free from './free';
import MyBroadCasts from './mybroadcasts';
import PublicBroadCasts from './publicBroadcasts';
// import Series from './series';
import Membership from './membership';
import Counselling from './counselling';
import Upcoming from './upcoming';
import CreateBroadcast from './createBroadcast';
import DetailedBroadcast from './detailedBroadcast';
import { history } from "../../../history"
import { connect } from "react-redux";

const mql = window.matchMedia(`(min-width: 992px)`)

class Index extends React.Component {
  state = {
    composeMailStatus: false,
    sidebarDocked: mql.matches,
    sidebarOpen: false,
    activeTab: 'my-broadcast'
  }

  handleComposeSidebar = status => {
    if (status === "open") {
      this.setState({
        composeMailStatus: true
      })
    } else {
      this.setState({
        composeMailStatus: false
      })
    }
  }

  componentDidMount() {
    const { location } = this.props;
    const { pathname } = location;
    const id = pathname.split('/').pop();
    if (id) {
      this.setState({
        activeTab: 'my-broadcast',
        broadcastId: id,
      })
    }
  }

  UNSAFE_componentWillMount() {
    mql.addListener(this.mediaQueryChanged)
  }

  componentWillUnmount() {
    mql.removeListener(this.mediaQueryChanged)
  }

  onSetSidebarOpen = open => {
    this.setState({ sidebarOpen: open })
  }

  mediaQueryChanged = () => {
    this.setState({ sidebarDocked: mql.matches, sidebarOpen: false })
  }

  handleMainAndComposeSidebar = () => {
    this.handleComposeSidebar("close")
    this.onSetSidebarOpen(false)
  }

  changeTab = (id) => {
    this.setState({
      activeTab: id,
    })
    history.push('/broadcasts')
  }

  render() {
    const { activeTab } = this.state;
    return (
      <React.Fragment>
        <div className="custom-fixed-sidebar-container position-relative" style={{height: '100%'}}>
          <div
            className={`app-content-overlay ${
              this.state.composeMailStatus || this.state.sidebarOpen ? "show" : ""
            }`}
            onClick={this.handleMainAndComposeSidebar}
          />
          <ContextLayout.Consumer>
            {context => (
              <Sidebar
                sidebar={
                  <FixedSidebar changeTab={this.changeTab}/>
                }
                docked={this.state.sidebarDocked}
                open={this.state.sidebarOpen}
                sidebarClassName="sidebar-content custom-app-sidebar d-flex"
                touch={false}
                contentClassName="sidebar-children"
                pullRight={context.state.direction === "rtl"}>
                <div className="w-100">
                  {activeTab === 'detailed-broadcast' && <DetailedBroadcast id={this.state.broadcastId} user={this.props.user}/>}
                  {activeTab === 'featured' && <Featured />}
                  {activeTab === 'free' && <Free />}
                  {activeTab === 'my-broadcast' && <MyBroadCasts changeTab={this.changeTab}/>}
                  {activeTab === 'public-broadcast' && <PublicBroadCasts />}
                  {activeTab === 'membership-program' && <Membership />}
                  {activeTab === 'counselling-program' && <Counselling />}
                  {activeTab === 'upcoming' && <Upcoming />}
                  {activeTab === 'create-broadcast' && <CreateBroadcast changeTab={this.changeTab}/>}
                </div>
              </Sidebar>
            )}
          </ContextLayout.Consumer>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {
})(Index);
