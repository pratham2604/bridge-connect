import React, { Component } from "react";
import { Tab, Icon, Button, Input } from 'semantic-ui-react';
import { getBroadCast, getBroadCastStats } from "../../../../redux/actions/broadcasts/index"
import { getUser } from '../../../../redux/actions/auth/loginActions'
import ChatBox from './chatbox';

export default class ChatRoom extends Component {
  state = {
    active: 1,
  }

  async componentDidMount() {
    const { roomId, user, dispatch, isHost } = this.props;
    const broadcast = await getBroadCast(user, roomId);
    const { title } = broadcast || {};
    this.setState({
      title,
      isHost,
    })
  }

  panes = [
    {
      menuItem: 'Stats',
      render: () => (
        <Tab.Pane>
          <Members roomId={this.props.roomId} user={this.props.user} isHost={this.state.isHost}/>
        </Tab.Pane>
      ),
    },
    {
      menuItem: 'Chat',
      render: () => (
        <Tab.Pane>
          <ChatBox roomId={this.props.roomId} user={this.props.user} />
        </Tab.Pane>
      ),
    },
  ]

  render() {
    const { title } = this.state;
    return (
      <div>
        <div className="mb-1">
          <span className="text-bold-600 mr-1">{title}</span>
        </div>
        <Tab panes={this.panes} />
      </div>
    )
  }
}

class Members extends Component {
  state = {
    host: {},
  }

  async componentDidMount() {
    const { roomId, user } = this.props;
    const broadcast = await getBroadCast(user, roomId);
    const broadcastStats = await getBroadCastStats(user, roomId);
    const { creator_id } = broadcast || {};
    const host = await getUser(user, creator_id)
    this.setState({
      host,
      stats: broadcastStats,
    })
  }

  render() {
    const { isHost } = this.props;
    const { host = {}, stats = {} } = this.state;
    return (
      <div>
        <div className="d-flex mb-1">
          <div className="pr-1">
            <span className="avatar avatar-md m-0">
              {host.avatar ?
                <img src={host.avatar} height="38" width="38" alt="profile"/> :
                <Avatar name={host.fullname} />
              }
            </span>
          </div>
          <div className="user-chat-info d-flex justify-content-center" style={{flexDirection: 'column'}}>
            <h5 className="text-bold-600 mb-0">
              {host.fullname}
              <span className="ml-1 text-bold-400 mr-1">(Host)</span>
            </h5>
          </div>
        </div>
        <div>
          <span className="text-bold-400">People Joined : {stats.num_clients}</span>
        </div>
      </div>
    )
  }
}

class Avatar extends React.Component {
  render() {
    const { name = '' } = this.props;
    const initial = name[0];

    return (
      <span className="avatar-section">
        {initial ?
          <span className="avatar" style={{padding: '1px 7px'}}>{initial}</span> :
          <span className="user-icon icon-user-circle"></span>
        }
      </span>
    )
  }
}
