import React, { Component } from "react";
import { Segment, Grid } from 'semantic-ui-react';
import * as Icon from "react-feather"
import VideoStream from './videoStream';
import { history } from "../../../../history";
import ChatRoom from './chatRoom';
import { muteRemoteStream } from "../../../../redux/actions/meeting/index";

const localStream = {
  getId: function() {
    return 'local_stream';
  }
}

class Call extends Component {
  state = {
    initialized: false,
    isScreenStreamInitialized: false,
    videoStream: {},
    remoteStreams: {},
    isAudio: this.props.isHost,
    isVideo: this.props.isHost,
    isSharingScreen: false,
    activeStream: 'local_stream',
    swapIndex: 0,
    showChatBox: true,
    swapVideos: false,
  };

  componentDidMount() {
    const { user, channel, isHost } = this.props;
    const videoStream = new VideoStream(user.user_id, this.updateRemoteStrams, false, !isHost);
    this.setState({
      videoStream,
      roomId: channel,
    });
  }

  updateRemoteStrams = (remoteStreams) => {
    this.setState({
      remoteStreams
    })
  } 

  async componentDidUpdate(prevProps, prevState) {
    const { channel, isHost } = this.props;
    if (!this.state.initialized && channel !== "") {
      this.startRoom(channel);
    }

    const { remoteStreams, swapVideos } = this.state;
    const isRemoteStreamPresent = Object.keys(remoteStreams).length > 0;
    if (!isHost && !swapVideos && isRemoteStreamPresent) {
      this.setState({
        swapVideos: true,
        swapIndex: 1,
      })
    }

    if (!isHost && swapVideos && !isRemoteStreamPresent) {
      this.setState({
        swapIndex: 0,
        swapVideos: false,
      })
    }
  }

  startRoom = (roomId) => {
    const { videoStream } = this.state;
    const { user } = this.props;
    this.setState({
      showRoom: true,
      initialized: true,
      roomId,
    }, () => {
      videoStream.initLocalStream('local_stream', roomId, user.user_id, () => {
        const { isAudio, isVideo } = this.state;
        if (!isAudio) {
          this.toggleMic();
        }
        if (!isVideo) {
          this.toggleVideo();
        }
      })
    })
  }

  componentWillUnmount() {
    const { videoStream } = this.state;
    videoStream.leaveMeeting();
  }

  leaveMeeting = () => {
    const { videoStream } = this.state;
    videoStream.leaveMeeting();
    // this.props.leaveMeeting();
    history.push('/broadcasts')
  }

  toggleMic = () => {
    const { videoStream } = this.state;
    const isAudio = videoStream.toggleAudio();
    this.setState({
      isAudio,
    })
  }

  toggleVideo = () => {
    const { videoStream } = this.state;
    const isVideo = videoStream.toggleVideo();
    this.setState({
      isVideo,
    });
  }

  toggleShareScreen = () => {
    const { videoStream, isSharingScreen, roomId } = this.state;
    const { user } = this.props;
    videoStream.close();
    videoStream.stop();
    videoStream.leaveMeeting();
    let newStream = null;
    if (!isSharingScreen) {
      newStream = new VideoStream(user.user_id, this.updateRemoteStrams, true);
    } else {
      newStream = new VideoStream(user.user_id, this.updateRemoteStrams);
    }
    newStream.initLocalStream('local_stream', roomId, user.user_id, () => {});
    this.setState({
      videoStream: newStream,
      isSharingScreen: !isSharingScreen,
    });
  }

  swapIndex = (index) => {
    this.setState({
      swapIndex: index,
    })
  }

  toggleChat = () => {
    this.setState({
      showChatBox: !this.state.showChatBox
    })
  }

  muteRemoteStream = async (data) => {
    const { user, meetingId } = this.props;
    await muteRemoteStream(user, meetingId, data);
  }

  render() {
    const { isAudio, isVideo, videoStream , isSharingScreen, swapIndex, showChatBox, roomId } = this.state;
    const { user, isHost, channel, dispatch } = this.props;
    const { showRoom } = this.state;
    const { remoteStreams = {} } = videoStream;
    if (!showRoom) {
      return null;
    }

    const presentVideoStreams = [];
    presentVideoStreams.push({
      stream: localStream,
      element: <div id="local_stream" className="h-100 w-100"></div>
    });
    Object.keys(remoteStreams).forEach(key => {
      const stream = remoteStreams[key];
      const streamId = stream.getId();
      if ((isHost && stream.hasVideo()) || !isHost) {
        presentVideoStreams.push ({
          stream,
          element: <div key={streamId} id={`agora_remote ${streamId}`} className="h-100 w-100"/>
        });
      }
    });
    let videoIndex = 0;

    return (
      <Grid className="h-100 m-1">
        <Grid.Column width={showChatBox ? 12 : 16}>
        <div className="d-flex flex-column position-relative h-100">
          {presentVideoStreams.map((streamElement, index) => {
            const { stream, element } = streamElement;
            if (index !== swapIndex) {
              videoIndex++;
            }
            return (
              <VideoStreamComponent 
                videoIndex={videoIndex - 1} 
                user={user} 
                index={index} 
                key={index} 
                element={element} 
                stream={stream} 
                swapIndex={swapIndex} 
                onSwapIndex={this.swapIndex} 
              />
            )
          })}
          <Segment>
            <Grid>
              <Grid.Row className="p-1">
                <Grid.Column width={4}>
                </Grid.Column>
                <Grid.Column width={8}>
                  <div className="text-center">
                    {!isAudio ?
                      isHost && <Icon.MicOff size={20} className="mr-1 cursor-pointer" onClick={this.toggleMic}/> :
                      isHost && <Icon.Mic size={20} className="mr-1 cursor-pointer" onClick={this.toggleMic}/>
                    }
                    <Icon.PhoneOff size={20} className="mr-1 cursor-pointer" onClick={this.leaveMeeting}/>
                    {!isVideo ?
                      isHost && <Icon.VideoOff size={20} className="mr-1 cursor-pointer" onClick={this.toggleVideo}/> :
                      isHost && <Icon.Video size={20} className="mr-1 cursor-pointer" onClick={this.toggleVideo}/>
                    }                  
                    {isHost && <Icon.Airplay size={20} className="mr-1 cursor-pointer" onClick={this.toggleShareScreen} style={{color: isSharingScreen ? 'green' : 'black'}}/>}
                  </div>
                </Grid.Column>
                <Grid.Column width={4}>
                  <Icon.MessageSquare size={20} className="float-right mr-1 cursor-pointer" onClick={this.toggleChat}/>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Segment>
        </div>
        </Grid.Column>
        {showChatBox &&
          <Grid.Column width={4} className="h-100">
            <Segment className="h-100">
              <ChatRoom roomId={channel} user={user} dispatch={dispatch} isHost={isHost}/>
            </Segment>
          </Grid.Column>
        }
      </Grid>
    );
  }
}

class VideoStreamComponent extends Component {
  render() {
    const { index, element, swapIndex, onSwapIndex, videoIndex } = this.props;

    if (index === swapIndex) {
      return (
        <div className="h-100 local-stream-container position-relative">
          {element}
        </div>
      )
    }

    const left = `${(150 * (videoIndex)) + 5}px`;
    return (
      <div className="remote-stream-container" style={{left}} onClick={() => {onSwapIndex(index)}}>
        {element}
      </div>
    )
  }
}

export default Call;