import React from "react"
import ReactDOM from "react-dom"
import { Input, Button } from "reactstrap"
import { Send } from "react-feather"
import PerfectScrollbar from "react-perfect-scrollbar"
import { connect } from "react-redux"
import { sendMessage, getBroadCastChats } from "../../../redux/actions/broadcasts/index"
import { getUser } from '../../../redux/actions/auth/loginActions'
import * as Icon from "react-feather"
import { Dropdown } from 'semantic-ui-react';
import '../../../assets/scss/pages/app-chat.scss';
// import Callroom from './callroom';

class ChatLog extends React.Component {
  state = {
    msg: "",
    activeUser: null,
    activeChat: null,
    chat: [],
    replyMessage: {},
  }

  componentDidMount() {
    this.scrollToBottom();
    this.fetchData();
  }

  async componentDidUpdate(prevPros) {
    this.scrollToBottom();
  }

  deleteMessage = async (messageId, forSelf = false) => {
    this.fetchData();
  } 

  fetchData = async () => {
    const { broadcastId, user } = this.props;

    const chats = await getBroadCastChats(user, broadcastId);
    const { messages } = chats;
    const userIds = [];
    messages.forEach(message => {
      const { sender_id } = message;
      if (!userIds.some(id => id === sender_id)) {
        userIds.push(sender_id);
      }
    })
    const members = await Promise.all(userIds.map(userId => getUser(user, userId)));
    const updatedMessages = messages.map(message => {
      return Object.assign({}, message, {
        user: members.find(member => member.user_id === message.sender_id) || {}
      })
    })
    this.setState({
      chat: updatedMessages,
    })
  }

  handleTime = (time_to, time_from) => {
    const date_time_to = new Date(Date.parse(time_to))
    const date_time_from = new Date(Date.parse(time_from))
    return (
      date_time_to.getFullYear() === date_time_from.getFullYear() &&
      date_time_to.getMonth() === date_time_from.getMonth() &&
      date_time_to.getDate() === date_time_from.getDate()
    )
  }

  scrollToBottom = () => {
    const chatContainer = ReactDOM.findDOMNode(this.chatArea)
    chatContainer.scrollTop = chatContainer.scrollHeight
  }

  sendMessage = async () => {
    const { broadcastId, user } = this.props;
    const { newMessage } = this.state;
    if (!newMessage) {
      return;
    }
    const msg = JSON.stringify({
      content: newMessage
    })
    const data = {
      msg,
      msg_type: 'text',
    }

    const { replyMessage } = this.state;
    if (replyMessage.msg_id) {
      const { msg_id } = replyMessage;
      data.reply_to = msg_id;
    }
    await sendMessage(user, data, broadcastId);
    this.fetchData();
    this.setState({
      newMessage: '',
      replyMessage: {}
    })
  }

  setReply = (replyMessage) => {
    this.setState({
      replyMessage,
    })
  }

  closeCallRoom = () => {
    this.setState({
      showCallRoom: false,
    })
  }

  render() {
    const { user } = this.props;
    const chats = this.state.chat.map(chat => Object.assign({}, chat));

    let renderChats = chats.reverse().map((chat, i) => {
      let renderAvatar = () => {
        return (
          <div className="chat-avatar">
            <div className="avatar m-0">
              {chat.user.avatar ? <img src={chat.user.avatar} height="40" width="40" alt="profile"/> :
              <Avatar name={chat.user.fullname} />}
            </div>
          </div>
        )
      }
      const { msg } = chat;
      let message = {};
      try {
        message = JSON.parse(msg)
      } catch(e) {
        message = {
          content: msg,
        }
      }
      const { content } = message;
      const { reply_to } = chat;
      let showReply = false;
      let replyMessageContent = '';
      let replyUser = {};
      if (reply_to) {
        showReply = true;
        const targetChat = chats.find(chat => chat.msg_id === reply_to.msg_id);
        const { msg } = targetChat || {};
        let message = {};
        replyUser = (targetChat || {}).user || {};
        if (msg) {
          try {
            message = JSON.parse(msg)
          } catch(e) {
            message = {
              content: '',
            }
          }
          message = JSON.parse(msg) || {};
          replyMessageContent = message.content;
        } else {
          replyMessageContent = 'Deleted Message'
        }
      }
      return (
        <React.Fragment key={i}>
          <div
            className={`chat ${
              chat.user.user_id !== user.user_id ? "chat-left" : "chat-right"
            }`}>
            {renderAvatar()}
            <div className="chat-body">
              <div className="chat-content">
                {showReply &&
                  <div className="text-left p-1" style={{borderRadius: '5px', backgroundColor: '#d3d3d3'}}>
                    <div className="text-bolo-600">{replyUser.fullname}</div>
                    <div>{replyMessageContent}</div>
                  </div>
                }
                <div>
                  {chat.user.user_id === user.user_id &&
                    <span className="mr-1">
                      {!chat.read ?
                        <Icon.Check size={15} /> :
                        <Icon.Check size={15} style={{color: '#00ffff'}}/>
                      }
                    </span>
                  }
                  {content}
                  <span className="chat-actions">
                    <Dropdown icon="chevron down" direction='left'>
                      <Dropdown.Menu>
                        <Dropdown.Item text="Reply" onClick={(e) => {this.setReply(chat)}}></Dropdown.Item>
                        <Dropdown.Item text="Delete For All" onClick={(e) => {this.deleteMessage(chat.msg_id)}}></Dropdown.Item>
                        <Dropdown.Item text="Delete For Me" onClick={(e) => {this.deleteMessage(chat.msg_id, true)}}></Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      )
    })

    const { replyMessage } = this.state;
    const { msg } = replyMessage || {};
    let message = {};
    let replyMessageContent = '';
    const replyUser = replyMessage.user;
    if (msg) {
      try {
        message = JSON.parse(msg)
      } catch(e) {
        message = {
          content: '',
        }
      }
      message = JSON.parse(msg) || {};
      replyMessageContent = message.content;
    }

    return (
      <div className="chat-application">
      <div className="chat-app-window" style={{height: '100%'}}>
        <div
          style={{height: '100%', flexDirection: 'column'}}
          className={`active-chat d-flex`}>
          <PerfectScrollbar style={{flex: '1', padding: '10px'}} className="user-chats" options={{ wheelPropagation: false}} ref={el => {this.chatArea = el }}>
            <div className="chats">{renderChats}</div>
          </PerfectScrollbar>
          <div className="chat-app-form">
            <div>
              {replyMessageContent &&
                <div className="mb-1 p-1 d-flex" style={{backgroundColor: '#c2c6dc', borderRadius: '5px'}}>
                  <div style={{flex: '1'}}>
                    <div className="text-bolo-600">{replyUser.fullname}</div>
                    <div>{replyMessageContent}</div>
                  </div>
                  <div className="p-1 cursor-pointer" onClick={(e) => {this.setState({replyMessage: {}})}}>
                    <Icon.X size={17} />
                  </div>
                </div>
              }
            </div>
            <form className="chat-app-input d-flex align-items-center" onSubmit={e => {e.preventDefault(); this.sendMessage();}}>
              <Input type="text" className="message mr-1 ml-50" placeholder="Type your message" value={this.state.newMessage}
                onChange={e => {
                  e.preventDefault()
                  this.setState({
                    newMessage: e.target.value
                  })
                }}
              />
              <Button color="primary" type="submit">
                <Send className="d-lg-none" size={15} />
                <span className="d-lg-block d-none">Send</span>
              </Button>
            </form>
          </div>
        </div>
      </div>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    user: state.auth.user,
    updateFromSocket: state.socket.newMessage
  }
}

class Avatar extends React.Component {
  render() {
    const { name = '' } = this.props;
    const initial = name[0];

    return (
      <span className="avatar-section">
        {initial ?
          <span className="avatar">{initial}</span> :
          <span className="user-icon icon-user-circle"></span>
        }
      </span>
    )
  }
}

const mapDispatchtoProps = (dispatch) => {
  return {
      dispatch
  };
};

export default connect(mapStateToProps, mapDispatchtoProps)(ChatLog)
