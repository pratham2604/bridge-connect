import React, { Component } from 'react';
import * as Icon from "react-feather"
import { Button } from 'reactstrap';

class FixedSidebar extends Component {
  state = {
    activeTab: 'my-schedule'
  }

  selectTab = (id) => {
    this.setState({
      activeTab: id
    });
    this.props.changeTab(id);
  }

  render() {
    const { activeTab } = this.state;
    return (
      <div className="p-1 w-100">
        <div style={{display: 'flex'}} className="mt-2">
          <h1 className="text-bold-600">
            Meetings
          </h1>
        </div>
        <div className="mt-2">
          <div className="text-muted mt-1 mb-1">
            Quick Actions
          </div>
          <div>
            {MENU.slice(0, 4).map((menu, index) => {
              const backgroundColor = menu.id === activeTab ? '#EFF7FF' : '#FFF';
              const color = menu.id === activeTab ? '#007AFF': '#333';
              return (
                <div key={index} className="menu p-1" style={{display: 'flex', cursor: 'pointer', backgroundColor, color }} onClick={this.selectTab.bind(this, menu.id)}>
                  {menu.icon}
                  <h4 className="text-bold-600 mt-0 text-left pl-1" style={{flex: '1'}}>{menu.title}</h4>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    )
  }
}

const MENU = [{
  id: 'my-schedule',
  title: 'My Meetings',
  icon: <Icon.Hash size={20} className="mr-1" />
}, {
  id: 'join-meeting',
  title: 'Join Meeting',
  icon: <Icon.Airplay size={20} className="mr-1" />
}, {
  id: 'preferences',
  title: 'Preferences',
  icon: <Icon.Settings size={20} className="mr-1" />
}]

export default FixedSidebar;