import React, { Component } from "react";
import { Segment, Grid } from 'semantic-ui-react';
import * as Icon from "react-feather"
import VideoStream from './videoStream';
const USER_ID = Math.floor(Math.random() * 1000000001);

export default class Call extends Component {
  state = {
    initialized: false,
    isScreenStreamInitialized: false,
    videoStream: {},
    remoteStreams: {},
    isAudio: true,
    isVideo: true,
    remoteShareScreenStreams: {},
    isSharingScreen: false,
    activeStream: 'local_stream'
  };

  componentDidMount() {
    const videoStream = new VideoStream(USER_ID, this.updateRemoteStrams);
    this.setState({
      videoStream,
    });
  }

  updateRemoteStrams = (remoteStreams) => {
    this.setState({
      remoteStreams
    })
  }

  updateRemoteShareScreenStreams = (remoteShareScreenStreams) => {
    this.setState({
      remoteShareScreenStreams,
    })
  } 

  componentDidUpdate(prevProps, prevState) {
    const { videoStream } = this.state;
    const { roomId } = this.props;
    if (!this.state.initialized && roomId !== "") {
      this.setState({
        showRoom: true,
        initialized: true,
      }, () => {
        videoStream.initLocalStream('local_stream', roomId, USER_ID);
      });
    }
  }

  leaveMeeting = () => {
    const { videoStream } = this.state;
    videoStream.leaveMeeting();
    this.props.leaveMeeting();
  }

  toggleMic = () => {
    const { videoStream } = this.state;
    const isAudio = videoStream.toggleAudio();
    this.setState({
      isAudio,
    })
  }

  toggleVideo = () => {
    const { videoStream } = this.state;
    const isVideo = videoStream.toggleVideo();
    this.setState({
      isVideo,
    });
  }

  toggleShareScreen = () => {
    const { videoStream, isSharingScreen } = this.state;
    const { roomId } = this.props;
    videoStream.close();
    videoStream.stop();
    videoStream.leaveMeeting();
    let newStream = null;
    if (!isSharingScreen) {
      newStream = new VideoStream(USER_ID, this.updateRemoteStrams, true);
    } else {
      newStream = new VideoStream(USER_ID, this.updateRemoteStrams);
    }
    newStream.initLocalStream('local_stream', roomId, USER_ID);
    this.setState({
      videoStream: newStream,
      isSharingScreen: !isSharingScreen,
    });
  }

  changeActiveStream = (stream) => {
    this.setState({
      activeStream: stream,
    })
    const { videoStream } = this.state;
    videoStream.onReplaceTrack(stream);
  }

  render() {
    const { isAudio, isVideo, videoStream , isSharingScreen, activeStream } = this.state;
    const { roomId } = this.props;
    const { showRoom } = this.state;
    const { remoteStreams = {} } = videoStream;
    if (!showRoom) {
      return null;
    }
    

    return (
      <div className="d-flex flex-column position-relative h-100">
        <Grid stackable className="h-100">
          <Grid.Row>
            <Grid.Column width={8}>
              <div className="h-100" id={activeStream}></div>
            </Grid.Column>
            {Object.keys(remoteStreams).map(key => {
              const stream = remoteStreams[key];
              const streamId = stream.getId();
              return (
                <Grid.Column width={8}>
                  <div key={streamId} id={`agora_remote ${streamId}`} className="h-100" onClick={this.changeActiveStream.bind(this, stream)}/>
                </Grid.Column>
              );
            })}
          </Grid.Row>
        </Grid>
        
        
        <Segment>
          <Grid>
            <Grid.Row className="p-1">
              <Grid.Column width={4}>
                <h3 className="text-bold-600 ml-1">{roomId}</h3>
              </Grid.Column>
              <Grid.Column width={8}>
                <div className="text-center">
                  {!isAudio ?
                    <Icon.MicOff size={20} className="mr-1 cursor-pointer" onClick={this.toggleMic}/> :
                    <Icon.Mic size={20} className="mr-1 cursor-pointer" onClick={this.toggleMic}/>
                  }
                  <Icon.PhoneOff size={20} className="mr-1 cursor-pointer" onClick={this.leaveMeeting}/>
                  {!isVideo ?
                    <Icon.VideoOff size={20} className="mr-1 cursor-pointer" onClick={this.toggleVideo}/> :
                    <Icon.Video size={20} className="mr-1 cursor-pointer" onClick={this.toggleVideo}/>
                  }                  
                  <Icon.Airplay size={20} className="mr-1 cursor-pointer" onClick={this.toggleShareScreen} style={{color: isSharingScreen ? 'green' : 'black'}}/>
                </div>
              </Grid.Column>
              <Grid.Column width={4}>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
      </div>
    );
  }
}