import React, { Component } from 'react';
import { Button, Grid, Input } from 'semantic-ui-react';
import Select from "react-select"
import moment from 'moment';
import Flatpickr from "react-flatpickr";
import { withSnackbar } from 'react-simple-snackbar'
import { v4 as uuidv4 } from 'uuid';
import { startMeeting, updateMeetingInfo } from "../../../../../redux/actions/meeting/index";
import "flatpickr/dist/themes/light.css";
import "../../../../../assets/scss/plugins/forms/flatpickr/flatpickr.scss"

const notifyOptions = [{
  label: '5 minutes before',
  value: 5,
}, {
  label: '10 minutes before',
  value: 10,
}]

class ScheduleForm extends Component {
  state = {
    startTime: new Date(),
    name: '',
    selectedOptions: [],
    notify_at: {
      label: '5 minutes before',
      value: 5,
    },
  }

  updateDetails = (meeting) => {
    console.log(meeting)
    const { title, invitedUsers, start_at, host_id, notify_at } = meeting;
    const selectedOptions = invitedUsers.filter(user => user.user_id !== host_id).map(user => Object.assign({}, {
      label: user.fullname,
      value: user.user_id,
    }));
    const start = moment.utc(start_at).local().toDate();
    const notifyDiff = Math.abs(moment.duration(moment(notify_at).diff(moment(start_at))).asMinutes());
    const notifyOption = notifyOptions.find(option => option.value === notifyDiff) || {}
    this.setState({
      name: title,
      selectedOptions,
      startTime: start,
      notify_at: notifyOption
    })
  }

  componentDidMount() {
    const { settings, editMeeting } = this.props;
    this.setState({
      name: settings.name,
    }, () => {
      if (editMeeting) {
        this.updateDetails(editMeeting);
      }
    });
  }

  componentDidUpdate(prevProps) {
    const { editMeeting } = this.props;
    const { id } = prevProps.editMeeting || {};
    if (editMeeting && editMeeting.id !== id) {
      this.updateDetails(editMeeting);
    }
  }

  onInputChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }

  onSelect = (options) => {
    this.setState({
      selectedOptions: options
    })
  }

  onSelectChange = (notify_at) => {
    this.setState({
      notify_at,
    })
  }

  onChange = (name, value) => {
    this.setState({
      [name]: value,
    });
  }

  startMeeting = async () => {
    const { openSnackbar, user, closeForm, editMeeting } = this.props;
    const { name, selectedOptions, startTime, notify_at } = this.state;
    if (!name) {
      openSnackbar('Name is required to schedule meeting');
      return;
    }
    console.log(selectedOptions)
    const selectedUsers = selectedOptions.map(option => option.value) || [];
    const start = moment(startTime).utc();

    const start_date = start.format('YYYY-MM-DD');
    const start_time = start.format('HH:mm');
    const start_at = `${start_date}T${start_time}:00.0000`;

    const notifyTime = start.subtract(notify_at.value, 'minutes');
    const notify_date = moment(notifyTime).format('YYYY-MM-DD');
    const notify_time = moment(notifyTime).format('HH:mm');
    const notifyValue = `${notify_date}T${notify_time}:00.0000`;

    const isMeetingInPast = start.isBefore(moment());
    if (isMeetingInPast) {
      openSnackbar('Select start time in future');
      return;
    }
    const roomId = uuidv4();
    const { origin } = window.location;
    const data = {
      call_id: roomId,
      call_type: 'video',
      agora_id: user.user_id,
      title: name,
      call_link: `${origin}/meeting-room/${roomId}`,
      user_id_list: [user.user_id].concat(selectedUsers),
      start_at: start_at,
      notify_at: notifyValue,
    };
    if (editMeeting) {
      await updateMeetingInfo(user, editMeeting.id, data);
    } else {
      await startMeeting(user, data);
    }
    
    this.setState({
      name: '',
      selectedOptions: [],
    }, () => {
      openSnackbar(`Meeting ${editMeeting ? 'updated' : 'scheduled'} successfully`);
      closeForm();
    })
  }

  render() {
    const { contacts = [], toggleForm, closeForm, editMeeting } = this.props;
    const { startTime, name, selectedOptions, notify_at } = this.state;
    const contactOptions = contacts.map(contact => Object.assign({}, {label: contact.fullname, value: contact.user_id}));
    const buttonTitle = editMeeting ? 'Update Meeting' : 'Schedule Meeting';
    return (
      <Grid.Column width={8}>
        <div className="mb-1 ml-2">Meeting name :</div>
        <Input type="text" name="name" className="ml-2" fluid placeholder="Meeting Name" value={name} onChange={this.onInputChange} />
        <Select isMulti name="with" options={contactOptions} className="React mt-1 ml-2" classNamePrefix="select" value={selectedOptions} onChange={this.onSelect} />
        <div className="mb-1 ml-2 mt-2">Start time :</div>
        <SelectTime dateTime={startTime} onChange={this.onChange} name="startTime" />
        <div className="mb-1 ml-2 mt-2">Notify members :</div>
        <Select name="notify_at" options={notifyOptions} className="React mt-1 ml-2" classNamePrefix="select" onChange={this.onSelectChange} value={notify_at} />
        <Button color="green" onClick={this.startMeeting} className="mt-1 ml-2">
          <span className="d-lg-block d-none" style={{color: 'white'}}>{buttonTitle}</span>
        </Button>
        <Button color="blue" onClick={toggleForm} className="mt-1 ml-1">
          <span className="d-lg-block d-none" style={{color: 'white'}}>Start Meeting</span>
        </Button>
        <Button color="red" onClick={closeForm} className="mt-1 ml-1">
          <span className="d-lg-block d-none" style={{color: 'white'}}>Cancel</span>
        </Button>
      </Grid.Column>
    )
  }
}

class SelectTime extends Component {
  onChange = (selectedDate, selectedTime) => {
    const selected_date = `${selectedDate}T${selectedTime}:00.0000`
    const value = new Date(selected_date);
    this.props.onChange(this.props.name, value);
  }

  onSelectDate = (date) => {
    const { dateTime } = this.props;
    const selectedDate = moment(date[0]).format('YYYY-MM-DD');
    const selectedTime = moment(dateTime).format('HH:mm');
    this.onChange(selectedDate, selectedTime);
  }

  onSelectTime = (time) => {
    const { dateTime } = this.props;
    const selectedDate = moment(dateTime).format('YYYY-MM-DD');
    const selectedTime = moment(time[0]).format('HH:mm');
    this.onChange(selectedDate, selectedTime);
  }

  render() {
    const { dateTime } = this.props;
    return (
      <Grid className="w-100 ml-2">
        <Grid.Row>
          <Grid.Column width={8}>
            <Flatpickr
              className="form-control w-100"
              value={dateTime}
              name="date"
              onChange={this.onSelectDate}
            />
          </Grid.Column>
          <Grid.Column width={8}>
            <Flatpickr
              className="form-control ml-2 w-100"
              value={dateTime}
              name="time"
              options={{
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
              }}
              onChange={this.onSelectTime}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

export default withSnackbar(ScheduleForm);