import React, { Component } from 'react';
import { Button, Grid, Input } from 'semantic-ui-react';
import Select from "react-select"
import { withSnackbar } from 'react-simple-snackbar'
import { v4 as uuidv4 } from 'uuid';
import { startMeeting } from "../../../../../redux/actions/meeting/index";

class CreateMeetingForm extends Component {
  state = {
    name: '',
    selectedOptions: [],
  }

  componentDidMount() {
    const { settings } = this.props;
    this.setState({
      name: settings.name,
    });
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }

  onSelect = (options) => {
    this.setState({
      selectedOptions: options
    })
  }

  startMeeting = async () => {
    const { openSnackbar, user, closeForm } = this.props;
    const { name, selectedOptions } = this.state;
    if (!name) {
      openSnackbar('Name is required to start meeting');
      return;
    }
    const selectedUsers = selectedOptions.map(option => option.value) || [];
    const { origin } = window.location;
    const roomId = uuidv4()
    const data = {
      call_id: roomId,
      call_type: 'video',
      agora_id: user.user_id,
      title: name,
      call_link: `${origin}/meeting-room/${roomId}`,
      user_id_list: [user.user_id].concat(selectedUsers),
    }
    const response = await startMeeting(user, data);
    const { meeting_id } = response;
    const url = `${origin}/meeting-room/${meeting_id}`
    const win = window.open(url, '_blank');
    win.focus();
    this.setState({
      name: '',
      selectedOptions: [],
    }, () => {
      closeForm();
    })
  }

  render() {
    const { contacts = [], toggleForm, closeForm } = this.props;
    const { name, selectedOptions } = this.state;
    const contactOptions = contacts.map(contact => Object.assign({}, {label: contact.fullname, value: contact.user_id}));
    return (
      <Grid.Column width={8}>
        <div className="mb-1 ml-2">Meeting name :</div>
        <Input type="text" name="name" className="ml-2" fluid placeholder="Meeting Name" value={name} onChange={this.onChange} />
        <Select isMulti name="with" options={contactOptions} className="React mt-1 ml-2" classNamePrefix="select" value={selectedOptions} onChange={this.onSelect} />
        <Button color="green" onClick={this.startMeeting} className="mt-1 ml-2">
          <span className="d-lg-block d-none" style={{color: 'white'}}>Start Meeting</span>
        </Button>
        <Button color="blue" onClick={toggleForm} className="mt-1 ml-1">
          <span className="d-lg-block d-none" style={{color: 'white'}}>Schedule Meeting</span>
        </Button>
        <Button color="red" onClick={closeForm} className="mt-1 ml-1">
          <span className="d-lg-block d-none" style={{color: 'white'}}>Cancel</span>
        </Button>
      </Grid.Column>
    )
  }
}

export default withSnackbar(CreateMeetingForm);
