import React from 'react';
import { Grid } from 'semantic-ui-react';

export default () => (
  <div className="mt-1">
    <Grid className="w-100">
      <Grid.Column width={8}>
        <h1 className="tex-bold-600 ml-2">
          My Meetings
        </h1>
      </Grid.Column>
    </Grid>
  </div>
)