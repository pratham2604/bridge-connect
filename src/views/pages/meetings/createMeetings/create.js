import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import '../../../../assets/scss/pages/app-chat.scss';
import CreateMeetingForm from './meetingForm/create';
import ScheduleMeetingForm from './meetingForm/schedule';

class Index extends Component {
  state = {
    showScheduleForm: false,
  }

  toggleForm = () => {
    this.setState({
      showScheduleForm: !this.state.showScheduleForm,
    });
  }

  render() {
    const { showScheduleForm } = this.state;
    const { contacts, user, closeForm, settings, editMeeting } = this.props;
    return (
      <div>
        <div className="mt-2 ml-2 h-100">
          <Grid className="w-100">
            <Grid.Row>
              {showScheduleForm || editMeeting ?
                <ScheduleMeetingForm contacts={contacts} toggleForm={this.toggleForm} user={user} closeForm={closeForm} settings={settings} editMeeting={editMeeting}/> :
                <CreateMeetingForm contacts={contacts} toggleForm={this.toggleForm} user={user} closeForm={closeForm} settings={settings}/>
              }
            </Grid.Row>
          </Grid>
        </div>
      </div>
    )
  }
}

export default Index;
