import React, { Component } from 'react';
import { connect } from "react-redux"
import Header from './header';
import CreateMeeting from './create';
import MeetingCard from './meetingCard';
import { Button, Grid } from 'semantic-ui-react';
import { getFutureMeetings } from "../../../../redux/actions/meeting/index"
import { getContactsList, getChats } from "../../../../redux/actions/chat/index"
import { getUser } from '../../../../redux/actions/auth/loginActions'

class Index extends Component {
  state = {
    showMeetingForm: false,
    contacts: [],
    futureMeetings: [],
    editMeeting: null,
  }

  async componentDidMount() {
    const { user } = this.props;
    const contacts = await getContactsList(user);
    const chats = await getChats(user);
    const filteredChats = (chats || []).filter(chat => !chat.is_group).filter((chat) => {
      return !!chat.last_msg && !!chat.last_msg.msg
    });
    const userIds = contacts.map(contact => contact);
    filteredChats.forEach(chat => {
      const { users } = chat;
      users.forEach(userId => {
        if (userId !== user.user_id && !userIds.some(id => id === userId)) {
          userIds.push(userId);
        }
      })
    });
    const users = await Promise.all(userIds.map(userId => getUser(user, userId)));
    const meetings = await this.getMeetings();
    this.setState({
      contacts: users,
      futureMeetings: meetings
    });
  }

  getMeetings = async () => {
    const { user } = this.props;
    const meetings = await getFutureMeetings(user) || [];
    await Promise.all(meetings.map(async (meeting) => {
      const { invited_users, host_id } = meeting;
      const userIds = Object.keys(invited_users);
      const users = await Promise.all(userIds.map(userId => getUser(user, userId)));
      meeting.invitedUsers = users;
    }));
    return meetings;
  }

  toggleForm = async () => {
    const { user } = this.props;
    const { futureMeetings, showMeetingForm } = this.state;
    const newValue = !showMeetingForm;
    let meetings = futureMeetings; 
    if (!newValue) {
      meetings = await this.getMeetings();
    }
    this.setState({
      showMeetingForm: newValue,
      futureMeetings: meetings,
      editMeeting: null,
    });
  }

  onEdit = (meeting) => {
    this.setState({
      showMeetingForm: true,
      editMeeting: meeting,
    })
  }

  render() {
    const { showMeetingForm, contacts, futureMeetings, editMeeting } = this.state;
    const { user, settings } = this.props;
    return (
      <Grid className="w-100 h-100">
        <Grid.Column width={16}>
          <Header />
          <div className="mt-2 ml-2">
            {showMeetingForm ?
              <CreateMeeting contacts={contacts} user={user} closeForm={this.toggleForm} settings={settings} editMeeting={editMeeting} /> :
              <Button color="blue" onClick={this.toggleForm} className="mt-1">
                <span className="d-lg-block d-none" style={{color: 'white'}}>Schedule Meeting</span>
              </Button>
            }
          </div>
          <div className="mt-2 ml-2">
            <Grid>
              <Grid.Row>
                {futureMeetings.map((meeting, index) => {
                  return (
                    <Grid.Column key={index} width={8}>
                      <MeetingCard meeting={meeting} user={user} edit={this.onEdit}/>
                    </Grid.Column>
                  )
                })}
              </Grid.Row>
            </Grid>
          </div>
        </Grid.Column>
      </Grid>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    settings: state.meeting.settings,
    updateFromSocket: state.socket.newMessage
  }
}

const mapDispatchtoProps = (dispatch) => {
  return {
    dispatch
  };
};

export default connect(mapStateToProps, mapDispatchtoProps)(Index);