import React, { Component, Fragment } from 'react';
import { Button, Grid, Input, Segment } from 'semantic-ui-react';
import { getMeetingInfoFromShortId } from "../../../redux/actions/meeting/index"

import CallRoom from './callroom';
import * as Icon from "react-feather"
import { history } from '../../../history';

class Index extends Component {
  state = {
    roomId: '',
    roomIdValue: ''
  }

  startMeeting = async () => {
    const { roomIdValue } = this.state;
    const { user } = this.props;
    const meetingInfo = await getMeetingInfoFromShortId(user, roomIdValue);
    const { id } = meetingInfo;
    history.push(`/meeting-room/${id}`);
  }

  onChange = (e) => {
    const { value } = e.target;
    this.setState({
      roomIdValue: value,
    })
  }

  leaveMeeting = () => {
    this.setState({
      roomId: '',
    }, () => {
      this.props.toggleSidebar();
    })
  }

  render() {
    const { roomId } = this.state;
    return (
      <Grid className="w-100 h-100">
        <Grid.Column width={16} className="h-100">
          <div className="mt-2 ml-2">
            <h1 className="tex-bold-600">
              Join meeting
            </h1>
          </div>
          <div className="mt-2 ml-2 h-100">
            <Input onChange={this.onChange} placeholder="Enter Code" className="mr-1"/>
            <Button onClick={this.startMeeting}>
              Join Meeting
            </Button>
          </div>
        </Grid.Column>
      </Grid>
    )
  }
}

export default Index;