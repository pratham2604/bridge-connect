import React, { Component } from 'react';
import { connect } from "react-redux"
import { Button, Grid, Card, Checkbox } from 'semantic-ui-react';
import { Input } from 'semantic-ui-react';
import { updateMeetingSettings } from '../../../redux/actions/meeting/index'
import { withSnackbar } from 'react-simple-snackbar'

class Index extends Component {
  state = {
    name: 'Meeting',
    audio: true,
    video: true,
  }

  componentDidMount() {
    const { settings } = this.props;
    const { name, audio, video } = settings;
    this.setState({
      name: name || 'Meeting',
      audio: !!audio,
      video: !!video
    });
  }

  onCheckChange = (reactClass, value) => {
    const { checked, name } = value;
    this.setState({
      [name]: checked,
    })
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }

  onSave = () => {
    const { dispatch, openSnackbar } = this.props;
    const { name, audio, video } = this.state;
    const data = { name, audio, video };
    dispatch(updateMeetingSettings(data));
    openSnackbar('Meeting preferences updated successfully');
  }

  render() {
    const { name, audio, video } = this.state;
    return (
      <Grid className="w-100 h-100">
        <Grid.Column width={16}>
          <div className="mt-1">
            <Grid className="w-100">
              <Grid.Column width={8}>
                <h1 className="tex-bold-600 ml-2">
                  Meeting Preferences
                </h1>
              </Grid.Column>
            </Grid>
          </div>
          <div className="mt-2 ml-2">
            <Grid>
              <Grid.Row>
                <Grid.Column width={8}>
                  <Card className="mt-2 ml-2 w-100 p-2">
                    <h3 className="tex-bold-600">
                      Default Topic
                    </h3>
                    <div>
                      <Input type="text" name="name" className="" value={name} fluid placeholder="Meeting Name" onChange={this.onChange} />
                    </div>
                    <hr />
                    <h3 className="tex-bold-600 mt-0">
                      Settings
                    </h3>
                    <div>
                      <div className="d-flex mb-1" style={{fontSize: '1.2rem'}}>
                        <div style={{flex: 1}}>Start with audio</div>
                        <div>
                          <Checkbox checked={audio} name="audio" toggle style={{cursor: 'pointer'}} onChange={this.onCheckChange}/>
                        </div>
                      </div>
                      <div className="d-flex" style={{fontSize: '1.2rem'}}>
                        <div style={{flex: 1}}>Start with video</div>
                        <div>
                          <Checkbox checked={video} name="video" toggle style={{cursor: 'pointer'}} onChange={this.onCheckChange}/>
                        </div>
                      </div>
                    </div>
                    <hr />
                    <div>
                      <Button color="green" onClick={this.onSave}>Save</Button>
                    </div>
                  </Card>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </Grid.Column>
      </Grid>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    settings: state.meeting.settings,
    updateFromSocket: state.socket.newMessage
  }
}

const mapDispatchtoProps = (dispatch) => {
  return {
    dispatch
  };
};

export default connect(mapStateToProps, mapDispatchtoProps)(withSnackbar(Index));