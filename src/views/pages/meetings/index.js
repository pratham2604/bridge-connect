import React from "react"
import Sidebar from "react-sidebar"
import { ContextLayout } from "../../../utility/context/Layout"
import FixedSideBar from './sidebar';
import MySchedule from './createMeetings/index';
import Preferences from './preferences';
import JoinMeeting from './joinMeetings';
import { connect } from "react-redux";
const mql = window.matchMedia(`(min-width: 992px)`)

class Index extends React.Component {
  state = {
    composeMailStatus: false,
    sidebarDocked: mql.matches,
    sidebarOpen: false,
    activeTab: 'my-schedule',
    showSidebar: true,
  }

  handleComposeSidebar = status => {
    if (status === "open") {
      this.setState({
        composeMailStatus: true
      })
    } else {
      this.setState({
        composeMailStatus: false
      })
    }
  }

  UNSAFE_componentWillMount() {
    mql.addListener(this.mediaQueryChanged)
  }

  componentWillUnmount() {
    mql.removeListener(this.mediaQueryChanged)
  }

  onSetSidebarOpen = open => {
    this.setState({ sidebarOpen: open })
  }

  mediaQueryChanged = () => {
    this.setState({ sidebarDocked: mql.matches, sidebarOpen: false })
  }

  handleMainAndComposeSidebar = () => {
    this.handleComposeSidebar("close")
    this.onSetSidebarOpen(false)
  }

  changeTab = (id) => {
    this.setState({
      activeTab: id,
    })
  }

  toggleSidebar = () => {
    this.setState({
      showSidebar: !this.state.showSidebar
    })
  }

  render() {
    const { activeTab, showSidebar } = this.state;
    const { user } = this.props;
    return (
      <React.Fragment>
        <div className="custom-fixed-sidebar-container position-relative" style={{height: '100%'}}>
          <div
            className={`app-content-overlay ${
              this.state.composeMailStatus || this.state.sidebarOpen ? "show" : ""
            }`}
            onClick={this.handleMainAndComposeSidebar}
          />
          <ContextLayout.Consumer>
            {context => (
              <Sidebar
                sidebar={
                  <FixedSideBar changeTab={this.changeTab} />
                }
                docked={showSidebar}
                open={this.state.sidebarOpen}
                sidebarClassName="sidebar-content custom-app-sidebar d-flex"
                touch={false}
                contentClassName="sidebar-children"
                pullRight={context.state.direction === "rtl"}>
                <div className="w-100 h-100">
                  {activeTab === 'my-schedule' && <MySchedule toggleSidebar={this.toggleSidebar} user={user}/>}
                  {activeTab === 'join-meeting' && <JoinMeeting toggleSidebar={this.toggleSidebar} user={user}/>}
                  {activeTab === 'preferences' && <Preferences />}
                </div>
              </Sidebar>
            )}
          </ContextLayout.Consumer>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {})(Index);
