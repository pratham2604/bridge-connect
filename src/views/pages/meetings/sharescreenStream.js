import AgoraRTC from "agora-rtc-sdk";
const client = AgoraRTC.createClient({mode: 'rtc', codec: 'vp8'});
const APP_ID = '6f4a0bdc0c894f73988c2e25a85430b8';

class ShareScreenStream {
  localStream = {};

  remoteStreams = {};
  isAudio = true;
  isVideo = true;

  constructor(userId, updateStreamsinParent) {
    this.localStream = AgoraRTC.createStream({
      streamID: userId,
      audio: false,
      video: false,
      screen: true
    });
    this.initClient();
    this.updateStreamsinParent = updateStreamsinParent;
  }

  initClient = () => {
    client.init(APP_ID, () => {
      console.log('AgoraRTC Sharescreen client initialized');
    }, (err) => {
      console.log("AgoraRTC Sharescreen client init failed", err);
    });
    this.subscribeToClient(client);
  };

  initLocalStream = (id, roomId, userId) => {
    this.localStream.init(() => {
      console.log("getUserMedia successfully");
      this.localStream.play(id);
      this.joinChannel(roomId, userId);
    }, (err) => {
      console.log("getUserMedia failed", err);
    });
  };

  joinChannel = (roomId, userId) => {
    client.join(null, roomId, userId, (uid) => {
      const logMessage = "User " + uid + " join channel successfully";
      console.log(logMessage);
      client.publish(this.localStream, (err) => {
        console.log("Publish local stream error: " + err);
      });

      client.on("stream-published", (evt) => {
        const logMessage = "Publish local stream successfully";
        console.log(logMessage);
      });
    }, (err) => {
      console.log("Join channel failed", err);
    });
  };

  subscribeToClient = () => {
    client.on("stream-added", this.onStreamAdded);
    client.on("stream-subscribed", this.onRemoteClientAdded);
    client.on("stream-removed", this.onStreamRemoved);
    client.on("peer-leave", this.onPeerLeave);
  };

  onStreamAdded = (event) => {
    let stream = event.stream;
    const message = "New Sharescreen stream added: " + stream.getId()
    console.log(message);
    const { remoteStreams } = this;
    const newId = stream.getId()
    const updatedStreams = Object.assign({}, remoteStreams, {
      [newId]: stream,
    });

    this.updateStreams(updatedStreams);
    // Subscribe after new remoteStreams state set to make sure
    // new stream dom el has been rendered for agora.io sdk to pick up
    client.subscribe(stream, (err) => {
      console.log("Subscribe Sharescreen stream failed", err);
    });
  };

  onStreamRemoved = evt => {
    const me = this;
    const stream = evt.stream;
    if (stream) {
      const streamId = stream.getId();
      const { remoteStreams } = me;
      stream.stop();
      delete remoteStreams[streamId];
      this.updateStreams(remoteStreams);
      console.log("Remote Sharescreen stream is removed " + stream.getId());
    }
  };

  onPeerLeave = evt => {
    const me = this;
    const stream = evt.stream;
    if (stream) {
      const streamId = stream.getId();
      const { remoteStreams } = me;
      stream.stop();
      delete remoteStreams[streamId];
      this.updateStreams(remoteStreams);
      console.log("Remote Sharescreen stream is removed " + stream.getId());
    }
  };

  onRemoteClientAdded = event => {
    const remoteStream = event.stream;
    const message = 'Remote Sharescreen stream added ' + remoteStream.getId();
    console.log(message);
    this.remoteStreams[remoteStream.getId()].play(
      "agora_sharescreen_remote " + remoteStream.getId()
    );
    this.updateStreams(this.remoteStreams);
  };

  updateStreams = (remoteStreams) => {
    this.remoteStreams = remoteStreams;
    this.updateStreamsinParent(remoteStreams);
  }

  leaveMeeting = () => {
    this.localStream.close();
    client.leave();
    this.updateStreams(this.remoteStreams);
  }

  toggleAudio = () => {
    if (this.isAudio) {
      this.localStream.muteAudio();
    } else {
      this.localStream.unmuteAudio();
    }
    this.isAudio = !this.isAudio;
    return this.isAudio;
  }

  toggleVideo = () => {
    if (this.isVideo) {
      this.localStream.muteVideo();
    } else {
      this.localStream.unmuteVideo();
    }
    this.isVideo = !this.isVideo;
    return this.isVideo;
  }
}

export default ShareScreenStream;