import React, { Component } from 'react';
import { Grid, Image, Button, Segment } from 'semantic-ui-react';
import Select from "react-select"
import {
  FormGroup,
  Input,
} from "reactstrap"
import "flatpickr/dist/themes/light.css";
import { updateProfile, getUser, SendOTP, VerifyOTP, getSelfUserInfo } from '../../../redux/actions/auth/loginActions';
import "../../../assets/scss/plugins/forms/flatpickr/flatpickr.scss"
import { connect } from "react-redux";
import axios from 'axios';
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'

const STATUS_OPTIONS = [{
  label: 'Available',
  value: 'Available',
}, {
  label: 'At Work',
  value: 'At work',
}, {
  label: 'Busy',
  value: 'Busy',
}]

class Index extends Component {
  state = {
    showAddProfileForm: true,
  }

  toggleAddProfileForm = () => {
    this.setState({
      showAddProfileForm: !this.state.showAddProfileForm,
    })
  }

  render() {
    const { showAddProfileForm } = this.state;
    return (
      <div className="ml-3">
        <div className="mt-1">
          <h1 className="tex-bold-600">
            Accounts
          </h1>
          <div className="text-muted mt-0">
            Manage your profiles
          </div>
        </div>
        <Grid className="w-100 mt-1">
          <Grid.Row>
            <Profile user={this.props.user} updateProfile={this.props.updateProfile}/>
            {/* <Grid.Column width={8}>
              {showAddProfileForm ?
                <AddProfileForm toggleForm={this.toggleAddProfileForm}/> :
                <Button onClick={this.toggleAddProfileForm}>Add Account</Button>
              }
            </Grid.Column> */}
          </Grid.Row>
        </Grid>
      </div>
    )
  }
}

class AddProfileForm extends Component {
  state = {
    fullname: '',
    avatar: '',
    status: STATUS_OPTIONS[0],
    phone: '',
    verifyForm: false,
    otp: '',
  }

  onChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  }

  requestOTP = () => {
    const { phone } = this.state;

    SendOTP(`+${phone}`, () => {
      this.setState({
        verifyForm: true,
      });
    })
  }

  verifyOTP = () => {
    const { phone, otp } = this.state;
    VerifyOTP(`+${phone}`, otp, async (user) => {
      const response = await getSelfUserInfo(user);
      const { profile } = response;
      console.log(profile)
      // if (!profile.fullname) {
      //   history.push('/account-setup');
      // } else {
      //   history.push('/meetings');
      // }
    })
  }

  render() {
    const { fullname, avatar, status, closed, verifyForm, otp } = this.state;
    const { toggleForm } = this.props;
    return (
      <Grid.Column width={8}>
        <Segment>
          <div className="mt-1 ml-2 mb-1">
            <Grid className="w-100">
              {verifyForm ?
                <Grid.Row>
                  <Grid.Column width={16}>
                    <div className="m-auto">
                      <Input placeholder='Enter OTP' onChange={e => this.setState({ otp: e.target.value })} value={otp} />
                    </div>
                    <div className="">
                      <Button className="mt-1" onClick={this.verifyOTP}>Verify OTP</Button>
                      <Button className="mt-1" onClick={toggleForm} color="red">Cancel</Button>
                    </div>
                  </Grid.Column>
                </Grid.Row> :
                <Grid.Row>
                  <Grid.Column width={16}>
                    <div className="m-auto">
                      <PhoneInput
                        country={'in'}
                        value={this.state.phone}
                        onChange={phone => this.setState({ phone })}
                        className="m-auto"
                      />
                    </div>
                    <div className="">
                      <Button className="mt-1" onClick={this.requestOTP}>Request OTP</Button>
                      <Button className="mt-1" onClick={toggleForm} color="red">Cancel</Button>
                    </div>
                  </Grid.Column>
                </Grid.Row>
              }
            </Grid>
          </div>
        </Segment>
      </Grid.Column>
    )
  }
}

class Profile extends Component {
  state = {
    fullname: '',
    avatar: '',
    status: STATUS_OPTIONS[0],
    closed: true,
  }

  async componentDidMount() {
    const { user } = this.props;
    let userProfile = user;
    if (!user.fullname) {
      userProfile = await getUser(user, user.user_id);
    }
    const { fullname, avatar, status, color } = userProfile || {};
    const selectedStatus = STATUS_OPTIONS.find(option => option.value === status) || {};
    this.setState({
      fullname,
      avatar: avatar || color,
      status: selectedStatus,
    });
  }

  onChange = async (event) => {
    const { name, value } = event.target;
    this.setState({
      [name]: value,
    });
    if (name === 'fullname') {
      const name = value.split(' ').join('+');
      const res = await axios.get(`https://ui-avatars.com/api/?name=${name}&background=CF98BF&color=ffffff&size=512`);
      const avatar = res.config.url;
      this.setState({
        avatar,
      })
    }
  }

  onSelect = (option) => {
    console.log(option)
    this.setState({
      status: option,
    })
  }

  onSubmit = () => {
    const { fullname, avatar, status } = this.state;
    const data = Object.assign({}, {
      fullname,
      avatar,
      status: status.value,
    });
    const { user, updateProfile } = this.props;
    updateProfile(data, user);
  }

  toggleDetails = () => {
    this.setState({
      closed: !this.state.closed,
    })
  }

  render() {
    const { fullname, avatar, status, closed } = this.state;
    return (
      <Grid.Column width={8}>
        {closed ?
          <Segment>
            <div className="mt-1 ml-2 mb-1">
              <div className="d-flex">
                <Image src={avatar} size="mini" circular className="m-auto"/>
                <h3 style={{flex: 1}} className="text-bold-600 d-flex flex-column ml-2 mt-1">{fullname}</h3>
                <Button onClick={this.toggleDetails}>Edit</Button>
              </div>
            </div>
          </Segment> :
          <Segment>
            <div className="mt-1 ml-2 mb-1">
              <Grid className="w-100">
                <Grid.Row>
                  <Grid.Column width={16}>
                    
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={6}>
                    <FormGroup>
                      <Image src={avatar} size="small" circular className="m-auto"/>
                    </FormGroup>
                  </Grid.Column>
                  <Grid.Column width={10}>
                    <FormGroup>
                      <h3 className="text-bold-600">Full Name</h3>
                      <Input type="text" id="name" name="fullname" placeholder="Full Name" value={fullname} onChange={this.onChange}/>
                    </FormGroup>
                    <FormGroup>
                      <h3 className="text-bold-600">Avatar</h3>
                      <Input type="text" id="avatar" name="avatar" placeholder="Avatar Url" value={avatar} onChange={this.onChange}/>
                    </FormGroup>
                    <FormGroup>
                      <h3 className="text-bold-600">Status</h3>
                      <Select
                        className="React"
                        classNamePrefix="select"
                        value={status}
                        name="status"
                        options={STATUS_OPTIONS}
                        onChange={this.onSelect}
                      />
                    </FormGroup>
                    <Button style={{cursor: 'pointer'}} className="" fluid primary color="grey" onClick={this.onSubmit}>
                      <span style={{color: 'white'}}>Update</span>
                    </Button>
                    <div style={{textAlign: 'right'}}>
                      <Button className="mt-1" onClick={this.toggleDetails}>Close</Button>
                    </div>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
          </Segment>
        }
      </Grid.Column>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {
  updateProfile,
})(Index);