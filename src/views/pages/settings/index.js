import React from "react"
import Sidebar from "react-sidebar"
import { ContextLayout } from "../../../utility/context/Layout"
import "../../../assets/scss/pages/app-email.scss"
import Profile from './profile';
const mql = window.matchMedia(`(min-width: 992px)`)

class Index extends React.Component {
  state = {
    composeMailStatus: false,
    sidebarDocked: mql.matches,
    sidebarOpen: false
  }

  handleComposeSidebar = status => {
    if (status === "open") {
      this.setState({
        composeMailStatus: true
      })
    } else {
      this.setState({
        composeMailStatus: false
      })
    }
  }

  UNSAFE_componentWillMount() {
    mql.addListener(this.mediaQueryChanged)
  }

  componentWillUnmount() {
    mql.removeListener(this.mediaQueryChanged)
  }

  onSetSidebarOpen = open => {
    this.setState({ sidebarOpen: open })
  }

  mediaQueryChanged = () => {
    this.setState({ sidebarDocked: mql.matches, sidebarOpen: false })
  }

  handleMainAndComposeSidebar = () => {
    this.handleComposeSidebar("close")
    this.onSetSidebarOpen(false)
  }

  render() {
    return (
      <React.Fragment>
        <div className="custom-fixed-sidebar-container position-relative" style={{height: '100%'}}>
          <div
            className={`app-content-overlay ${
              this.state.composeMailStatus || this.state.sidebarOpen ? "show" : ""
            }`}
            onClick={this.handleMainAndComposeSidebar}
          />
          <ContextLayout.Consumer>
            {context => (
              <Sidebar
                sidebar={
                  <div>
                  </div>
                }
                docked={false}
                open={this.state.sidebarOpen}
                sidebarClassName="sidebar-content custom-app-sidebar d-flex"
                touch={false}
                contentClassName="sidebar-children"
                pullRight={context.state.direction === "rtl"}>
                <Profile />
              </Sidebar>
            )}
          </ContextLayout.Consumer>
        </div>
      </React.Fragment>
    )
  }
}

export default Index
