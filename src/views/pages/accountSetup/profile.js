import React, { Component } from 'react';
import { Grid, Image, Button, Segment } from 'semantic-ui-react';
import Select from "react-select"
import {
  FormGroup,
  Input,
} from "reactstrap"
import "flatpickr/dist/themes/light.css";
import { updateProfile } from '../../../redux/actions/auth/loginActions';
import "../../../assets/scss/plugins/forms/flatpickr/flatpickr.scss"
import { connect } from "react-redux";
import axios from 'axios';
import { history } from '../../../history';

const STATUS_OPTIONS = [{
  label: 'Active',
  value: 'active',
}, {
  label: 'Inactive',
  value: 'inactive',
}, {
  label: 'Busy',
  value: 'Busy'
}]

class Index extends Component {
  state = {
    fullname: '',
    avatar: '',
    status: STATUS_OPTIONS[0],
  }

  componentDidMount() {
    const { user } = this.props;
    const { fullname, avatar, status, color } = user;
    if (fullname && (avatar || color) && status) {
      history.push('/meetings');
    }

    const selectedStatus = STATUS_OPTIONS.find(option => option.value === status) || {};
    this.setState({
      fullname,
      avatar: avatar || color,
      status: selectedStatus,
    })
  }

  onChange = async (event) => {
    const { name, value } = event.target;
    this.setState({
      [name]: value,
    });
    if (name === 'fullname') {
      const name = value.split(' ').join('+');
      const res = await axios.get(`https://ui-avatars.com/api/?name=${name}&background=CF98BF&color=ffffff&size=512`);
      const avatar = res.config.url;
      this.setState({
        avatar,
      })
    }
  }

  onSelect = (option) => {
    console.log(option)
    this.setState({
      status: option,
    })
  }

  onSubmit = () => {
    const { fullname, avatar, status } = this.state;
    const data = Object.assign({}, {
      fullname,
      avatar,
      status: status.value,
    });
    const { user, updateProfile } = this.props;
    updateProfile(data, user);
  }

  render() {
    const { fullname, avatar, status } = this.state;

    return (
      <Grid className="w-100 mt-1" centered>
        <Grid.Row>
          <Grid.Column width={10}>
            <Segment>
              <div className="mt-1 ml-2 mb-1">
                <Grid className="w-100">
                  <Grid.Row>
                    <Grid.Column width={16}>
                      <h1 className="tex-bold-600 text-center">
                        Account
                      </h1>
                      <div className="text-muted mt-0 text-center">
                        Update your profile
                      </div>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column width={6}>
                      <FormGroup>
                        <Image src={avatar} size="small" circular className="m-auto"/>
                      </FormGroup>
                    </Grid.Column>
                    <Grid.Column width={10}>
                      <FormGroup>
                        <h3 className="text-bold-600">Full Name</h3>
                        <Input type="text" id="name" name="fullname" placeholder="Full Name" value={fullname} onChange={this.onChange}/>
                      </FormGroup>
                      <FormGroup>
                        <h3 className="text-bold-600">Avatar</h3>
                        <Input type="text" id="avatar" name="avatar" placeholder="Avatar Url" value={avatar} onChange={this.onChange}/>
                      </FormGroup>
                      <FormGroup>
                        <h3 className="text-bold-600">Status</h3>
                        <Select
                          className="React"
                          classNamePrefix="select"
                          value={status}
                          name="status"
                          options={STATUS_OPTIONS}
                          onChange={this.onSelect}
                        />
                      </FormGroup>
                      <Button style={{cursor: 'pointer'}} className="" fluid primary color="grey" onClick={this.onSubmit}>
                        <span style={{color: 'white'}}>Update</span>
                      </Button>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </div>
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  }
}

export default connect(mapStateToProps, {
  updateProfile,
})(Index);