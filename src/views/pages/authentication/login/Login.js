import React, { Fragment } from "react"
import {
  Card,
  CardHeader,
  CardTitle,
  Row,
  Col,
} from "reactstrap"
import "../../../../assets/scss/pages/authentication.scss"
import LoginFirebase from "./LoginFirebase"
import Menu from '../../home/menu';

class Login extends React.Component {
  state = {
    activeTab: "1"
  }
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      })
    }
  }
  render() {
    return (
      <Fragment>
        <Menu fixed/>
        <Row className="m-0 justify-content-center">
          <Col
            sm="4"
            className="d-flex justify-content-center"
          >
            <Card className="bg-authentication login-card rounded-0 mb-0 w-100">
              <Row className="m-0">
                <Col lg="12" md="12" className="p-0">
                  <Card className="rounded-0 mb-0 px-2 login-tabs-container">
                    <CardHeader className="pb-1">
                      <CardTitle>
                        <h4 className="mb-0">Login</h4>
                      </CardTitle>
                    </CardHeader>
                    <p className="px-2 auth-title">
                      Welcome back, please login to your account.
                    </p>
                    <LoginFirebase />
                  </Card>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </Fragment>
    )
  }
}
export default Login
