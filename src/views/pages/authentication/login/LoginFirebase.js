import React from "react"
import { CardBody, FormGroup, Form, Input, Button, Label } from "reactstrap"
import { Lock } from "react-feather"
import { history } from "../../../../history"
import { connect } from "react-redux"
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import {
  SendOTP,
  VerifyOTP,
  getSelfUserInfo
} from "../../../../redux/actions/auth/loginActions"

class LoginFirebase extends React.Component {
  state = {
    email: "demo@demo.com",
    password: "demodemo",
    remember: false,
    otpSent: false,
  }

  handleLogin = e => {
    e.preventDefault();
    const { phone, otp } = this.state;
    this.props.VerifyOTP(`+${phone}`, otp, async (user) => {
      const response = await getSelfUserInfo(user);
      const { profile } = response;
      if (!profile.fullname) {
        history.push('/account-setup');
      } else {
        history.push('/meetings');
      }
    })
  }

  sendOTP = () => {
    const { phone } = this.state;

    SendOTP(`+${phone}`, () => {
      this.setState({
        otpSent: true,
      });
    })
  }

  handleRemember = e => {
    this.setState({
      remember: e.target.checked
    })
  }

  render() {
    const { otpSent } = this.state;
    return (
      <React.Fragment>
        <CardBody className="pt-1">
          <Form>
            {!otpSent ?
              <React.Fragment>
                <FormGroup className="form-label-group position-relative has-icon-left">
                  <Label>Mobile</Label>
                  <PhoneInput
                    country={'in'}
                    value={this.state.phone}
                    onChange={phone => this.setState({ phone })}
                  />
                  {/* <div className="form-control-position">
                    <Phone size={15} />
                  </div> */}
                  
                </FormGroup>
                <div className="d-flex justify-content-center mt-1">
                  <Button.Ripple color="primary" onClick={this.sendOTP}>
                    Request OTP
                  </Button.Ripple>
                </div>
              </React.Fragment> :
              <React.Fragment>
                <Form onSubmit={this.handleLogin}>
                <FormGroup className="form-label-group position-relative has-icon-left">
                  <PhoneInput
                    country={'in'}
                    value={this.state.phone}
                    onChange={phone => this.setState({ phone })}
                  />
                  {/* <div className="form-control-position">
                    <Phone size={15} />
                  </div> */}
                  {/* <Label>Phone</Label> */}
                </FormGroup>
                <FormGroup className="form-label-group position-relative has-icon-left">
                  <Input
                    type="text"
                    placeholder="OTP"
                    value={this.state.otp}
                    onChange={e => this.setState({ otp: e.target.value })}
                    required
                  />
                  <div className="form-control-position">
                    <Lock size={15} />
                  </div>
                  <div className="d-flex justify-content-center mt-1">
                    <Button.Ripple color="primary" type="submit">
                      Login
                    </Button.Ripple>
                  </div>
                </FormGroup>
                </Form>
              </React.Fragment>
            }
          </Form>
        </CardBody>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    values: state.auth.user
  }
}

export default connect(mapStateToProps, {
  // submitLoginWithFireBase,
  // loginWithFB,
  // loginWithTwitter,
  // loginWithGoogle,
  // loginWithGithub
  VerifyOTP,
})(LoginFirebase)
