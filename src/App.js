import React from "react"
import Router from "./Router"
import "./components/@OCG/rippleButton/RippleButton"

import "react-perfect-scrollbar/dist/css/styles.css"
import "prismjs/themes/prism-tomorrow.css"
import 'semantic-ui-css/semantic.min.css'

const App = props => {
  return <Router />
}

export default App
