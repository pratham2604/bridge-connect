import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getSelfInfo } from '../redux/actions/auth/loginActions'
import { setupClient } from '../redux/actions/socket/index'
import JoinRoom from '../views/pages/chats/callNotification';
import MeetingNotification from '../views/pages/meetingRoom/notification';
const GUEST_ROUTES = ['/login', '/', '/privacy-policy', '/terms'];
const AUTH_ROUTES = ['/settings', '/chats', '/groups'];

class Auth extends Component {
  state = {
    loadingAuth: true,
    socketConnected: false,
    showCall: false,
    openCallModal: false,
    callData: {},
    showMeeting: false,
    openMeetingModal: false,
  }

  componentDidMount() {
    const { history, isAuthenticated, location, dispatch, auth } = this.props;
    const isGuestRoute = GUEST_ROUTES.find(route => route === location.pathname);
    if (isAuthenticated && isGuestRoute) {
      if (!auth.fullname) {
        history.push('/account-setup');
      } else {
        const isAuthRoute = AUTH_ROUTES.find(route => route === location.pathname);
        if (!isAuthRoute) {
          history.push('/meetings');
        }
      }
    } else if (!isAuthenticated && !isGuestRoute) {
      history.push('/');
    } else if (isAuthenticated) {
      if (!auth.fullname) {
        history.push('/account-setup');
      }
      dispatch(getSelfInfo(auth))
    }
    dispatch({
      type: 'RESET_SOCKET'
    })
    this.connectToSocket();
  }

  connectToSocket = (meeting_id) => {
    const { pathname } = window.location;
    let meetingId = '';
    const pathnameParts = pathname.split('/');
    const id = pathnameParts.length && pathnameParts.pop();
    const route = pathnameParts.length && pathnameParts.pop();
    if (route === 'meeting-room') {
      meetingId = id;
    }

    const { socketConnected } = this.state;
    const { auth, dispatch } = this.props;
    if (!socketConnected && auth.user_id) {
      setupClient(auth.user_id, auth.token, dispatch, meetingId);
      this.setState({
        socketConnected: true,
      })
    }
  }

  componentDidUpdate() {
    const { showCall, showMeeting } = this.state;
    const { messageType, callData = {} } = this.props;
    if (messageType === 'NEW_CALL' && !showCall && !callData.meeting_id) {
      this.setState({
        openCallModal: true,
        showCall: true,
        callData,
      }); 
    }
    if (messageType === 'JOIN_MEETING' && !showMeeting && callData.meeting_id) {
      this.setState({
        openMeetingModal: true,
        showMeeting: true,
        callData,
      }); 
    }
    if (messageType === 'END_CALL' && showCall) {
      this.setState({
        openCallModal: false,
        showCall: false,
      })
    }
    this.connectToSocket();
  }

  onCloseCall = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'RESET_SOCKET'
    });
    this.setState({
      showCall: false,
      openCallModal: false,
      callData: {},
      openMeetingModal: false,
      showMeeting: false,
    })
  }

  render() {
    const { openCallModal, callData, openMeetingModal } = this.state;
    return (
      <Fragment>
        {openCallModal &&
          <JoinRoom callInfo={callData} socket={this.props.callData} user={this.props.auth} onClose={this.onCloseCall} />
        }
        {openMeetingModal &&
          <MeetingNotification meetingInfo={callData} user={this.props.auth} onClose={this.onCloseCall} />
        }
        {this.props.children}
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth.user,
  fetchingAuth: state.auth.fetching,
  isAuthenticated: state.auth.isAuthenticated,
  messageType: state.socket.type,
  callData: state.socket.data,
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Auth));